# Geoclipmap Terrain Generation

Geoclipmaped Terrain Generation is a non bounded terrain generation and
visualization library. It uses OpenGL-CUDA interop to generate a Perlin noise
based terrain height map that is visualized using the Geoclipmap technique
described in *Arul Asirvatham and Hugues Hoppe. “Terrain Rendering Using
GPU-Based Geometry Clipmaps”*. The terrain is rendered using a deferred
shading approach that tries to be generic by exposing different buffers as
outputs.

## Design and Implementation Overview

The library is composed of three main components: TerrainDataCache,
TerrainModel and TerrainRenderer.

### TerrainDataCache

The cache is the component in charge of store and update the data that shapes
the terrain local to the camera. It uses a multilevel memory approach where each
level represents double the surface area at half the detail than the one
before. A Perlin noise based height map is generated on demand as the camera
moves using CUDA. 

### TerrainModel

The model generates and stores the meshes needed to visualize the cached height
fields. The arrangement described in *Arul Asirvatham and Hugues Hoppe.
“Terrain Rendering Using GPU-Based Geometry Clipmaps”* is used. Also,  an Static
dependency injection is used to allocate the device memory needed for the mesh
in a API agnostic fashion.

### TerrainRenderer

The renderer main function is to project into an set of images the terrain data
represented by de cache and the model. It uses a deferred shading approach
exposing different output buffers. The buffers that can be generated are: 

* terrain\_data: Normal and height.

* texture\_coords: Texture coordinates to an wang tile atlas.

* texture\_coords_dF: Screen space partial derivatives of the
  texture coordinates.

* depth: depth buffer provided by the graphics API. 

* stencil: stencil buffer provided by the graphics API. 

* depth\_stencil: A combination of the depth and the stencil buffers.

Not always all outputs will be useful. TerrainRenderer can be created with a
enum class template parameter representing a subset of outputs. Since outputs
are determined at compile time, any output not used will disable parts of the
implementation not required using meta-programming techniques.

## Build

### Tools

* g++. Any version that fully support C++14.
* nvcc.

### Dependencies

* OpenGL 4.5
* Cuda 8
* GLFW3
* GLM
