
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include <CudaGLSurfaceObject.hpp>

CudaGLSurfaceObject::CudaGLSurfaceObject() {
    object_ = 0;
}

CudaGLSurfaceObject::~CudaGLSurfaceObject() {}

bool CudaGLSurfaceObject::Register(GLuint id, GLenum target, GLuint layer) {
    cudaError error;

    cudaGraphicsResource_t resource;
    error = cudaGraphicsGLRegisterImage
                (&resource, id, target, 
                 cudaGraphicsRegisterFlagsSurfaceLoadStore);
    if (error != cudaSuccess) return false;

    error = cudaGraphicsMapResources(1, &resource);
    if (error != cudaSuccess) return false;

    cudaArray_t texture_array;
    error = 
        cudaGraphicsSubResourceGetMappedArray(&texture_array, 
                                              resource, layer, 0); 
    if (error != cudaSuccess) return false;

    cudaResourceDesc resDesc = {};
    resDesc.resType = cudaResourceTypeArray;
    resDesc.res.array.array = texture_array;

    error = cudaCreateSurfaceObject(&object_, &resDesc);
    if (error != cudaSuccess) return false;

    error = cudaGraphicsUnmapResources(1, &resource, 0);
    if (error != cudaSuccess) return false;

    return true;
} 

const cudaSurfaceObject_t& CudaGLSurfaceObject::object() const {
    return object_;
}

bool CudaGLSurfaceObject::good() const {
    return object_ != 0;
}
