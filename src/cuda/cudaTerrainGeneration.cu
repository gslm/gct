#include <cudaTerrainGeneration.h>

#include <cuda_runtime.h>

#include <helper_math.h>
#include <cudaNoise.h>

#include <iostream>

namespace gct {

__global__
void cuGenerateFbmKernel
        (cudaSurfaceObject_t surf, unsigned surf_layer,
         int2 coords, int2 rect_dim, int2 surf_dim,
         float octaves, float period, float amplitude, float lacunarity, float invH) {
    const int2 tid = rect_dim.x > rect_dim.y 
                   ? make_int2(threadIdx.x + blockIdx.x * blockDim.x,
                               threadIdx.y + blockIdx.y * blockDim.y) 
                   : make_int2(threadIdx.y + blockIdx.x * blockDim.y,
                               threadIdx.x + blockIdx.y * blockDim.x);

/* 
    const int2 tid = make_int2(threadIdx.x + blockIdx.x * blockDim.x,
                               threadIdx.y + blockIdx.y * blockDim.y);
*/
    if (tid.x > (rect_dim.x - 1) || tid.y > (rect_dim.y - 1)) return; 

	const int2 tex_coord = tid + coords;

    const float scale = float(1 << surf_layer);
    
    const float2 p = make_float2(tex_coord.x, tex_coord.y) * scale * period;
	float h = cuFbm(p, octaves, amplitude, lacunarity, invH);

	surf2Dwrite(make_float4(0.0, 0.0, 0.0, h),
		               surf,
		               (tex_coord.x & (surf_dim.x - 1)) * sizeof(float4),
				        tex_coord.y & (surf_dim.y - 1));
}

__global__
void cuComputeNormalKernel
    (cudaSurfaceObject_t surf, unsigned surf_layer, int2 coords, int2 rect_dim, int2 surf_dim) {
    const int2 tid = rect_dim.x > rect_dim.y 
                   ? make_int2(threadIdx.x + blockIdx.x * blockDim.x,
                               threadIdx.y + blockIdx.y * blockDim.y) 
                   : make_int2(threadIdx.y + blockIdx.x * blockDim.y,
                               threadIdx.x + blockIdx.y * blockDim.x);   
    if (tid.x > (rect_dim.x - 1) || tid.y > (rect_dim.y - 1)) return;

    const int2 tex_coord = tid + coords;

    const float hl = surf2Dread<float4>(surf, 
               ((tex_coord.x - 1) & (surf_dim.x - 1)) * sizeof(float4), 
               tex_coord.y & (surf_dim.y - 1)).w;

	const float hr = surf2Dread<float4>(surf, 
               ((tex_coord.x + 1) & (surf_dim.x - 1)) * sizeof(float4), 
               tex_coord.y & (surf_dim.y - 1)).w;

    const float hb = surf2Dread<float4>(surf, 
               (tex_coord.x  & (surf_dim.x - 1)) * sizeof(float4), 
               (tex_coord.y - 1) & (surf_dim.y - 1)).w;

	const float ht = surf2Dread<float4>(surf, 
               (tex_coord.x  & (surf_dim.x - 1)) * sizeof(float4), 
               (tex_coord.y + 1) & (surf_dim.y - 1)).w;


    const float s = float(2 << surf_layer);
	//const float3 vx = make_float3(s, hr - hl, 0.0);
    //const float3 vz =  make_float3(0.0, ht - hb, s);  

    const float3 n = normalize(make_float3(hr-hl, -s, hb-ht));

	float h = surf2Dread<float4>(surf, 
               (tex_coord.x & (surf_dim.x - 1)) * sizeof(float4), 
               tex_coord.y & (surf_dim.y - 1)).w;
    
	surf2Dwrite(make_float4(n.x, n.y, n.z, h),
		        surf,
		        (tex_coord.x & (surf_dim.x - 1)) * sizeof(float4),
				tex_coord.y & (surf_dim.y - 1));
}

void cudaDeviceGenerateRectangleHeight
        (const CudaGLSurfaceObject& surf, 
        unsigned surf_layer,
        int rect_coords_x, int rect_coords_y, 
        unsigned rect_dim_x, unsigned rect_dim_y,
        unsigned surf_dim_x, unsigned surf_dim_y, unsigned surf_dim_z,
        int octaves, float period, float amplitude, 
        float lacunarity, float invH) {
 
    if (rect_dim_x == 0 || rect_dim_y == 0) return;

    constexpr unsigned log2_threads = 4;

    dim3 threads(1 << log2_threads, 1 << log2_threads);
    dim3 blocks((rect_dim_x >> log2_threads) + 1, (rect_dim_y >> log2_threads) + 1);
    cuGenerateFbmKernel<<<blocks, threads>>>
                (surf.object(), surf_layer,
                 make_int2(rect_coords_x, rect_coords_y), 
                 make_int2(rect_dim_x, rect_dim_y), 
                 make_int2(surf_dim_x, surf_dim_y), 
                 octaves, period, amplitude, lacunarity, invH);
}

void cudaDeviceGenerateRectangleNormal
        (const CudaGLSurfaceObject& surf, 
        unsigned surf_layer, 
        int rect_coords_x, int rect_coords_y,
        unsigned rect_dim_x, unsigned rect_dim_y,
        unsigned surf_dim_x, unsigned surf_dim_y, unsigned surf_dim_z) {
 
    if (rect_dim_x == 0 || rect_dim_y == 0) return;

    constexpr unsigned log2_threads = 3;

    dim3 threads(1 << log2_threads, 1 << log2_threads);
    dim3 blocks((rect_dim_x >> log2_threads) + 1, (rect_dim_y >> log2_threads) + 1);

    cuComputeNormalKernel<<<blocks, threads>>>
                (surf.object(), surf_layer, 
                 make_int2(rect_coords_x, rect_coords_y),
                 make_int2(rect_dim_x, rect_dim_y), 
                 make_int2(surf_dim_x, surf_dim_y));
}

void cuSynchronize() {
    cudaDeviceSynchronize();
}

void cudaHostGeneratePos(float x, float y,
        int octaves, float period, float amplitude, 
        float lacunarity, float invH,
        float& nx, float& ny, float& nz, float& h) {
    h = cuFbm(make_float2(x, y) * period, 
              octaves, amplitude, lacunarity, invH);

    const float epsilon = 0.01;
    const float hl = cuFbm(make_float2(x + epsilon, y), octaves, amplitude, lacunarity, invH);
	const float hr = cuFbm(make_float2(x - epsilon, y), octaves, amplitude, lacunarity, invH);
	const float ht = cuFbm(make_float2(x, y + epsilon), octaves, amplitude, lacunarity, invH);
	const float hb = cuFbm(make_float2(x, y - epsilon), octaves, amplitude, lacunarity, invH);

    const float3 vx =  make_float3(x + epsilon, hl, y) - 
                       make_float3(x - epsilon, hr, y);

    const float3 vy = make_float3(x, ht, y + epsilon) - 
                      make_float3(x, hb, y - epsilon);

    const float3 n = normalize(cross(vx, vy));

    nx = n.x;
    ny = n.y;
    nz = n.z;
}

}
