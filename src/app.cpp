//#define _GLIBCXX_USE_CXX11_ABI 0

#include <OpenImageIO/imageio.h>

#include <glfwapp/GLFWApp.hpp>
//#include <stgl/OpenGLTexture.hpp>

#include <OpenGLTerrainModel.hpp>
#include <OpenGLTerrainRenderer.hpp>
#include <CudaGLTerrainDataCache.hpp>
#include <RenderOutput.hpp>
#include <Lod.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <OpenGLPostRenderer.hpp>

#include <Histogram.hpp>

#include <stgl/OpenGLDebug.hpp>

#include <iostream>
#include <chrono>
#include <limits>

struct AppLogic {

    stgl::OpenGLTexture<GL_RGBA8, GL_TEXTURE_2D_ARRAY> wang_atlas_;

    stgl::OpenGLTexture<GL_RGB16F, GL_TEXTURE_2D> texture_coords_;
    stgl::OpenGLTexture<GL_RGBA16F, GL_TEXTURE_2D> texture_coords_dF_;
    stgl::OpenGLTexture<GL_RGBA32F, GL_TEXTURE_2D> terrain_data_;
    stgl::OpenGLTexture<GL_DEPTH32F_STENCIL8, GL_TEXTURE_2D> depth_stencil_;

    using GeometryRenderer =
    gct::OpenGLTerrainRenderer<gct::RenderOutput::texture_coords    |
                               gct::RenderOutput::texture_coords_dF |
                               gct::RenderOutput::terrain_data      |
                               gct::RenderOutput::depth_stencil     |
                               gct::RenderOutput::none>;
    GeometryRenderer geometry_pass_;
    GeometryRenderer::Framebuffer geometry_framebuffer_;

    OpenGLPostRenderer post_pass_;

    gct::OpenGLTerrainModel model_;
    gct::CudaGLTerrainDataCache<8> cache_;

    float velocity;
    glm::vec3 delta_camera_pos;
    glm::vec3 camera_pos;
    glm::quat camera_rot;

    glm::mat4 projection;
    glm::mat4 inv_projection;

    double tmin, tmax, ttotal, frames;

    Histogram<double> hist;

    template<size_t E> 
    using cache_filter = gct::OrderedIndexingSetFilter<E, true>;

    AppLogic(int width, int height)
        : texture_coords_(width, height)
        , texture_coords_dF_(width, height)
        , terrain_data_(width, height)
        , depth_stencil_(width, height)
        , geometry_framebuffer_(texture_coords_, texture_coords_dF_, terrain_data_, depth_stencil_)
        , post_pass_(texture_coords_, texture_coords_dF_, terrain_data_, depth_stencil_)
        , model_(256)
        , cache_(256, 256) {
            if (not geometry_framebuffer_.good()) {
                std::cout << "[ERROR] :: An error ocurred on the ";
                std::cout << "creation of the framebuffer." << std::endl;
            }

            if (not post_pass_.good()) {
                std::cout << "[ERROR] :: An error ocurred on the ";
                std::cout << "creation of the light pass." << std::endl;
            }
        }

    void Start() {
        tmin = std::numeric_limits<float>::max();
        tmax = 0.0f;
        ttotal = 0.0f;
        frames = 0.0f;

        hist = Histogram<double>(0.0, 0.02, 20);  

        STGL_OPENGL_ERROR_DEBUG;
         
        glViewport(0, 0, terrain_data_.width(), terrain_data_.height()); 
       
//        glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
//        glDepthMask( GL_TRUE );
/*
        terrain_data_.SetParameter<GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE>();
        terrain_data_.SetParameter<GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE>();
        terrain_data_.SetParameter<GL_TEXTURE_MAG_FILTER, GL_NEAREST>();
        terrain_data_.SetParameter<GL_TEXTURE_MIN_FILTER, GL_NEAREST>();

        STGL_OPENGL_ERROR_DEBUG;
        texture_coords_.SetParameter<GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE>();
        texture_coords_.SetParameter<GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE>();
        texture_coords_.SetParameter<GL_TEXTURE_MAG_FILTER, GL_NEAREST>();
        texture_coords_.SetParameter<GL_TEXTURE_MIN_FILTER, GL_NEAREST>();

        STGL_OPENGL_ERROR_DEBUG;
        depth_stencil_.SetParameter<GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE>();
        depth_stencil_.SetParameter<GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE>();
        depth_stencil_.SetParameter<GL_TEXTURE_MAG_FILTER, GL_NEAREST>();
        depth_stencil_.SetParameter<GL_TEXTURE_MIN_FILTER, GL_NEAREST>();
*/
        geometry_pass_.set_wang_tiles(1, 1, 2);
        
        LoadWangTiles("./resources/wang_tile_set.png");
        post_pass_.set_wang_tile_atlas(wang_atlas_);
       
        STGL_OPENGL_ERROR_DEBUG;
        velocity = 1000.0f;
        delta_camera_pos = glm::vec3(0.0f);
        camera_pos = glm::vec3(0.0f, 200.0f, 0.0f);
        camera_rot = glm::quat();

        float aspect = float(terrain_data_.width()) / float(terrain_data_.height());
        projection = glm::perspective(45.0f, aspect, 1.0f, 10000.0f);

        float near = 1.0f;
        float right = near/projection[0][0];
        float top = near/projection[1][1];

        projection = glm::frustum(-right, right, -top, top, 1.0f, 10000.0f);
        post_pass_.set_camera_frustum(-right, right, -top, top, 1.0f, 10000.0f);

        inv_projection = glm::inverse(projection);
        
        cache_.SetGenParams(2000.0f, 1.0f/8000.0f, 2.0f, 0.7f, 1.0f, 1.0f, 16, 0);
        cache_.Init(camera_pos.x, camera_pos.z, 0);

        auto h = cache_.GetDataAt(glm::vec2(camera_pos.x, camera_pos.z)).a;
        camera_pos.y = h + 10.0f;

        STGL_OPENGL_ERROR_DEBUG;
    }

    bool Loop(const glfwapp::OpenGLApp<AppLogic>::InputT& input,
              const glfwapp::OpenGLApp<AppLogic>::TimeT& time) {

        if (input.key('R')) {
            std::cout << "##################################\n";
            std::cout << "#\n";
            std::cout << "# tmin = " << tmin << "s\n" ;
            std::cout << "# tavg = " << ttotal / frames << "s\n";
            std::cout << "# tmax = " << tmax << "s\n" ;
            std::cout << "#\n";
            std::cout << "##################################" << std::endl;

            tmin = std::numeric_limits<float>::max();
            tmax = 0.0f;
            ttotal = 0.0f;
            frames = 0.0f;
        }
        //if (not geometry_pass_.good()) return true;
        
        auto t1 = std::chrono::steady_clock::now();

        auto rotate_camera = [&](const glm::vec2& m) {
            camera_rot =
                glm::quat(glm::vec3(0.0f, -m.x, 0.0f) * 0.001f) *
                glm::quat(glm::vec3(m.y, 0.0f, 0.0f) * 0.001f);
        };

        auto translate_camera = [&](const glm::vec2& a) {
            const glm::vec3 forward = camera_rot * glm::vec3(0.0, 0.0, -1.0);
            const glm::vec3 right = camera_rot * glm::vec3(1.0, 0.0, 0.0);

            const glm::vec3 d = forward*a.y + right*a.x;

            delta_camera_pos = d * velocity * float(time.delta());

            camera_pos += delta_camera_pos;
        };

        using InputT = glfwapp::OpenGLApp<AppLogic>::InputT;
        glm::vec2 a = glm::vec2(input.key('D') - input.key('A'),
                                input.key('W') - input.key('S'));

        
        glm::vec2 m = glm::vec2(input.mouse(InputT::Axis::X),
                                input.mouse(InputT::Axis::Y));

        rotate_camera(m);
        translate_camera(a);

        // basic collisions
        float h = cache_.GetDataAt(glm::vec2(camera_pos.x, camera_pos.z)).a;
        camera_pos.y = std::max(h + 2.0f, camera_pos.y);
/*
        unsigned lod_h = gct::compute_lod(45.0f, float(terrain_data_.height()),
                                        std::abs(camera_pos.y - h), 4.0f);

        unsigned lod_v = gct::compute_lod(glm::length(
                                              glm::vec2(delta_camera_pos.x,
                                                        delta_camera_pos.z)),
                                          2.0f);
*/
        //cache_.Lod(glm::max(lod_h, lod_v));
        cache_.Translate(delta_camera_pos.x, delta_camera_pos.z);
        //cache_.Init(camera_pos.x, camera_pos.z, 0);
        glm::vec2 ct = glm::vec2(input.key('J') - input.key('L'),
                                 input.key('I') - input.key('K')) * 
                       100.0f * float(time.delta());

        //cache_.Translate(ct.x, ct.y);

        const glm::mat4 r = glm::mat4_cast(glm::conjugate(camera_rot));
        const glm::mat4 mvp_matrix = projection * glm::translate(r, -camera_pos);

        geometry_pass_.set_mvp_matrix(mvp_matrix);
        post_pass_.set_camera_transform(glm::translate(
                                            glm::mat4_cast(camera_rot), camera_pos));

        if (input.key('P')) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
       
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
        glStencilFunc(GL_ALWAYS, 1, 1);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
 
        geometry_framebuffer_.Clear<gct::RenderOutput::terrain_data>(
                std::array<float,4>{{1.0f, 0.0f, 0.0f, 0.0f}});
        geometry_framebuffer_.Clear<gct::RenderOutput::texture_coords>(
                std::array<float,4>{{1.0f, 0.0f, 0.0f, 0.0f}});
        geometry_framebuffer_.Clear<gct::RenderOutput::texture_coords_dF>(
                std::array<float,4>{{1.0f, 0.0f, 0.0f, 0.0f}});
        geometry_framebuffer_.Clear<gct::RenderOutput::depth_stencil>(1.0f, 0);

        geometry_pass_.Render(model_, cache_.make_view(), geometry_framebuffer_);

//        STGL_OPENGL_ERROR_DEBUG;
//      glFinish();

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        
        if (input.key('M')) post_pass_.TerrainDebugPass();
        else post_pass_.TerrainShadingPass();

        post_pass_.DisplayPass();

        glFinish();
        auto t2 = std::chrono::steady_clock::now();
        auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

        hist.insert(time_span.count());

        ++frames;
        ttotal += time_span.count();
        tmin = std::min(tmin, time_span.count());
        tmax = std::max(tmax, time_span.count());

        return input.key('Q');

        STGL_OPENGL_ERROR_DEBUG;
    }

    void End() {
        std::cout << "##################################\n";
        std::cout << "#\n";
        std::cout << "# tmin = " << tmin << "s\n" ;
        std::cout << "# tavg = " << ttotal / frames << "s\n";
        std::cout << "# tmax = " << tmax << "s\n" ;
        std::cout << "#\n";
        std::cout << "##################################" << std::endl;

        hist.print();
    }

private:

    bool LoadWangTiles(const std::string& path) {
        using WangTileAtlas_t = stgl::OpenGLTexture<GL_RGBA8, GL_TEXTURE_2D_ARRAY>;
        
        auto set_error_data = [&]() {
            std::array<uint8_t, 8> error_data({{255, 0, 255, 255}});
            wang_atlas_ = WangTileAtlas_t(1, 1, 1, 1);
            wang_atlas_.SubImage<GL_RGBA, GL_UNSIGNED_BYTE>
                    (0, 0, 0, 0, 1, 1, 1, error_data.data());
        };

        auto in = OIIO::ImageInput::open(path.data()); 
        if (not in) {
            set_error_data();
            return false;
        }

        const OIIO::ImageSpec spec = in->spec();

        size_t image_bytes = spec.height*spec.width*spec.nchannels;
        std::vector<uint8_t> buffer(image_bytes);
        
        bool read = in->read_image(OIIO::TypeDesc::UINT8, buffer.data());
        in->close();

        OIIO::ImageInput::destroy(in);

        if (spec.height < 1 or spec.width < 1 or spec.nchannels > 4 or not read) {
            set_error_data();
            return false;
        }

        std::vector<uint8_t> tex_buffer(spec.height*spec.width*4, 255);
        for (int i = 0; i < spec.height*spec.width; ++i) {
            for (int c = 0; c < spec.nchannels; ++c) {
                tex_buffer[i*4 + c] = buffer[i*spec.nchannels + c];
            }
        } 

        int levels = std::log2(spec.width);
        wang_atlas_ = WangTileAtlas_t(spec.width, spec.width, spec.height/spec.width, levels);

        wang_atlas_.SubImage<GL_RGBA, GL_UNSIGNED_BYTE>(0, 
                                    0, 0, 0,
                                    spec.width, spec.width, spec.height/spec.width,
                                    tex_buffer.data());

        wang_atlas_.SetParameter<GL_TEXTURE_WRAP_S, GL_REPEAT>();
        wang_atlas_.SetParameter<GL_TEXTURE_WRAP_T, GL_REPEAT>();
        wang_atlas_.SetParameter<GL_TEXTURE_MAG_FILTER, GL_LINEAR>();
        wang_atlas_.SetParameter<GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR>(); 

        wang_atlas_.SetParameter<GL_TEXTURE_MIN_LOD>(0.0); 
        wang_atlas_.SetParameter<GL_TEXTURE_MAX_LOD>(float(levels) + 1.0);
        wang_atlas_.SetParameter<GL_TEXTURE_BASE_LEVEL>(0);

        wang_atlas_.GenerateMipmap();
        return true;
    }
};

int main() {
    glfwapp::OpenGLApp<AppLogic> app(1920, 1080, "App", false);
    return app.Run();
}
