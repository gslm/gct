#ifndef GLFW_APP_HPP
#define GLFW_APP_HPP

#include <GL/gl3w.h>

#define GLFW_INCLUDE_GLCOREARB
#include "GLFW/glfw3.h"

#include "Input.hpp"
#include "Time.hpp"

namespace glfwapp {

class GLFWApp {
    static int instances;

public:
    GLFWApp(const GLFWApp&) = delete;
    GLFWApp operator=(const GLFWApp&) = delete;

    bool good() { return instances >= 0; }

protected:
    GLFWApp() {
        if (instances < 0) {
            if (not glfwInit()) --instances;
        }

        ++instances;
    }

    ~GLFWApp() {
        --instances;

        if (instances == 0) {
            glfwTerminate();
            instances = -1;
        }
    }
};

int GLFWApp::instances{-1};

template<class AppLogic>
class OpenGLApp : public GLFWApp {
public:
    using InputT = Input;
    using TimeT = Time;

    OpenGLApp(unsigned width, unsigned height, const char* title, bool fullscreen = false);
    ~OpenGLApp();

    bool good();

    int Run();

private:
    GLFWwindow* window;
    
    InputT input;

    bool CreateWindow(unsigned width, unsigned height, 
                      const char* title, bool fullscreen = false);
    bool SetOpenGLContext();
    void SetInputCallbacks();
};


#include "GLFWApp.inl"

}

#endif
