#ifndef GLFWAPP_TIME_HPP
#define GLFWAPP_TIME_HPP

namespace glfwapp {

class Time {
    template<class>
    friend class OpenGLApp;

public:
    Time() : time_(0.0), delta_(0.0), frames_(0) {}

    double time() const {
        return time_;
    }

    double delta() const {
        return delta_;
    }
    
    int frames() const {
        return frames_;
    }

private: 
    double time_;
    double delta_;
    int frames_;    

    void Update(double t) {
        delta_ = t - time_;
        time_ = t;
        ++frames_;
    }
};

}
#endif


