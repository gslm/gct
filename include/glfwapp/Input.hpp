#ifndef GLFW_APP_INPUT_HPP
#define GLFW_APP_INPUT_HPP

#include <limits>

#include <iostream>
namespace glfwapp {

class Input {
    template<class>
    friend class OpenGLApp;

public:
    enum class Axis : size_t {X = 0, Y = 1, Size};
    enum class MouseButton : size_t {R = 0, L = 1, M = 2, Size};

    double axis(Axis a) const {
        if (a == Axis::X) return double(key('D')) - double(key('A')); 
        if (a == Axis::Y) return double(key('W')) - double(key('S'));
        
        return 0.0;//axis_[static_cast<size_t>(a)];
    }

    double mouse(Axis a) const {
        return mouse_axis_[static_cast<size_t>(a)];
    }

    double mouse(MouseButton b) const {
        return mouse_buttons_[static_cast<size_t>(b)];
    }

    double scrollwheel(Axis a) const {
        return scrollwheel_[static_cast<size_t>(a)]; 
    }

    bool key(char k) const {
        //std::cout << keys_[k] << std::endl;
        return keys_[static_cast<size_t>(k)];
    }

private: 
    //double axis_[static_cast<size_t>(Axis::Size)];
    double mouse_axis_[static_cast<size_t>(Axis::Size)];
    double scrollwheel_[static_cast<size_t>(Axis::Size)];
    bool mouse_buttons_[static_cast<size_t>(MouseButton::Size)];
    
    bool keys_[std::numeric_limits<char>::max() - 
               std::numeric_limits<char>::min() + 1];

    void mouse(double xpos, double ypos) {
        mouse_axis_[static_cast<size_t>(Axis::X)] = xpos;
        mouse_axis_[static_cast<size_t>(Axis::Y)] = ypos;
    }

    void mouse(int button, int action, int mods) {
        
    }

    void scrollwheel(double xoffset, double yoffset) {
        scrollwheel_[static_cast<size_t>(Axis::X)] += xoffset;
        scrollwheel_[static_cast<size_t>(Axis::Y)] += yoffset;
    }

    void key(int key, int scancode, int action, int mods) {
        keys_[static_cast<size_t>(key)] = action > 0;
    }

    double& mouse(Axis a) {
        return mouse_axis_[static_cast<size_t>(a)];
    }

    bool& mouse(MouseButton b) {
        return mouse_buttons_[static_cast<size_t>(b)];
    }

    double& scrollwheel(Axis a) {
        return scrollwheel_[static_cast<size_t>(a)]; 
    }

    bool& key(char k) {
        return keys_[static_cast<size_t>(k)];
    }
};

}
#endif
