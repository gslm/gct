
template<class AppLogic>
bool OpenGLApp<AppLogic>::CreateWindow(unsigned width, unsigned height, 
                                       const char* title, bool fullscreen) 
{
    window = nullptr;
    
    if (not GLFWApp::good()) return false;
   
    if (fullscreen) window = glfwCreateWindow(width, height, title, 
                                                     glfwGetPrimaryMonitor(), NULL);
    else window = glfwCreateWindow(width, height, title, NULL, NULL);

    return window != nullptr;
}

template<class AppLogic>
bool OpenGLApp<AppLogic>::SetOpenGLContext() 
{
    glfwMakeContextCurrent(window);

    if (gl3wInit() == -1) return false;
    if (not gl3wIsSupported(4, 5)) return false;
    
    return true;
}

template<class AppLogic>
void OpenGLApp<AppLogic>::SetInputCallbacks()
{

    glfwSetKeyCallback(window, 
        [](GLFWwindow* window, 
            int key, int scancode, int action, int mods) -> void {
            static_cast<decltype(this)>(glfwGetWindowUserPointer(window))->
                input.key(key, scancode, action, mods);
        }
    );

    glfwSetCursorPosCallback(window,
        [](GLFWwindow* window, double xpos, double ypos) -> void { 
            static_cast<decltype(this)>(glfwGetWindowUserPointer(window))->
                input.mouse(xpos, ypos);
        }
    );

    glfwSetScrollCallback(window,
        [](GLFWwindow* window, double xoffset, double yoffset) -> void {
            static_cast<decltype(this)>(glfwGetWindowUserPointer(window))->
                input.scrollwheel(xoffset, yoffset);
        }
    );
    
    glfwSetMouseButtonCallback(window,
        [](GLFWwindow* window, int button, int action, int mods) -> void {
            static_cast<decltype(this)>(glfwGetWindowUserPointer(window))->
                input.mouse(button, action, mods);
        }
    );

}

template<class AppLogic>
OpenGLApp<AppLogic>::OpenGLApp(unsigned width, unsigned height, const char* title, bool fullscreen) 
{
    CreateWindow(width, height, title, fullscreen);
    if (window and not SetOpenGLContext()) window = nullptr;
    if (window) {
        glfwSetWindowUserPointer(window, this);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        SetInputCallbacks();
    }
}

template<class AppLogic>
OpenGLApp<AppLogic>::~OpenGLApp() 
{
    //glfwTerminate();
}

template<class AppLogic>
bool OpenGLApp<AppLogic>::good() 
{
    return window != nullptr;
}

template<class AppLogic>
int OpenGLApp<AppLogic>::Run() 
{
    if (not good()) return -1;

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    AppLogic app_logic(width, height);
    
    app_logic.Start();
    
    Time time;
    double start_time = glfwGetTime(); 

    bool should_close = glfwWindowShouldClose(window); 
    while (not should_close) {
        time.Update(glfwGetTime() - start_time); 
        
        should_close = app_logic.Loop(input, time);
        glfwSwapBuffers(window);
        glfwPollEvents();

        should_close = should_close or glfwWindowShouldClose(window);
    }

    app_logic.End();

    return 0;
}
