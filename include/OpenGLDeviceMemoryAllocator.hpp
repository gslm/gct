#ifndef GCT_OPENGL_DEVICE_MEMORY_ALLOCATOR_HPP
#define GCT_OPENGL_DEVICE_MEMORY_ALLOCATOR_HPP

#include <array>
#include <vector>

#include <stgl/OpenGLBuffer.hpp>

namespace gct {

template<class IndexT, class Vec2T> 
class OpenGLDeviceMemoryAllocator {

    stgl::OpenGLBuffer<Vec2T> vertices_;
    stgl::OpenGLBuffer<IndexT> indices_;

    stgl::OpenGLBufferView<Vec2T> block_vertices_;
    stgl::OpenGLBufferView<Vec2T> fixup_vertices_;
    stgl::OpenGLBufferView<Vec2T> trans_vertices_;
    std::array<stgl::OpenGLBufferView<Vec2T>, 4> trims_vertices_; 

    stgl::OpenGLBufferView<IndexT> block_indices_;
    stgl::OpenGLBufferView<IndexT> fixup_indices_;
    stgl::OpenGLBufferView<IndexT> trans_indices_;
    stgl::OpenGLBufferView<IndexT> trims_indices_;

public:
    OpenGLDeviceMemoryAllocator(const std::vector<Vec2T>& block_vertices,
                                const std::vector<IndexT>& block_indices,
                                const std::vector<Vec2T>& fixup_vertices,
                                const std::vector<IndexT>& fixup_indices,
                                const std::vector<Vec2T>& trans_vertices,
                                const std::vector<IndexT>& trans_indices, 
                                const std::array<std::vector<Vec2T>, 4>& trims_vertices,
                                const std::vector<IndexT>& trims_indices)
        : vertices_(make_vertices(block_vertices, fixup_vertices, trans_vertices,
                                 trims_vertices))
        , indices_(make_indices(block_indices, fixup_indices, trans_indices,
                                trims_indices)) { 
       
        using VerticesView = stgl::OpenGLBufferView<Vec2T>;
        using IndicesView = stgl::OpenGLBufferView<IndexT>;
        
        size_t voffset = 0;
        
        block_vertices_ = VerticesView(vertices_, block_vertices.size(), 0, voffset);
        voffset += block_vertices.size();
        
        fixup_vertices_ = VerticesView(vertices_, fixup_vertices.size(), 0, voffset);
        voffset += fixup_vertices.size(); 
        
        trans_vertices_ = VerticesView(vertices_, trans_vertices.size(), 0, voffset);
        voffset += trans_vertices.size();        

        for (size_t v = 0; v < trims_vertices.size(); ++v) {
            trims_vertices_[v] =  
                VerticesView(vertices_, trims_vertices[v].size(), 0, voffset);
            voffset += trims_vertices[v].size();
        }

        size_t ioffset = 0;

        block_indices_ = IndicesView(indices_, block_indices.size(), 0, ioffset);
        ioffset += block_indices.size();

        fixup_indices_ = IndicesView(indices_, fixup_indices.size(), 0, ioffset);
        ioffset += fixup_indices.size();

        trans_indices_ = IndicesView(indices_, trans_indices.size(), 0, ioffset);
        ioffset += trans_indices.size();
    
        trims_indices_ = IndicesView(indices_, trims_indices.size(), 0, ioffset);
    }

    const stgl::OpenGLBuffer<Vec2T>& vertices_buffer() const {
        return vertices_;
    }

    const stgl::OpenGLBuffer<IndexT>& indices_buffer() const {
        return indices_;
    }
 
    const stgl::OpenGLBufferView<Vec2T>& block_vertices_dev_ptr() const {
        return block_vertices_;
    }

    const stgl::OpenGLBufferView<Vec2T>& fixup_vertices_dev_ptr() const {
        return fixup_vertices_;
    }

    const stgl::OpenGLBufferView<Vec2T>& trans_vertices_dev_ptr() const {
        return trans_vertices_;
    }

    const stgl::OpenGLBufferView<Vec2T>& trims_vertices_dev_ptr(size_t i) const {
        return trims_vertices_[i];
    }
   
    const stgl::OpenGLBufferView<IndexT>& block_indices_dev_ptr() const {
        return block_indices_;
    }

    const stgl::OpenGLBufferView<IndexT>& fixup_indices_dev_ptr() const {
        return fixup_indices_;
    }

    const stgl::OpenGLBufferView<IndexT>& trans_indices_dev_ptr() const {
        return trans_indices_;
    }

    const stgl::OpenGLBufferView<IndexT>& trims_indices_dev_ptr() const {
        return trims_indices_;
    }

private:

    static inline
    stgl::OpenGLBuffer<Vec2T> make_vertices(
            const std::vector<Vec2T>& block_vertices, 
            const std::vector<Vec2T>& fixup_vertices, 
            const std::vector<Vec2T>& trans_vertices,
            const std::array<std::vector<Vec2T>, 4>& trims_vertices) {
        std::vector<Vec2T> vertices_data(block_vertices.size() + 
                                              fixup_vertices.size() +
                                              trans_vertices.size() + 
                                              trims_vertices[0].size() + 
                                              trims_vertices[1].size() +
                                              trims_vertices[2].size() + 
                                              trims_vertices[3].size());
 
        auto offset_it = vertices_data.begin(); 
        offset_it = std::copy(block_vertices.begin(), block_vertices.end(), offset_it);
        offset_it = std::copy(fixup_vertices.begin(), fixup_vertices.end(), offset_it);
        offset_it = std::copy(trans_vertices.begin(), trans_vertices.end(), offset_it);

        for (const auto& v : trims_vertices) {
            offset_it = std::copy(v.begin(), v.end(), offset_it);
        }

        return stgl::OpenGLBuffer<Vec2T>(0, vertices_data.size(), vertices_data.data());
    }

    static inline
    stgl::OpenGLBuffer<IndexT> make_indices(
            const std::vector<IndexT>& block_indices,
            const std::vector<IndexT>& fixup_indices,
            const std::vector<IndexT>& trans_indices,
            const std::vector<IndexT>& trims_indices) {
        std::vector<IndexT> indices_data(block_indices.size() + fixup_indices.size() +
                                         trans_indices.size() + trims_indices.size());

        auto offset_it = indices_data.begin();
        offset_it = std::copy(block_indices.begin(), block_indices.end(), offset_it);
        offset_it = std::copy(fixup_indices.begin(), fixup_indices.end(), offset_it);
        offset_it = std::copy(trans_indices.begin(), trans_indices.end(), offset_it);
        offset_it = std::copy(trims_indices.begin(), trims_indices.end(), offset_it);
        return stgl::OpenGLBuffer<IndexT>(0, indices_data.size(), indices_data.data()); 
    }
};

}
#endif
