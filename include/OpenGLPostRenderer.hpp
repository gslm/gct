#ifndef OPENGL_POST_RENDERER_GLSL_HPP
#define OPENGL_POST_RENDERER_GLSL_HPP

#include <OpenGLTerrainShadingSource.glsl.hpp>
#include <OpenGLTerrainDebugSource.glsl.hpp>
#include <OpenGLSkySource.glsl.hpp>
#include <OpenGLDisplaySource.glsl.hpp>

class OpenGLPostRenderer {
    stgl::OpenGLFramebuffer framebuffer_;    
    stgl::OpenGLTexture<GL_RGB8, GL_TEXTURE_2D> output_;

    stgl::OpenGLProgram<OpenGLTerrainShadingSource> terrain_shading_program_;
    stgl::OpenGLProgram<OpenGLTerrainDebugSource> terrain_debug_program_;
    stgl::OpenGLProgram<OpenGLSkySource> sky_program_;
    stgl::OpenGLProgram<OpenGLDisplaySource> display_program_;

    stgl::OpenGLBuffer<glm::vec2> vertices_;
    stgl::OpenGLBuffer<uint8_t> indices_;

public:
    template<GLenum TexCoordsFormat, GLenum TexCoordsDFFormat,
             GLenum TerrainDataFormat, GLenum DepthStencilFormat>
    OpenGLPostRenderer(
            const stgl::OpenGLTexture<TexCoordsFormat, GL_TEXTURE_2D>& texture_coords,
            const stgl::OpenGLTexture<TexCoordsDFFormat, GL_TEXTURE_2D>& texture_coords_dF,
            const stgl::OpenGLTexture<TerrainDataFormat, GL_TEXTURE_2D>& terrain_data,
            const stgl::OpenGLTexture<DepthStencilFormat, GL_TEXTURE_2D>& depth_stencil)
        : output_(terrain_data.width(), terrain_data.height())
        , vertices_(0, {glm::vec2(-1.0f, -1.0f), glm::vec2(1.0f, -1.0f),
                        glm::vec2(-1.0f, 1.0f), glm::vec2(1.0f, 1.0f)})
        , indices_(0, {0, 1, 2, 3}) 
    {
        framebuffer_.AttachTexture<GL_DEPTH_STENCIL_ATTACHMENT>(depth_stencil);
        framebuffer_.AttachTexture<GL_COLOR_ATTACHMENT0>(output_);
        framebuffer_.DrawBuffers(std::array<GLuint, 1>{{GL_COLOR_ATTACHMENT0}}); 

        terrain_shading_program_.set_texture_coords_buffer(texture_coords);
        terrain_debug_program_.set_texture_coords_buffer(texture_coords);
        terrain_shading_program_.set_texture_coords_dF_buffer(texture_coords_dF);
        terrain_debug_program_.set_texture_coords_dF_buffer(texture_coords_dF);
        terrain_shading_program_.set_terrain_data_buffer(terrain_data);
        terrain_debug_program_.set_terrain_data_buffer(terrain_data);
        terrain_shading_program_.set_depth_buffer(depth_stencil);
        display_program_.set_output_buffer(output_);
    }

    template<GLenum TexFormat>
    void set_wang_tile_atlas(const stgl::OpenGLTexture<TexFormat, GL_TEXTURE_2D_ARRAY>& tex) {
        terrain_shading_program_.set_wang_tile_atlas(tex);
    }

    void set_camera_frustum(float left, float right, float bottom, float top, float near, float far) {
        sky_program_.set_frustum(left, right, bottom, top, near, far);
    }

    void set_camera_transform(const glm::mat4& camera_transform) {
        sky_program_.set_camera_transform(camera_transform);
    }

    void TerrainShadingPass() {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_.id());

        glDisable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
       // glStencilMask(0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilFunc(GL_EQUAL, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        terrain_shading_program_.Draw<GL_TRIANGLE_STRIP>(indices_, vertices_);
        
        glStencilFunc(GL_EQUAL, 0, 1);
        sky_program_.Draw<GL_TRIANGLE_STRIP>(indices_, vertices_);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void TerrainDebugPass() {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_.id());
        
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
        //glStencilMask(0x00);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilFunc(GL_EQUAL, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        terrain_debug_program_.Draw<GL_TRIANGLE_STRIP>(indices_, vertices_);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void DisplayPass() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_STENCIL_TEST);
        glClear(GL_COLOR_BUFFER_BIT);
        display_program_.Draw<GL_TRIANGLE_STRIP>(indices_, vertices_);
    }

    bool good() {
        return  terrain_shading_program_.good() 
            and terrain_debug_program_.good() 
            and display_program_.good();
    }
};

#endif
