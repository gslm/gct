#ifndef POW_2_HPP
#define POW_2_HPP

#include <ostream>
#include <type_traits>
#include <limits>

namespace gct {
namespace pow2_detail { 

template<unsigned Exp, class T>
using is_representable_pow2 =
        std::integral_constant<bool, 
                              (std::numeric_limits<T>::digits > Exp)>;

}

template<unsigned Exp, class T>
struct pow2 {
    static_assert(std::is_integral<T>::value, "");
    static_assert(pow2_detail::is_representable_pow2<Exp, T>::value, "");
 
    static const T value = static_cast<T>(1) << Exp;
};

namespace Pow2_detail {
    template<class T>
    constexpr T floor(T p) {
        // returns 1 if p == 0
        //if (p == 0) return 0;
       
        T sign = 1; 
        if (std::is_signed<T>::value and p < 0) {
            p = -p;
            sign = -1;
        }

        int c = 0;
        while (p > 0) {
            p >>= 1;
            ++c;
        } 
        --c;

        return (static_cast<T>(1) << c) * sign; 
    } 
}

template<class T>
class Pow2 {
    static_assert(std::is_integral<T>::value, "");

public:
    Pow2() {};

    Pow2(T p) {
        value_ = Pow2_detail::floor(p);
    }

    template<unsigned Exp>
    Pow2(pow2<Exp, T>) {
        value_ = pow2<Exp, T>::value;
    }

    operator T() const { return value_; }

    //T value() const { return value_; }

private:
    T value_;
};

}

#endif
