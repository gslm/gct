#ifndef OPENGL_TERRAIN_SOURCE_HPP
#define OPENGL_TERRAIN_SOURCE_HPP

#include <string>
#include <type_traits>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stgl/OpenGLTexture.hpp>
#include <stgl/OpenGLBuffer.hpp>
#include <stgl/OpenGLProgram.hpp>
#include <RenderOutput.hpp>
#include <GLSLTerrainSource.glsl.hpp>

namespace gct {

namespace OpenGLTerrainSource_detail {

    template<RenderOutput>
    struct OutputIndex;

    template<>
    struct OutputIndex<RenderOutput::terrain_data> {
        static constexpr GLuint value = static_cast<GLuint>
            (GLSLTerrainSource::OutputLocation::terrain_data); 
    };

    template<>
    struct OutputIndex<RenderOutput::texture_coords> {
         static constexpr GLuint value = static_cast<GLuint>
            (GLSLTerrainSource::OutputLocation::texture_coords); 
    };

    template<>
    struct OutputIndex<RenderOutput::texture_coords_dF> {
        static constexpr GLuint value = static_cast<GLuint>
            (GLSLTerrainSource::OutputLocation::texture_coords_dF);
    };

/*
    template<RenderOutput Output>
    struct RenderFlags {
        static constexpr const char* const flag = "";
    };

    template<>
    struct RenderFlags<RenderOutput::texture_coords> {
        static constexpr const char* const flag = 
            "#define TEXTURE_COORDS"; 
    };
 
    template<>
    struct RenderFlags<RenderOutput::texture_coords_dF> {
        static constexpr const char* const flag = 
            "#define TEXTURE_COORDS_DF"; 
    };
   
    template<>
    struct RenderFlags<RenderOutput::terrain_data> {
        static constexpr const char* const flag = 
            "#define TERRAIN_DATA"; 
    };

    template<GLenum ShaderStage>
    struct ShaderSource;

    template<>
    struct ShaderSource<GL_VERTEX_SHADER> {
        static constexpr const char* str = //BEGIN_GLSLSOURCE
            R"GLSLSOURCE(
// Per vertex data.
layout(location = 0) in ivec2 position;

// Per instance uniform data.
layout(location = 0) uniform ivec3 footprint;

// Uniform data among instances.
layout(location = 1) uniform vec4 transition; 
// xy = view_offets; z = alpha_offset; w = one_over_width
layout(location = 2) uniform sampler2DArray cache_data;
layout(location = 3) uniform ivec2 cache_offset;
layout(location = 4) uniform mat4 mvp_matrix;

// Data sent to fragment shader.
#if defined(TERRAIN_DATA)
layout (location = 0) out vec4 frag_terrain_data;
layout (location = 1) out float alpha;
#endif

#if defined(TEXTURE_COORDS)
//layout (location = 2) out float height;
layout (location = 3) out flat ivec2 tile_id;
layout (location = 4) out vec2 tile_offset;
#endif

void main() {

    ivec2 ring_position = position + footprint.xy + 1;
    vec2 f_ring_position = vec2(ring_position);

    ivec2 c = ring_position + ((cache_offset >> footprint.z+1) << 1); 
    vec2 p = vec2(c << footprint.z);//((ring_position + 1) << footprint.z) + ((cache_offset >> (footprint.z+1)) << (footprint.z+1));// + (1 << footprint.z);
    ivec2 d = ring_position & 1;

    ivec3 ts = textureSize(cache_data, 0) - 1;

    vec4 s0 = texelFetch(cache_data, ivec3( c      & ts.xy, footprint.z & ts.z), 0);
    vec4 s1 = texelFetch(cache_data, ivec3((c + d) & ts.xy, footprint.z & ts.z), 0);
    vec4 s2 = texelFetch(cache_data, ivec3((c - d) & ts.xy, footprint.z & ts.z), 0);

    vec4 m = (s1 + s2) * 0.5;
    vec2 a = ((abs(f_ring_position) + transition.xy) - transition.z) * transition.w;
    alpha = clamp(max(a.x, a.y), 0.0, 1.0);
    frag_terrain_data = mix(s0, m, alpha);
    
    tile_id = c - 1;
    tile_offset = f_ring_position;

    gl_Position = mvp_matrix * vec4(p, frag_terrain_data.a, 1).xzyw;

}
)GLSLSOURCE"; //END_GLSLSOURCE
    };

    template<>
    struct ShaderSource<GL_FRAGMENT_SHADER> {

//BEGIN_GLSLSOURCE
        static constexpr const char* str =
R"GLSLSOURCE(

#if defined(TEXTURE_COORDS)
layout (location = 0) uniform ivec3 footprint;
layout (location = 9) uniform ivec2 K;
layout (location = 10) uniform int log2_tile_size;
layout (location = 11) uniform vec2 one_over_atlas_size;
#endif

#if defined(TERRAIN_DATA)
layout (location = 0) in vec4 frag_terrain_data;
layout (location = 1) in float frag_alpha;
#endif

#if defined(TEXTURE_COORDS)
layout (location = 3) in flat ivec2 tile_id;
layout (location = 4) in vec2 tile_offset;
#endif

#if defined(TERRAIN_DATA)
layout (location = 0) out vec4 terrain_data;
#endif

#if defined(TEXTURE_COORDS)
layout (location = 1) out vec3 texture_coords;
#endif

#if defined(TEXTURE_COORDS_DF)
layout (location = 2) out vec4 texture_coords_dF;
#endif


#if defined(TEXTURE_COORDS)

    uint hash(in uint x) {
        x += x << 10;
        x ^= x >> 6;
        x += x << 3;
        x ^= x >> 11;
        x += x << 15;
        return x;
    }

    vec4 tileEdges(in ivec2 t) {
        uvec2 h = uvec2(hash(t.x), hash(t.y << 1));
 
        uint s = hash(h.x + t.y) & (K.y - 1);
        uint n = hash(h.x + (t.y + 1)) & (K.y - 1);
        
        uint w = hash(t.x + h.y) & (K.x - 1);
        uint e = hash((t.x + 1) + h.y) & (K.x - 1);

        return vec4(s,n,w,e);
    } 

    float tileIndex1D(in vec2 e) {
        if (e.x < e.y) return 2.0 * e.x + e.y * e.y; 
        if (e.x > 0.0 && e.x == e.y) return (e.x + 1.0) * (e.y + 1.0) - 2.0;
        if (e.x <= 0.0 && e.x == e.y) return 0.0;
        if (e.y > 0.0) return e.x * e.x + 2.0 * e.y - 1.0;
        return (e.x + 1.0) * (e.y + 1.0) - 1.0;
    }

    float tileIndex1D_nobr(in vec2 e) {
        float c1 = (e.x * e.x + 2.0 * e.y - 1.0) * float(e.x > e.y) * float(e.y > 0.0);
        float c2 = (2.0 * e.x + e.y * e.y) * float(e.y > e.x) * float(e.x >= 0.0);
        float c3 = ((e.x + 1.0) * (e.x + 1.0) - 2.0) * float(e.x == e.y) * float(e.y > 0.0);
        float c4 = ((e.x + 1.0) * (e.x + 1.0) - 1.0) * float(e.x > e.y) * float(e.y == 0.0);

        return c1 + c2 + c3 + c4;
    }
 
    vec2 TileIndex2D(in vec4 tile_edges) {
        return vec2(tileIndex1D_nobr(tile_edges.zw), 
                    tileIndex1D_nobr(tile_edges.xy));
    }

#endif

vec3 pal(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d) {
    return a + b*cos( 6.28318*(c*t+d) );
}

void main() {

#if defined(TERRAIN_DATA) 
    terrain_data = vec4(normalize(frag_terrain_data.xyz), frag_terrain_data.a);
#endif

#if defined(TEXTURE_COORDS) || defined(TEXTUERE_COORDS_DF)
#endif

#if defined(TEXTURE_COORDS) 
    vec2 i = floor(tile_offset);
    vec2 f = fract(tile_offset);

    vec2 l = f * float(1 << footprint.z);
     
    ivec2 k = ivec2(floor(l)) + (tile_id << footprint.z);

    vec2 ti = TileIndex2D(tileEdges(k >> log2_tile_size));
    vec2 tc = ti*float(1 << log2_tile_size) + 
              vec2(k & ((1 << log2_tile_size) - 1)) + 
              fract(l);

    texture_coords = vec3((tc * one_over_atlas_size), 0.0); 
    #endif

#if defined(TEXTURE_COORDS_DF)
    vec2 fake_texture_coords = tile_offset * float(1 << max(0, footprint.z - log2_tile_size));
    texture_coords_dF = vec4(dFdx(texture_coords).xy, dFdy(texture_coords).xy); 
#endif


}
)GLSLSOURCE"; //END_GLSLSOURCE

    };*/
}

template<RenderOutput O>
class OpenGLTerrainSource {
    using Program = stgl::OpenGLProgram<OpenGLTerrainSource>; 
    using S = GLSLTerrainSource;


    template<RenderOutput Out>
    static constexpr GLuint OutputIndex_v = 
        OpenGLTerrainSource_detail::OutputIndex<Out>::value;
/*
    enum class VertexAttribLocation : GLuint {
        position = 0
    };

    enum class TextureUnit : GLuint {
        cache_data = 0,
        Size = 1
    };

    enum class UniformLocation : GLint {
        footprint = 0,
        transition = 1,
        cache_data = 2,
        cache_offset= 3,
        mvp_matrix = 4,

        K = 9,
        log2_tile_size = 10,
        one_over_atlas_size = 11
    };

    enum class OutputLocation : GLuint {
        terrain_data = 0,
        texture_coords = 1,
        texture_coords_dF = 2
    };
*/
    GLuint textures_[static_cast<size_t>(S::TextureUnit::Size)];
    
protected:
    
    OpenGLTerrainSource() {}

public:
    STGL_OPENGL_SHADER(GL_VERTEX_SHADER) {
/*        using namespace OpenGLTerrainSource_detail;        

        std::string source = "#version 450 core\n";
        source += RenderFlags<O & RenderOutput::terrain_data>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords_dF>::flag;
        source += "\n";
        return source += ShaderSource<GL_VERTEX_SHADER>::str;
*/
        return GLSLTerrainSource::vertex_source<O>();
    }

    STGL_OPENGL_SHADER(GL_FRAGMENT_SHADER) {
/*        using namespace OpenGLTerrainSource_detail; 

        std::string source = "#version 450 core\n";
        source += RenderFlags<O & RenderOutput::terrain_data>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords_dF>::flag;
        source += "\n";
        return source += ShaderSource<GL_FRAGMENT_SHADER>::str; 
*/
        return GLSLTerrainSource::fragment_source<O>();
    }

    void set_transition(glm::vec2 view_offset, int n, int w) {
        glm::vec4 transition(view_offset.x, view_offset.y, 
                             float(n/2 - w - 3), 1.0 / float(w)); 
        glProgramUniform4fv(Program::get_id(this),
                            static_cast<GLint>(S::UniformLocation::transition),
                            1, glm::value_ptr(transition));
    }

    void set_cache_offset(const glm::ivec2& cache_offset) {
         glProgramUniform2iv(Program::get_id(this),
                             static_cast<GLint>(S::UniformLocation::cache_offset),
                             1, glm::value_ptr(cache_offset));
    }

    void set_mvp_matrix(const glm::mat4& mvp_matrix) {
        glProgramUniformMatrix4fv(Program::get_id(this), 
                                  static_cast<GLint>(
                                        S::UniformLocation::mvp_matrix),
                                  1, GL_FALSE, glm::value_ptr(mvp_matrix));
    }

    void set_cache_data(const stgl::OpenGLTexture
                            <GL_RGBA32F, GL_TEXTURE_2D_ARRAY>& cache_data) {
        textures_[static_cast<GLuint>(S::TextureUnit::cache_data)] = 
                                                        cache_data.id();

        glProgramUniform1i(Program::get_id(this),
                           static_cast<GLint>(S::UniformLocation::cache_data),
                           static_cast<GLuint>(S::TextureUnit::cache_data));
    }
   
    void set_wang_tiles(const glm::ivec2& log2_K, int log2_tile_size) {
        glm::ivec2 K = glm::ivec2(1) << log2_K;
        glProgramUniform2iv(Program::get_id(this),
                            static_cast<GLint>(S::UniformLocation::K),
                            1, glm::value_ptr(K));
        glProgramUniform1i(Program::get_id(this),
                           static_cast<GLint>(S::UniformLocation::log2_tile_size),
                           log2_tile_size);
        
        glm::vec2 one_over_atlas_size = 
            1.0f / glm::vec2(1 << (2*log2_K+log2_tile_size));

        glProgramUniform2fv(Program::get_id(this),
                            static_cast<GLint>(
                                S::UniformLocation::one_over_atlas_size),
                            1, glm::value_ptr(one_over_atlas_size));
    }

private:
    void set_footprint(const glm::ivec3& footprint) {
        glProgramUniform3iv(Program::get_id(this),
                            static_cast<GLint>(S::UniformLocation::footprint),
                            1, glm::value_ptr(footprint));
    }

public:

    template<RenderOutput Out>
    constexpr static GLuint get_out_index() {
        static_assert(output_count_v<Out> == 1, "");
        static_assert(is_output_subset_v<O, Out>, "");
        return OutputIndex_v<Out>;
    }
/*
    constexpr static GLuint get_out_texture_coords() {
        return static_cast<GLuint>(OutputLocation::texture_coords);
    }

    constexpr static GLuint get_out_texture_coords_dF() {
        return static_cast<GLuint>(OutputLocation::texture_coords_dF);
    }

    constexpr static GLuint get_out_terrain_data() {
        return static_cast<GLuint>(OutputLocation::terrain_data);
    }
*/
    constexpr static GLuint get_dim_terrain_data() {
        return 4;
    }

    constexpr static GLuint get_dim_texture_coords() {
        return 3;
    }

    constexpr static GLuint get_dim_texture_coords_dF() {
        return 4;
    }

    void Enable() { 
        glUseProgram(Program::get_id(this));
 
        glBindTextureUnit(static_cast<GLuint>(S::TextureUnit::cache_data), 
                          textures_[static_cast<GLuint>(S::TextureUnit::cache_data)]);

        glEnableVertexAttribArray(static_cast<GLuint>(S::VertexAttribLocation::position));
    }

    template<GLenum Primitive, class ForwardIter>
    void DrawInstances(ForwardIter instances_begin, ForwardIter instances_end,
              const stgl::OpenGLBufferView<glm::ivec2>& vertices,
              const stgl::OpenGLBufferView<uint16_t>& indices) {
        
        static_assert(std::is_same<glm::ivec3, 
                                   std::decay_t<decltype(
                                            *std::declval<ForwardIter>())>>::value, "");
        static_assert(stgl::is_primitive<Primitive>::value, "");
 
        glVertexAttribIPointer(static_cast<GLuint>(S::VertexAttribLocation::position), 
                               2, GL_INT, vertices.stride(), vertices.offset()); 

        for (auto it = instances_begin; it != instances_end; ++it) {
            set_footprint(*it);
            glDrawElements(Primitive, indices.length(), GL_UNSIGNED_SHORT, 
                           indices.offset());
        }

    }

    void Disable() {
        glDisableVertexAttribArray(static_cast<GLuint>(S::VertexAttribLocation::position));
        glBindTextureUnit(static_cast<GLuint>(S::TextureUnit::cache_data), 0);
        glUseProgram(0);
    }
 
};

}

#endif

