#ifndef OPENGL_FRAMEBUFFER_HH
#define OPENGL_FRAMEBUFFER_HH

#include "OpenGLObject.hpp"
#include "OpenGLTexture.hpp"
#include "OpenGLEnumsTraits.hpp"

#include <array>

namespace stgl {

namespace OpenGLFramebuffer_detail {

template<GLenum... Buffs>
struct is_buffs_valid : std::true_type {
    static_assert(sizeof...(Buffs) == 0, "");
};

template<GLenum Head, GLenum... Tail>
struct is_buffs_valid<Head, Tail...> 
    : std::integral_constant<bool, (Head == GL_NONE or 
                                   is_color_attachment<Head>::value) and
                                   is_buffs_valid<Tail...>::value> {};
};

class OpenGLFramebuffer : public OpenGLObject {

public:
    OpenGLFramebuffer() {
        glCreateFramebuffers(1, &id_);
        glBindFramebuffer(GL_FRAMEBUFFER, id_);
    }

    ~OpenGLFramebuffer() {
        glDeleteFramebuffers(1, &id_);
    }

    template<GLenum AttachementPoint, GLenum TextureFormat, GLenum TextureTarget>
    void AttachTexture(const OpenGLTexture<TextureFormat, TextureTarget>& texture, GLuint level = 0) {
        /*
        static_assert(is_gl_color_attachemnt<AttachementPoint>::value && 
                      is_gl_renderable_texture_format<TextureFormat>::value, "");
        */
        
        glNamedFramebufferTexture(id(), AttachementPoint, texture.id(), level);     
    }

/*
    template<GLenum... Buffs>
    void DrawBuffers() {
        static_assert(OpenGLFramebuffer_detail::is_buffs_valid<Buffs...>::value, "");
        std::array<GLenum, sizeof...(Buffs)> buffs = { Buffs... };
        glNamedFramebufferDrawBuffers(id(), buffs.size(), buffs.data());
    }
*/
    template<size_t N>
    void DrawBuffers(const std::array<GLenum, N>& buffs) { 
        glNamedFramebufferDrawBuffers(id(), buffs.size(), buffs.data());
    }

    template<GLenum Target>
    GLenum CheckStatus() {
        //static_assert(is_render_target<Target>::value, "");
        return glCheckNamedFramebufferStatus(id(), Target);
    }

    template<GLenum Buffer, class T>
    std::enable_if_t<Buffer == GL_COLOR and 
                     std::is_same<T, GLfloat>::value,
    void> Clear(GLint drawbuffer_index, std::array<GLfloat, 4>&& value) { 
        glClearNamedFramebufferfv(id(), Buffer, drawbuffer_index, value.data());
    }

    template<GLenum Buffer, class T>
    std::enable_if_t<Buffer == GL_COLOR and 
                     std::is_same<T, GLint>::value,
    void> Clear(GLint drawbuffer_index, std::array<GLint, 4>&& value) { 
        glClearNamedFramebufferiv(id(), Buffer, drawbuffer_index, value.data());
    }
    template<GLenum Buffer, class T>
    std::enable_if_t<Buffer == GL_COLOR and 
                     std::is_same<T, GLuint>::value,
    void> Clear(GLint drawbuffer_index, std::array<GLuint, 4>&& value) { 
        glClearNamedFramebufferuiv(id(), Buffer, drawbuffer_index, value.data());
    }
    template<GLenum Buffer>
    std::enable_if_t<Buffer == GL_DEPTH,
    void> Clear(GLfloat value) {
        glClearNamedFramebufferfv(id(), Buffer, 0, &value);
    };

    template<GLenum Buffer>
    std::enable_if_t<Buffer == GL_STENCIL,
    void> Clear(GLint value) {
        glClearNamedFramebufferiv(id(), Buffer, 0, &value);
    };

    template<GLenum Buffer>
    std::enable_if_t<Buffer == GL_DEPTH_STENCIL,
    void> Clear(GLfloat depth_value, GLint stencil_value) {
        glClearNamedFramebufferfi(id(), Buffer, 0, depth_value, stencil_value);
    };
};

}

#endif
