#ifndef OPENGL_ENUMS_TRAITS
#define OPENGL_ENUMS_TRAITS

#include <GL/glinclude.hh>

#include <type_traits>
//#include <nonstd_type_traits.hpp>

namespace stgl {

template<GLenum T, GLenum... P0toN >
struct is_one_GLenum_of;

template<GLenum T>
struct is_one_GLenum_of<T> : std::false_type {};

template<GLenum T, GLenum... P1toN>
struct is_one_GLenum_of<T, T, P1toN...> : std::true_type {};

template<GLenum T, GLenum P0, GLenum... P1toN>
struct is_one_GLenum_of<T, P0, P1toN...> : is_one_GLenum_of<T, P1toN...> {};

// Shader assertions
template<GLenum ShaderType>
using is_gl_shader_type = is_one_GLenum_of<ShaderType,
                                 GL_VERTEX_SHADER,
                                 GL_TESS_CONTROL_SHADER,
                                 GL_TESS_EVALUATION_SHADER,
                                 GL_GEOMETRY_SHADER,
                                 GL_FRAGMENT_SHADER>;

// Texture assertions

template<GLenum TextureTarget>
using is_1D_texture_target = is_one_GLenum_of<TextureTarget,
                                              GL_TEXTURE_1D,
                                              GL_TEXTURE_BUFFER>;

template<GLenum TextureTarget>
using is_2D_texture_target = is_one_GLenum_of<TextureTarget,
                                              GL_TEXTURE_2D,
                                              GL_TEXTURE_1D_ARRAY,
                                              GL_TEXTURE_2D_MULTISAMPLE>;

template<GLenum TextureTarget>
using is_3D_texture_target = is_one_GLenum_of<TextureTarget,
                                              GL_TEXTURE_3D,
                                              GL_TEXTURE_2D_ARRAY,
                                              GL_TEXTURE_2D_MULTISAMPLE_ARRAY>;

template<GLenum TextureTarget>
using is_texture_array = is_one_GLenum_of<TextureTarget,
                                          GL_TEXTURE_1D_ARRAY,
                                          GL_TEXTURE_2D_ARRAY>;

template<GLenum TextureTarget>
using is_texture_target = 
        std::integral_constant<bool, 
                               is_1D_texture_target<TextureTarget>::value or
                               is_2D_texture_target<TextureTarget>::value or
                               is_3D_texture_target<TextureTarget>::value>;
                                                    
/*
template<GLenum TextureTarget>
using is_texture_target = is_one_GLenum_of<TextureTarget,
                                              GL_TEXTURE_1D, 
                                              GL_TEXTURE_2D, 
                                              GL_TEXTURE_3D, 
                                              GL_TEXTURE_1D_ARRAY, 
                                              GL_TEXTURE_2D_ARRAY, 
                                              GL_TEXTURE_RECTANGLE, 
                                              GL_TEXTURE_CUBE_MAP, 
                                              GL_TEXTURE_CUBE_MAP_ARRAY, 
                                              GL_TEXTURE_BUFFER, 
                                              GL_TEXTURE_2D_MULTISAMPLE,
                                              GL_TEXTURE_2D_MULTISAMPLE_ARRAY>;
*/

template<GLenum TextureInternalFormat>
using is_texture_sized_internal_format = is_one_GLenum_of<TextureInternalFormat,
                                            GL_R8,
                                            GL_R8_SNORM,
                                            GL_R16,
                                            GL_R16_SNORM,
                                            GL_RG8,
                                            GL_RG8_SNORM,        
                                            GL_RG16,
                                            GL_RG16_SNORM, 
                                            GL_R3_G3_B2,
                                            GL_RGB4,
                                            GL_RGB5,
                                            GL_RGB8,
                                            GL_RGB8_SNORM,
                                            GL_RGB10,
                                            GL_RGB12,
                                            GL_RGB16_SNORM,
                                            GL_RGBA2,
                                            GL_RGBA4,
                                            GL_RGB5_A1,
                                            GL_RGBA8,
                                            GL_RGBA8_SNORM,
                                            GL_RGB10_A2,
                                            GL_RGB10_A2UI,
                                            GL_RGBA12,
                                            GL_RGBA16,
                                            GL_SRGB8,
                                            GL_SRGB8_ALPHA8,
                                            GL_R16F,
                                            GL_RG16F,
                                            GL_RGB16F,
                                            GL_RGBA16F,
                                            GL_R32F,
                                            GL_RG32F,
                                            GL_RGB32F,
                                            GL_RGBA32F,
                                            GL_R11F_G11F_B10F, 
                                            GL_RGB9_E5,
                                            GL_R8I,
                                            GL_R8UI,
                                            GL_R16I,
                                            GL_R16UI,
                                            GL_R32I,
                                            GL_R32UI, 
                                            GL_RG8I,
                                            GL_RG8UI,          
                                            GL_RG16I,
                                            GL_RG16UI,
                                            GL_RG32I,
                                            GL_RG32UI,
                                            GL_RGB8I,
                                            GL_RGB8UI,
                                            GL_RGB16I,
                                            GL_RGB16UI,
                                            GL_RGB32I,
                                            GL_RGB32UI,
                                            GL_RGBA8I,
                                            GL_RGBA8UI,
                                            GL_RGBA16I,
                                            GL_RGBA16UI,
                                            GL_RGBA32I,
                                            GL_RGBA32UI,
                                            GL_DEPTH_COMPONENT16, 
                                            GL_DEPTH_COMPONENT24, 
                                            GL_DEPTH_COMPONENT32,
                                            GL_DEPTH_COMPONENT32F>;
template<GLenum TextureInternalFormat>
using is_sampler_assignable = is_one_GLenum_of<TextureInternalFormat,
                                            GL_R8,
                                            GL_R8_SNORM,
                                            GL_R16,
                                            GL_R16_SNORM,
                                            GL_RG8,
                                            GL_RG8_SNORM,        
                                            GL_RG16,
                                            GL_RG16_SNORM, 
                                            GL_R3_G3_B2,
                                            GL_RGB4,
                                            GL_RGB5,
                                            GL_RGB8,
                                            GL_RGB8_SNORM,
                                            GL_RGB10,
                                            GL_RGB12,
                                            GL_RGB16_SNORM,
                                            GL_RGBA2,
                                            GL_RGBA4,
                                            GL_RGB5_A1,
                                            GL_RGBA8,
                                            GL_RGBA8_SNORM,
                                            GL_RGB10_A2,
                                            GL_RGBA12,
                                            GL_RGBA16,
                                            GL_SRGB8,
                                            GL_SRGB8_ALPHA8,
                                            GL_R16F,
                                            GL_RG16F,
                                            GL_RGB16F,
                                            GL_RGBA16F,
                                            GL_R32F,
                                            GL_RG32F,
                                            GL_RGB32F,
                                            GL_RGBA32F,
                                            GL_R11F_G11F_B10F, 
                                            GL_RGB9_E5>;


template<GLenum TextureInternalFormat>
using is_isampler_assignable = is_one_GLenum_of<TextureInternalFormat,
                                            GL_R8I,
                                            GL_R16I,
                                            GL_R32I,
                                            GL_RG8I,
                                            GL_RG16I,
                                            GL_RG32I,
                                            GL_RGB8I,
                                            GL_RGB16I,
                                            GL_RGB32I,
                                            GL_RGBA8I,
                                            GL_RGBA16I,
                                            GL_RGBA32I>;

template<GLenum TextureInternalFormat>
using is_usampler_assignable = is_one_GLenum_of<TextureInternalFormat,
                                            GL_RGB10_A2UI,
                                            GL_R8UI,
                                            GL_R16UI,
                                            GL_R32UI, 
                                            GL_RG8UI,          
                                            GL_RG16UI,
                                            GL_RG32UI,
                                            GL_RGB8UI,
                                            GL_RGB16UI,
                                            GL_RGB32UI,
                                            GL_RGBA8UI,
                                            GL_RGBA16UI,
                                            GL_RGBA32UI>;

template<GLenum TextureInternalFormat>
using is_texture_internal_format = std::integral_constant<bool,
        is_sampler_assignable<TextureInternalFormat>::value or
        is_isampler_assignable<TextureInternalFormat>::value or
        is_usampler_assignable<TextureInternalFormat>::value>;
                                     
/*
template<GLenum TextureInternalFormat, class = void>
struct texture_format_component_count {
   st  
};

template<GLenum TextureInternalFormat>
struct texture_format_component_count<TextureInternalFormat, 
std::enable_if_t<has_1_component<TextureInternalFormat>::value>>
    : std::integral_constant<int, 1> {};

template<GLenum TextureInternalFormat>
struct texture_format_component_count<TextureInternalFormat, 
std::enable_if_t<has_2_component<TextureInternalFormat>::value>>
    : std::integral_constant<int, 2> {};

template<GLenum TextureInternalFormat>
struct texture_format_component_count<TextureInternalFormat, 
std::enable_if_t<has_2_component<TextureInternalFormat>::value>>
    : std::integral_constant<int, 3> {};

template<GLenum TextureInternalFormat>
struct texture_format_component_count<TextureInternalFormat, 
std::enable_if_t<has_4_component<TextureInternalFormat>::value>>
    : std::integral_constant<int, 4> {};

*/

template<GLenum TextureDataType>
using is_texture_data_type = is_one_GLenum_of<TextureDataType,
                                          GL_UNSIGNED_BYTE, 
                                          GL_BYTE, 
                                          GL_UNSIGNED_SHORT,
                                          GL_SHORT,
                                          GL_UNSIGNED_INT,
                                          GL_INT, 
                                          GL_FLOAT,
                                          GL_UNSIGNED_BYTE_3_3_2,
                                          GL_UNSIGNED_BYTE_2_3_3_REV,
                                          GL_UNSIGNED_SHORT_5_6_5,
                                          GL_UNSIGNED_SHORT_5_6_5_REV,
                                          GL_UNSIGNED_SHORT_4_4_4_4,
                                          GL_UNSIGNED_SHORT_4_4_4_4_REV,
                                          GL_UNSIGNED_SHORT_5_5_5_1,
                                          GL_UNSIGNED_SHORT_1_5_5_5_REV,
                                          GL_UNSIGNED_INT_8_8_8_8,
                                          GL_UNSIGNED_INT_8_8_8_8_REV,
                                          GL_UNSIGNED_INT_10_10_10_2,
                                          GL_UNSIGNED_INT_2_10_10_10_REV>;

template<GLenum TextureDataFormat>
using is_texture_data_format = is_one_GLenum_of<TextureDataFormat,
                                            GL_RED, 
                                            GL_RG,
                                            GL_RGB,
                                            GL_BGR,
                                            GL_RGBA,
                                            GL_DEPTH_COMPONENT,
                                            GL_STENCIL_INDEX>;

template<GLenum TextureDataType, GLenum TextureDataFormat>
struct is_texture_data_format_compatible_with_type : std::true_type {
    static_assert(is_texture_data_type<TextureDataType>::value, "");
    static_assert(is_texture_data_format<TextureDataFormat>::value, "");    
};

template<GLenum TextureDataFormat>
struct is_texture_data_format_compatible_with_type<GL_RGB, TextureDataFormat>
         : public std::integral_constant<bool, not is_one_GLenum_of<TextureDataFormat,
                                          GL_UNSIGNED_SHORT_4_4_4_4,
                                          GL_UNSIGNED_SHORT_4_4_4_4_REV,
                                          GL_UNSIGNED_SHORT_5_5_5_1,
                                          GL_UNSIGNED_SHORT_1_5_5_5_REV,
                                          GL_UNSIGNED_INT_8_8_8_8,
                                          GL_UNSIGNED_INT_8_8_8_8_REV,
                                          GL_UNSIGNED_INT_10_10_10_2,
                                          GL_UNSIGNED_INT_2_10_10_10_REV>::value> {};
/*
Documentation does not mention GL_BGR 
template<GLenum TextureDataFormat>
struct is_texture_data_format_compatible_with_type<GL_BGR, TextureDataFormat> 
        : std::integral_constant<bool, not is_one_GLenum_of<TextureDataFormat,
                                          GL_UNSIGNED_SHORT_4_4_4_4,
                                          GL_UNSIGNED_SHORT_4_4_4_4_REV,
                                          GL_UNSIGNED_SHORT_5_5_5_1,
                                          GL_UNSIGNED_SHORT_1_5_5_5_REV,
                                          GL_UNSIGNED_INT_8_8_8_8,
                                          GL_UNSIGNED_INT_8_8_8_8_REV,
                                          GL_UNSIGNED_INT_10_10_10_2,
                                          GL_UNSIGNED_INT_2_10_10_10_REV>::value> {};
*/ 

template<GLenum TextureDataFormat>
struct is_texture_data_format_compatible_with_type<GL_RGBA, TextureDataFormat> 
        : public std::integral_constant<bool, not is_one_GLenum_of<TextureDataFormat,
                                                GL_UNSIGNED_BYTE_3_3_2,
                                                GL_UNSIGNED_BYTE_2_3_3_REV,
                                                GL_UNSIGNED_SHORT_5_6_5,
                                                GL_UNSIGNED_SHORT_5_6_5_REV>::value> {};
template<GLenum TextureDataFormat>
struct is_texture_data_format_compatible_with_type<GL_BGRA, TextureDataFormat> 
        : public std::integral_constant<bool, not is_one_GLenum_of<TextureDataFormat,
                                                GL_UNSIGNED_BYTE_3_3_2,
                                                GL_UNSIGNED_BYTE_2_3_3_REV,
                                                GL_UNSIGNED_SHORT_5_6_5,
                                                GL_UNSIGNED_SHORT_5_6_5_REV>::value> {};
template<GLenum TextureParam>
using is_depth_stencil_texture_mode_param = is_one_GLenum_of<TextureParam, 
                                                                GL_DEPTH_COMPONENT, 
                                                                GL_STENCIL_INDEX>;
                                            
template<GLenum TextureParam>
using is_compare_func_param = is_one_GLenum_of<TextureParam, 
                                                  GL_LEQUAL,
                                                  GL_GEQUAL,
                                                  GL_LESS,
                                                  GL_GREATER,
                                                  GL_EQUAL,
                                                  GL_NOTEQUAL,
                                                  GL_ALWAYS,
                                                  GL_NEVER>;

template<GLenum TextureParam>
using is_compare_mode_param = is_one_GLenum_of<TextureParam,
                                                  GL_COMPARE_REF_TO_TEXTURE,
                                                  GL_NONE>;

template<GLenum TextureParam>
using is_min_filter_param = is_one_GLenum_of<TextureParam, 
                                                GL_NEAREST,
                                                GL_LINEAR,
                                                GL_NEAREST_MIPMAP_NEAREST,
                                                GL_LINEAR_MIPMAP_NEAREST,
                                                GL_NEAREST_MIPMAP_LINEAR,
                                                GL_LINEAR_MIPMAP_LINEAR>;

template<GLenum TextureParam>
using is_mag_filter_param = is_one_GLenum_of<TextureParam, 
                                                GL_NEAREST,
                                                GL_LINEAR>;

template<GLenum TextureParam>
using is_swizzle_param = is_one_GLenum_of<TextureParam,
                                             GL_RED,
                                             GL_GREEN,
                                             GL_BLUE,
                                             GL_ALPHA,
                                             GL_ZERO,
                                             GL_ONE>;

template<GLenum TextureParam>
using is_wrap_param = is_one_GLenum_of<TextureParam, 
                                          GL_CLAMP_TO_EDGE,
                                          GL_CLAMP_TO_BORDER,
                                          GL_MIRRORED_REPEAT,
                                          GL_REPEAT,
                                          GL_MIRROR_CLAMP_TO_EDGE>;



template<GLenum Primitive>
using is_primitive = is_one_GLenum_of<Primitive,
                                           GL_POINTS, 
                                           GL_LINE_STRIP, 
                                           GL_LINE_LOOP, 
                                           GL_LINES, 
                                           GL_TRIANGLE_STRIP, 
                                           GL_TRIANGLE_FAN,
                                           GL_TRIANGLES>;

template<GLenum ColorAttachment>
using is_color_attachment = is_one_GLenum_of<ColorAttachment,
                                       GL_COLOR_ATTACHMENT0,
                                       GL_COLOR_ATTACHMENT1,
                                       GL_COLOR_ATTACHMENT2,
                                       GL_COLOR_ATTACHMENT3,
                                       GL_COLOR_ATTACHMENT4,
                                       GL_COLOR_ATTACHMENT5,
                                       GL_COLOR_ATTACHMENT6,
                                       GL_COLOR_ATTACHMENT7,
                                       GL_COLOR_ATTACHMENT8>;


}
#endif
