#ifndef STGL_OPENGL_DEBUG_HPP
#define STGL_OPENGL_DEBUG_HPP

#include <GL/glinclude.hh>
#include <iostream>
#include <string>

#define GL_ERROR_DEBUG 1

#if GL_ERROR_DEBUG
/*
#define STGL_OPENGL_ERROR_DEBUG std::cout << "[DEBUG] :: " \
                                << __FILE__ << ":" << __LINE__ << " :: "\
                                << "GL_ERROR " << glGetError() << std::endl;
*/
#define STGL_OPENGL_ERROR_DEBUG stgl::glDebugOutput(__FILE__, __LINE__);
#define STGL_OPENGL_ASSERT(X) X; stgl::glDebugOutput(__FILE__, __LINE__);


#else

#define STGL_OPENGL_ERROR_DEBUG
#define STGL_GL_ASSERT(X) X;

#endif

namespace stgl {

    void glDebugOutput(const char* file, int line) {
        GLenum e = glGetError();        

        //if (e == GL_NO_ERROR) return;

        const char* gl_error_string = [e]() {
            if (e == GL_INVALID_OPERATION) return "GL_INVALID_OPERATION";
            if (e == GL_INVALID_ENUM)      return "GL_INVALID_ENUM";
            if (e == GL_INVALID_VALUE)     return "GL_INVALID_VALUE";
            if (e == GL_OUT_OF_MEMORY)     return "GL_OUT_OF_MEMORY";
            if (e == GL_INVALID_FRAMEBUFFER_OPERATION) 
                return "GL_INVALID_FRAMEBUFFER_OPERATION";
            return "GL_NO_ERROR";
        }();

        std::cerr << "[DEBUG] :: " << file << ":" << line;
        std::cerr << " :: GL_ERROR " << gl_error_string << std::endl; 
    }

}


#endif
