#ifndef OPENGL_TEXTURE_HPP
#define OPENGL_TEXTURE_HPP

#include <GL/glinclude.hh>

#include "OpenGLEnumsTraits.hpp"
#include "OpenGLObject.hpp"

#include <array>

namespace stgl {

namespace OpenGLTexture_detail {
/*
    template<GLenum T>
    using is_1D_texture_target = stgl::is_one_GLenum_of<T,
                                           GL_TEXTURE_1D>;

    template<GLenum T>
    using is_2D_texture_target = stgl::is_one_GLenum_of<T,
                                           GL_TEXTURE_2D, 
                                           GL_TEXTURE_1D_ARRAY, 
                                           GL_TEXTURE_RECTANGLE>;

    template<GLenum T>
    using is_3D_texture_target = stgl::is_one_GLenum_of<T,
                                           GL_TEXTURE_3D, 
                                           GL_TEXTURE_2D_ARRAY, 
                                           GL_TEXTURE_CUBE_MAP_ARRAY>;
*/ 
    template<GLenum Target, class = void>
    struct TextureDim {
        static_assert(stgl::is_texture_target<Target>::value, "");
    };

    template<GLenum Target>
    struct TextureDim<Target, 
    std::enable_if_t<stgl::is_1D_texture_target<Target>::value>> {
        GLuint width;
        
        TextureDim() : width(0) {}
    };

    template<GLenum Target>
    struct TextureDim<Target, 
    std::enable_if_t<stgl::is_2D_texture_target<Target>::value>> {
        GLuint width;
        GLuint height;

        TextureDim() : width(0), height(0) {}
    };

    template<GLenum Target>
    struct TextureDim<Target, 
    std::enable_if_t<stgl::is_3D_texture_target<Target>::value>> {
        GLuint width;
        GLuint height;
        GLuint depth;

        TextureDim() : width(0), height(0), depth(0) {}
    };
}

template<GLenum InternalFormat, GLenum Target>
class OpenGLTexture : public OpenGLObject {   
    static_assert(stgl::is_texture_target<Target>::value, "");

    template<GLenum T>
    using TextureDim = OpenGLTexture_detail::TextureDim<T>;

protected:
/*
    template<GLenum T>
    using is_1D_texture_target = 
        OpenGLTexture_detail::is_1D_texture_target<T>; 

    template<GLenum T>
    using is_2D_texture_target = 
        OpenGLTexture_detail::is_2D_texture_target<T>; 

    template<GLenum T>
    using is_3D_texture_target = 
        OpenGLTexture_detail::is_2D_texture_target<T>; 
*/
    GLuint levels_;
    TextureDim<Target> dim_;

public:
 
    OpenGLTexture() {}

    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_1D_texture_target<T>::value>>
    OpenGLTexture(GLuint width, GLuint levels = 1) {
        static_assert(is_texture_sized_internal_format<InternalFormat>::value, "");
        
        glCreateTextures(Target, 1, &id_);
        glTextureStorage1D(id_, levels, InternalFormat, width);

        dim_.width = width;
    
        levels_ = levels;
    }
    
    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_2D_texture_target<T>::value>>
    OpenGLTexture(GLuint width, GLuint height, GLuint levels = 1) {
        //static_assert(is_texture_sized_internal_format<InternalFormat>::value, "");
        
        glCreateTextures(Target, 1, &id_);
        glTextureStorage2D(id_, levels, InternalFormat, width, height);
     
        dim_.width = width;   
        dim_.height = height;
        
        levels_= levels;
    }

    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_3D_texture_target<T>::value>>
    OpenGLTexture(GLuint width, GLuint height, GLuint depth, GLuint levels = 1) {
        static_assert(is_texture_sized_internal_format<InternalFormat>::value, "");
        
        glCreateTextures(Target, 1, &id_);
        glTextureStorage3D(id_, levels, InternalFormat, width, height, depth);
    
        dim_.width = width;
        dim_.height = height;
        dim_.depth = depth;

        levels_ = levels;
    }  

    OpenGLTexture(OpenGLTexture&&) = default; 
    OpenGLTexture& operator=(OpenGLTexture&&) = default; 
    
    ~OpenGLTexture() {
        glDeleteTextures(1, &id_);
    }

    template<GLenum Format, GLenum Type, GLenum T = Target>
    std::enable_if_t<stgl::is_1D_texture_target<T>::value,
    void> SubImage(GLuint level, 
                   GLint xoffset, GLint yoffset, 
                   GLuint width, GLuint height, 
                   void* data) {
        
        static_assert(stgl::is_texture_data_format<Format>::value, "");
        static_assert(stgl::is_texture_data_type<Type>::value, "");
        static_assert(stgl::is_texture_data_format_compatible_with_type
                                                    <Format, Type>::value, "");

        glTextureSubImage1D(id_, level, xoffset, width, Format, Type, data);
    }

    template<GLenum Format, GLenum Type, GLenum T = Target>
    std::enable_if_t<stgl::is_2D_texture_target<T>::value,
    void> SubImage(GLuint level, 
                   GLint xoffset, GLint yoffset, 
                   GLuint width, GLuint height,
                   void* data) {
        
        static_assert(stgl::is_texture_data_format<Format>::value, "");
        static_assert(stgl::is_texture_data_type<Type>::value, "");
        static_assert(stgl::is_texture_data_format_compatible_with_type
                                                    <Format, Type>::value, "");

        glTextureSubImage2D(id_, 
                            level, 
                            xoffset, yoffset, 
                            width, height, 
                            Format, Type, data);
    }

    template<GLenum Format, GLenum Type, GLenum T = Target>
    std::enable_if_t<stgl::is_3D_texture_target<T>::value,
    void> SubImage(GLuint level,
                   GLint xoffset, GLint yoffset, GLint zoffset, 
                   GLuint width, GLuint height, GLuint depth, 
                   void* data) {
        
        static_assert(stgl::is_texture_data_format<Format>::value, "");
        static_assert(stgl::is_texture_data_type<Type>::value, "");
        static_assert(stgl::is_texture_data_format_compatible_with_type
                                                    <Format, Type>::value, "");

        glTextureSubImage3D(id_, 
                            level, 
                            xoffset, yoffset, zoffset, 
                            width, height, depth, 
                            Format, Type, data);
    }
    
    GLuint width() const { 
        return dim_.width; 
    }
   
    template<GLenum T = Target> 
    std::enable_if_t<not stgl::is_1D_texture_target<T>::value,
    GLuint> height() const { 
        return dim_.height; 
    }

    template<GLenum T = Target>
    std::enable_if_t<stgl::is_3D_texture_target<T>::value,
    GLuint> depth() const { 
        return dim_.depth; 
    }

    GLuint levels() const { 
        return levels_;
    }
    
    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_DEPTH_STENCIL_TEXTURE_MODE, 
    void> SetParameter() {
        static_assert(stgl::is_depth_stencil_texture_mode_param
                                                   <Param>::value, "");

        glTextureParameteri(id_, Pname, Param); 
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_BASE_LEVEL,
    void> SetParameter(unsigned int base_level) { 
        glTextureParameteri(id_, Pname, base_level);
    }

    template<GLenum Pname, bool I = false>
    std::enable_if_t<Pname == GL_TEXTURE_BORDER_COLOR,
    void> SetParameter(const std::array<int, 4>& border_color) {
        if (I) glTextureParameterIiv(id_, Pname, border_color.data());  
        else   glTextureParameteriv(id_, Pname, border_color.data());  
    }
 
    template<GLenum Pname, bool I = false>
    std::enable_if_t<Pname == GL_TEXTURE_BORDER_COLOR,
    void> SetParameter(const std::array<unsigned int, 4>& border_color) {
        if (I) glTextureParameterIuiv(id_, Pname, border_color.data());
        else   glTextureParameteriv(id_, Pname, border_color.data());
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_BORDER_COLOR,
    void> SetParameter(const std::array<float, 4>& border_color) {
        glTextureParameterfv(id_, Pname, border_color.data());  
    }

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_COMPARE_FUNC,
    void> SetParameter() {
        static_assert(stgl::is_compare_func_param<Param>::value, "");
 
        glTextureParameteri(id_, GL_TEXTURE_COMPARE_FUNC, Param);
    }

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_COMPARE_MODE,
    void> SetParameter() {
        static_assert(stgl::is_compare_mode_param<Param>::value, "");
        
        glTextureParameteri(id_, Pname, Param);
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_LOD_BIAS,
    void> SetParameter(float lod_bias) { 
        glTextureParameterf(id_, Pname, lod_bias);
    }

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_MIN_FILTER,
    void> SetParameter() {
        static_assert(stgl::is_min_filter_param<Param>::value, "");

        glTextureParameteri(id_, Pname, Param);
    }

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_MAG_FILTER,
    void> SetParameter() {
        static_assert(stgl::is_mag_filter_param<Param>::value, "");

        glTextureParameteri(id_, Pname, Param);
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_MIN_LOD,
    void> SetParameter(float min_lod) {
        glTextureParameteri(id_, Pname, min_lod);
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_MAX_LOD,
    void> SetParameter(float max_lod) {
        glTextureParameteri(id_, Pname, max_lod);
    }

    template<GLenum Pname>
    std::enable_if_t<Pname == GL_TEXTURE_MAX_LEVEL,
    void> SetParameter(int max_level) {
        glTextureParameteri(id_, Pname, max_level);
    }

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_SWIZZLE_R or
                     Pname == GL_TEXTURE_SWIZZLE_G or
                     Pname == GL_TEXTURE_SWIZZLE_B or
                     Pname == GL_TEXTURE_SWIZZLE_A,
    void> SetParameter() {
        static_assert(stgl::is_swizzle_param<Param>::value, "");    
    
        glTextureParameteri(id_, Pname, Param);
    }

    template<GLenum Pname, 
             GLenum ParamR, GLenum ParamG, GLenum ParamB, GLenum ParamA>
    std::enable_if_t<Pname == GL_TEXTURE_SWIZZLE_RGBA,
    void> SetParameter() {
        static_assert(stgl::is_swizzle_param<ParamR>::value, "");    
        static_assert(stgl::is_swizzle_param<ParamG>::value, "");    
        static_assert(stgl::is_swizzle_param<ParamB>::value, "");    
        static_assert(stgl::is_swizzle_param<ParamA>::value, "");

        std::array<GLenum, 4> swizzle = {{ ParamR, ParamG, ParamB, ParamA }};
        glTextureParameteriv(id_, Pname, swizzle.data());
    } 

    template<GLenum Pname, GLenum Param>
    std::enable_if_t<Pname == GL_TEXTURE_WRAP_T or 
                     Pname == GL_TEXTURE_WRAP_S or
                     Pname == GL_TEXTURE_WRAP_R,
    void> SetParameter() {
        static_assert(stgl::is_wrap_param<Param>::value, "");

        glTextureParameteri(id_, Pname, Param);
    }

    void GenerateMipmap() {
        glGenerateTextureMipmap(id()); 
    }

/*
    template<class = std::enable_if<is_1D_texture_target<Target>::value>>
    OpenGLTexture(GLuint id, GLuint levels, GLuint width) {
        id_ = id;

        levels_ = levels;

        dim_.width = width;
    }

    template<class = std::enable_if<is_2D_texture_target<Target>::value>>
    OpenGLTexture(GLuint id, GLuint levels, GLuint width, GLuint height) {
        id_ = id;

        levels_ = levels;
     
        dim_.width = width;
        dim_.height = height;
    }

    template<class = std::enable_if<is_3D_texture_target<Target>::value>>
    OpenGLTexture(GLuint id, GLuint levels, GLuint width, GLuint depth) {
        id_ = id_;
            
        levels_ = levels;

        dim_.width = width;
        dim_.height = height;
        dim_.depth = depth;
    }
*/
};

}
#endif
