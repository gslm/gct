#ifndef OPENGL_BUFFER_HPP
#define OPENGL_BUFFER_HPP

#include <type_traits>
#include "OpenGLObject.hpp"

#include <initializer_list>

namespace stgl {

template<class T>
class OpenGLBuffer : public OpenGLObject {

    std::size_t size_; 
    GLbitfield flags_;
    
public:

    OpenGLBuffer(GLbitfield flags, std::size_t size, const T* data = nullptr) {
        glCreateBuffers(1, &id_); 
        glNamedBufferStorage(id(), sizeof(T)*size, data, flags);

        size_ = size;
        flags_ = flags;
    }

    OpenGLBuffer(GLbitfield flags, std::initializer_list<T> data) {
        glCreateBuffers(1, &id_); 
        glNamedBufferStorage(id(), sizeof(T)*data.size(), data.begin(), flags);

        size_ = data.size();
        flags_ = flags;
    }

    OpenGLBuffer(OpenGLBuffer&&) = default;
    OpenGLBuffer& operator=(OpenGLBuffer&&) = default; 
    
    ~OpenGLBuffer() {
        glDeleteBuffers(1, &id_);
    }

    void SubData(std::size_t size, const T* data, std::size_t offset = 0) {
        glNamedBufferSubData(id(), sizeof(T)*offset, sizeof(T)*size, data);
    }

    void SubData(std::initializer_list<T> data, std::size_t offset = 0) {
        glNamedBufferSubData(id(), sizeof(T)*offset, sizeof(T)*data.size(), data.begin());
    }

    size_t size() const { return size_; }
    GLbitfield flags() const { return flags_; }
};

template<class T>
class OpenGLBufferView {
    GLuint id_ = 0;
    size_t length_ = 0;
    GLvoid* offset_ = 0;
    GLsizei stride_ = 0;

public:

    OpenGLBufferView() {}
    template<class U>
    OpenGLBufferView(const OpenGLBuffer<U>& buffer, size_t length, size_t stride, size_t offset) {
        id_ = buffer.id();
        length_ = length;
        offset_ = reinterpret_cast<GLvoid*>(offset*sizeof(T));
        stride_ = stride;
    }

    GLuint id() const { return id_; }
    size_t length() const { return length_; }
    GLvoid* offset() const { return offset_; }
    GLsizei stride() const { return stride_; } 

};

}
#endif
