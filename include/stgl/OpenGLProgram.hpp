#ifndef GLSL_PROGRAM_HPP
#define GLSL_PROGRAM_HPP

#include "OpenGLObject.hpp"
//#include <OpenGLEnumTraits.hpp>

#include <type_traits>
#include <string>
#include <array>

#ifdef GLSL_DEBUG
#include <iostream>
#endif

namespace stgl {

namespace OpenGLProgram_detail {

template<class... T>
using void_t = void;

template<GLenum E>
struct GLenumType {
    enum : GLenum { value = E };
};

static const size_t StagesCount = 5;

template<GLenum>
struct ShaderStage;

template<>
struct ShaderStage<GL_VERTEX_SHADER> {
    static const int index = 0;
    static constexpr const char* str = "VERTEX SHADER";
};

template<>
struct ShaderStage<GL_TESS_CONTROL_SHADER> {
    static const int index = 1;
    static constexpr const char* str = "TESS CONTROL SHADER";
};

template<>
struct ShaderStage<GL_TESS_EVALUATION_SHADER> {
    static const int index = 2;
    static constexpr const char* str = "TESS EVALUATION SHADER";
};

template<>
struct ShaderStage<GL_GEOMETRY_SHADER> {
    static const int index = 3;
    static constexpr const char* str = "GEOMETRY SHADER";
};

template<>
struct ShaderStage<GL_FRAGMENT_SHADER> {
    static const int index = 4;
    static constexpr const char* str = "FRAGMENT SHADER";
};


template<class ShaderSource, GLenum ShaderStage, class = void_t<> >
struct has_shader_stage : std::false_type {};

template<class ShaderSource, GLenum ShaderStage>
struct has_shader_stage<ShaderSource, ShaderStage, 
                        void_t<decltype(ShaderSource::STGL_OPENGL_SHADER_MACRO(
                                    GLenumType<ShaderStage>()))> > 
                       : std::true_type {};

template<class ShaderSource, GLenum Stage>
struct ShaderCompiler {
    
    template<class Source = ShaderSource, GLenum S = Stage>
    static inline 
    std::enable_if_t<has_shader_stage<Source, Stage>::value,
    GLuint> compile(GLuint id) { 
        GLuint shader_id = glCreateShader(S);

        std::string s = ShaderSource::STGL_OPENGL_SHADER_MACRO(GLenumType<S>());
        const GLchar* source_ptr = s.c_str();

        glShaderSource(shader_id, 1, (const GLchar**)&source_ptr, nullptr);
        glCompileShader(shader_id);
      
        GLint success = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);

#ifdef GLSL_DEBUG
        PrintShaderLog(shader_id, success);
#endif 
        if (not success) {
            glDeleteShader(shader_id);
            return 0;
        }
 
        glAttachShader(id, shader_id);
        
        return shader_id;
    }

    template<class Source = ShaderSource, GLenum S = Stage>
    static inline
    std::enable_if_t<not has_shader_stage<Source, S>::value,
    GLuint> compile(GLuint) { return 0; }

private:
    static inline void PrintShaderLog(GLuint shader_id, bool success) {
        std::cout << typeid(ShaderSource).name() << "\n";
        std::cout << ShaderStage<Stage>::str << " COMPILATION LOG:\n"; 
        if (success) {
            std::cout << "SUCCESS." << std::endl;
            return;
        }

        GLint max_length = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &max_length);
        std::vector<GLchar> info_log(max_length);
        glGetShaderInfoLog(shader_id, max_length, &max_length, info_log.data());
        std::cout << info_log.data() << std::endl; 
    }
};


template<class ShaderSource, GLenum...>
struct ProgramBuilder_impl {
    static inline bool build(GLuint, 
                             std::array<GLuint, StagesCount>&) { return true; }
};

template<class ShaderSource, GLenum S, GLenum... XS>
struct ProgramBuilder_impl<ShaderSource, S, XS...> {
    static inline bool build(GLuint id, 
                             std::array<GLuint, StagesCount>& shader_ids) {

        shader_ids[ShaderStage<S>::index] = 
                ShaderCompiler<ShaderSource, S>::compile(id);

        bool success = 
                ProgramBuilder_impl<ShaderSource, XS...>::build(id, shader_ids);
        
        if (not has_shader_stage<ShaderSource, S>::value) return true;
        return success and (shader_ids[ShaderStage<S>::index] > 0);
    }
};

template<class ShaderSource>
struct ProgramBuilder {
    static inline bool build(GLuint id) {
        std::array<GLuint, StagesCount> shader_ids;
        for (auto&& s : shader_ids) s = 0;

        bool compile_success = ProgramBuilder_impl<ShaderSource,
                            GL_VERTEX_SHADER,
                            GL_TESS_CONTROL_SHADER,
                            GL_TESS_EVALUATION_SHADER,
                            GL_GEOMETRY_SHADER,
                            GL_FRAGMENT_SHADER>::build(id, shader_ids);

        glLinkProgram(id);

        for (auto&& s : shader_ids) {
            if (s > 0) {
                glDetachShader(id, s);
                glDeleteShader(s);
            }
        }

        GLint success = 0;
        glGetProgramiv(id, GL_LINK_STATUS, &success);

#if GLSL_DEBUG
        PrintLinkLog(id, success);
#endif

        return success and compile_success;
    }

private:
    static inline void PrintLinkLog(GLuint program_id, bool success) { 
        std::cout << typeid(ShaderSource).name() << "\n";
        std::cout << "PROGRAM LINK LOG:\n";
        if (success) {
            std::cout << "SUCCESS." << std::endl;
            return;
        }

        GLint max_length = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &max_length);
        std::vector<GLchar> info_log(max_length);
        glGetProgramInfoLog(program_id, max_length, &max_length, info_log.data());

        std::cout << info_log.data() << std::endl;
    }
};

}

#define STGL_OPENGL_SHADER(X) static inline std::string \
                             STGL_OPENGL_SHADER_MACRO\
                             (stgl::GLenumType<X>)

template<GLenum E>
using GLenumType = typename OpenGLProgram_detail::GLenumType<E>;

template<class ShaderSource>
class OpenGLProgram : public OpenGLObject, 
                      public ShaderSource {
    using Builder = 
        OpenGLProgram_detail::ProgramBuilder<ShaderSource>;

    bool good_;

public:
    OpenGLProgram() { 
        id_ = glCreateProgram();
        bool success = Builder::build(id());
        if (not success) {
            glDeleteProgram(id_);
            id_ = 0;
        }
    }

    ~OpenGLProgram() {
        glDeleteProgram(id());
    }

    static GLuint get_id(ShaderSource* s) {
        return static_cast<OpenGLProgram<ShaderSource>*>(s)->id();
    }

    bool good() { return id_ > 0; }
};

}
#endif
