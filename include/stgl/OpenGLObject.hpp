#ifndef OPENGL_OBJECT_HPP
#define OPENGL_OBJECT_HPP


#include <GL/glinclude.hh>

namespace stgl {

class OpenGLObject {
public:
    //GLuint id() const { return id_; }
    const GLuint& id() const { return id_; }    

    // Disallow copy;
    OpenGLObject(const OpenGLObject&) = delete;
    OpenGLObject& operator=(const OpenGLObject&) = delete;

    // Allow move semanitcs.
    OpenGLObject(OpenGLObject&& o) {
        id_ = o.id_;
        o.id_ = 0;
    }

    OpenGLObject& operator=(OpenGLObject&& o) {
        id_ = o.id_;
        o.id_ = 0;
        return *this;
    } 

protected:
    GLuint id_;
 
    OpenGLObject() : id_(0) {}

    //GLuint& id() { return id_; }
};

}
#endif
