#ifndef OPTIMIZED_INDEXED_GRID_MESH_DATA_HPP
#define OPTIMIZED_INDEXED_GRID_MESH_DATA_HPP

#include <cstdint>
#include <vector>
//#include <array>

#include <type_traits>

#include <TrianglePrimitive.hpp>

#include <IndexedMeshDataBase.hpp> 

namespace gct {

template<class IndexT, class Vec2T, 
                       class Vec2TAccess = GenericVec2Access<Vec2T>, 
                       class Vec2TOps = GenericVec2Ops<Vec2T,Vec2TAccess>>
class OptimizedIndexedGridMeshData 
    : public IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps> {

    using VA = Vec2TAccess;
    using VO = Vec2TOps;

    using B = IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps>;

public:
    static const TrianglePrimitive Primitive = TrianglePrimitive::Triangle;

    enum class MeshPattern : uint8_t
        {Regular = 0, Cross = 1};

    enum class ProvokingQuadVert : uint8_t
        {BottomLeft = 0b00, BottomRight = 0b01, TopLeft = 0b10, TopRight = 0b11};

    OptimizedIndexedGridMeshData() {}
    OptimizedIndexedGridMeshData
            (const Vec2T& p0, IndexT width, IndexT height, const Vec2T& scale, 
             IndexT cache_size, 
             MeshPattern mesh_pattern = MeshPattern::Regular, 
             ProvokingQuadVert start_prov_vert = ProvokingQuadVert::TopRight) {

        cache_size = cache_size == 0 ? 2*width + 1 : cache_size;

        GenerateGridVertices(p0, width, height, scale, cache_size);
        GenerateGridIndices(width, height, cache_size, mesh_pattern, start_prov_vert);
    }
/*
    static constexpr TrianglePrimitive primitive() {
        return TrianglePrimitive::Traingle;
    } 
*/
private:

    static
    IndexT width_after_part(IndexT width, IndexT cache_size) {
        return  width + (width/cache_size + std::min(1, width%cache_size) - 1);
    }    

    void GenerateSubGridVertices
            (const Vec2T& p0, IndexT width, IndexT height, const Vec2T& scale) {

        for (IndexT y = 0; y < height; ++y) {
            for (IndexT x = 0; x < width; ++x) { 
                B::vertices().push_back(VO::add(p0, 
                                        VO::mul(VO::make(x, y), scale)));
            }
        }
    }

    void GenerateGridVertices
            (const Vec2T& p0, IndexT width, IndexT height, const Vec2T& scale, 
             IndexT cache_size) {
        //const IT part = width/cache_size + std::min(1, width%cache_size) - 1;

        const IndexT w = width_after_part(width, height);  
 
        B::vertices().reserve(w * height);

        //using Vec2T_xT = typename std::decay_t<decltype(VA::x(p0))>;

        Vec2T pc = p0; 
        for (int i = 0; i < w/cache_size; ++i) {
            GenerateSubGridVertices(pc, cache_size, height, scale);
            VA::x(pc) = VA::x(pc) + cache_size - 1;
        }
    
        GenerateSubGridVertices(pc, (w%cache_size), height, scale);
    }


    void GenerateSubGridIndices(IndexT offset, IndexT width, IndexT height, 
                                MeshPattern mesh_pattern, ProvokingQuadVert start_pqv,
                                bool enable_precaching = true) {
 
        auto emit_top_left_quad = [&](IndexT i, IndexT j) {
            B::indices().push_back(offset + width*(i+0) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+0);
                
            B::indices().push_back(offset + width*(i+0) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+0);
        };

        auto emit_top_right_quad = [&](IndexT i, IndexT j) {
            B::indices().push_back(offset + width*(i+1) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+0);
            B::indices().push_back(offset + width*(i+1) + j+1);
             
            B::indices().push_back(offset + width*(i+0) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+1);
        };

        auto emit_bottom_right_quad = [&](IndexT i, IndexT j) {
            B::indices().push_back(offset + width*(i+1) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+1);
             
            B::indices().push_back(offset + width*(i+1) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+1);
        };

        auto emit_bottom_left_quad = [&](IndexT i, IndexT j) {
            B::indices().push_back(offset + width*(i+1) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+0);
            B::indices().push_back(offset + width*(i+0) + j+0);
             
            B::indices().push_back(offset + width*(i+0) + j+1);
            B::indices().push_back(offset + width*(i+1) + j+1);
            B::indices().push_back(offset + width*(i+0) + j+0);
        };

        if (enable_precaching) {
            for (IndexT j = 0; j < width-1; ++j) {
                B::indices().push_back(offset + j+0);
                B::indices().push_back(offset + j+1);
                B::indices().push_back(offset + j+1);
            }
        }

        for (IndexT i = 0; i < height-1; ++i) {
            for (IndexT j = 0; j < width-1; ++j) {

                ProvokingQuadVert pqv = mesh_pattern == MeshPattern::Regular
                    ? start_pqv
                    : compute_pqv(start_pqv, i, j);

                if      (pqv == ProvokingQuadVert::TopRight) emit_top_right_quad(i, j);
                else if (pqv == ProvokingQuadVert::TopLeft) emit_top_left_quad(i, j);
                else if (pqv == ProvokingQuadVert::BottomRight) emit_bottom_right_quad(i, j);
                else if (pqv == ProvokingQuadVert::BottomLeft) emit_bottom_left_quad(i, j); 
            }
        }
    }

    void GenerateGridIndices(IndexT width, IndexT height, IndexT cache_size, 
                             MeshPattern mesh_pattern, ProvokingQuadVert start_pqv) {
        B::indices().reserve((width-1)*(height-1)*6 + (width-1)*3); 
        const IndexT w = width_after_part(width, cache_size);
        
        IndexT offset = 0; 
        for (IndexT i = 0; i < w/cache_size; ++i) {
            GenerateSubGridIndices(offset, cache_size, height, mesh_pattern, start_pqv);

            offset += height*cache_size;
            start_pqv = mesh_pattern == MeshPattern::Regular 
                ? start_pqv
                : compute_pqv(start_pqv, 0, cache_size + 1);
        }
    
        const bool precaching = cache_size < 2*width; 
        GenerateSubGridIndices(offset, (w%cache_size), height, mesh_pattern, start_pqv, precaching);
    }

    ProvokingQuadVert compute_pqv(ProvokingQuadVert pqv, IndexT i, IndexT j) {
        j += static_cast<IndexT>(pqv) & 1;
        i += static_cast<IndexT>(pqv) >> 1;

        return static_cast<ProvokingQuadVert>(((i % 2) << 1) + (j % 2));
    }

};

//#include <OptimizedIndexedGridMeshData.inl>

}

#endif
