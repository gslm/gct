#ifndef OPENGL_TERRAIN_SHADEING_SOURCE_GLSL_HPP
#define OPENGL_TERRAIN_SHADEING_SOURCE_GLSL_HPP

class OpenGLTerrainShadingSource {
    using Program = stgl::OpenGLProgram<OpenGLTerrainShadingSource>;
public:
    STGL_OPENGL_SHADER(GL_VERTEX_SHADER) {
    return std::string( //BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450 core

layout(early_fragment_tests) in;
layout(location = 0) in vec2 position;

layout(location = 0) out vec2 uv;

void main() {
    uv = position;
    gl_Position = vec4(vec2(position), 0.0, 1.0);
}
)GLSLSOURCE" ); 
//END_GLSLSOURCE
    }

    STGL_OPENGL_SHADER(GL_FRAGMENT_SHADER) {
        return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450

layout(early_fragment_tests) in;
layout(location = 0, binding = 0) uniform sampler2D texture_coords_buffer;
layout(location = 1, binding = 1) uniform sampler2D texture_coords_dF_buffer;
layout(location = 2, binding = 2) uniform sampler2D terrain_data_buffer;
layout(location = 3, binding = 3) uniform sampler2D depth_buffer;
layout(location = 4, binding = 4) uniform sampler2DArray wang_tile_atlas;

layout(location = 5) uniform mat4 inv_projection;

layout(location = 0) out vec3 color;

vec3 TerrainColor(float ny, vec2 coords, vec4 coords_dF) {
    vec3 c0 = vec3(0.0);
    vec3 c1 = vec3(0.0);
    const vec2 se = vec2(0.7, 0.8);

   /* if (ny > se.x)*/ c0 = textureGrad(wang_tile_atlas, vec3(coords, 0.0),
                                    coords_dF.xy, coords_dF.zw).rgb;
   /* if (ny < se.y) */c1 = textureGrad(wang_tile_atlas, vec3(coords, 1.0), 
                                   coords_dF.xy, coords_dF.zx).rgb;
    return mix(c1, c0, smoothstep(se.x, se.y, ny));
}

void main() {
    ivec2 texel_coords = ivec2(gl_FragCoord.xy);

    const vec3 l = normalize(vec3(1.0, 1.0, 1.0));
    
    vec4 terrain_data = texelFetch(terrain_data_buffer, texel_coords, 0);
    vec2 wt_coords = texelFetch(texture_coords_buffer, texel_coords, 0).rg;
    vec4 wt_coords_dF = texelFetch(texture_coords_dF_buffer, texel_coords, 0).rgba;
    float depth = texelFetch(depth_buffer, texel_coords, 0).r;
    //vec4 dist = inv_projection * vec4(uv.x, uv.y, depth, 1.0);

    vec3 tc = TerrainColor(-terrain_data.y, wt_coords, wt_coords_dF); 

    color = tc * max(0.0, dot(-terrain_data.xyz, l)) * 0.8 + tc * 0.2;
}
)GLSLSOURCE"); 
//END_GLSLSOURCE
    
    }

    template<GLenum TextureFormat>
    void set_texture_coords_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& texture_coords_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[0] = texture_coords_buffer.id();        
    }

    template<GLenum TextureFormat>
    void set_texture_coords_dF_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& texture_coords_dF_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[1] = texture_coords_dF_buffer.id();        
    } 

    template<GLenum TextureFormat>
    void set_terrain_data_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& terrain_data_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[2] = terrain_data_buffer.id(); 
    }

    template<GLenum TextureFormat>
    void set_depth_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                                    GL_TEXTURE_2D>& depth_buffer) {
        //static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[3] = depth_buffer.id(); 
    }

    template<GLenum TextureFormat>
    void set_wang_tile_atlas(const stgl::OpenGLTexture<TextureFormat, 
                                                 GL_TEXTURE_2D_ARRAY>& wang_tile_atlas) { 
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[4] = wang_tile_atlas.id(); 
    }

    template<GLenum Primitive>
    void Draw(const stgl::OpenGLBuffer<uint8_t>& indices, 
              const stgl::OpenGLBuffer<glm::vec2>& vertices) {
        static_assert(stgl::is_primitive<Primitive>::value, "");
        glUseProgram(Program::get_id(this));

        for (int i = 0; i < textures_.size(); ++i) {
            glBindTextureUnit(i, textures_[i]);
        }
        
        glBindBuffer(GL_ARRAY_BUFFER, vertices.id());
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 
                              reinterpret_cast<GLvoid*>(0));
 
        //Draw Call
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.id());
        glDrawElements(Primitive, indices.size(), GL_UNSIGNED_BYTE, 
                       reinterpret_cast<void*>(0));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
        glDisableVertexAttribArray(0);

        glUseProgram(0);
    }

private:
    std::array<GLuint, 5> textures_ = {{0,0,0,0,0}};
};

#endif 
