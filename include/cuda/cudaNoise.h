#ifndef CUDA_NOISE_H
#define CUDA_NOISE_H

//#include <cuda.h>
#include <helper_math.h>

//namespace gct { 

inline __host__ __device__
unsigned cuHash(unsigned x) {
    x += x << 10u;
    x ^= x >> 6u;
    x += x << 3u;
    x ^= x >> 11u;
    x += x << 15u;

    return x;
}

inline __host__ __device__
float cuHash(float x) {
    unsigned h = (cuHash(*reinterpret_cast<unsigned*>(&x)) >> 9) | 0x40000000;
    return (*reinterpret_cast<float*>(&h)) - 3.0f; 
}
/*
inline __host__ __device__
float chHash(float2 p) {
    uint2 h = *reinterpret_cast<uint2*>(&p);
}
*/
inline __host__ __device__
float2 cuHash(float2 p) {
    uint2 b = *reinterpret_cast<uint2*>(&p);
 
    uint2 h = make_uint2((cuHash(b.x            + cuHash(b.y)) >> 9u) | 0x40000000u, 
                         (cuHash((b.x + 73459u) + cuHash(b.y)) >> 9u) | 0x40000000u); 

    return *reinterpret_cast<float2*>(&h) - 3.0f;
}

/*
inline __host__ __device__ 
float2 cuHash(float2 p) {
 
    float2 a = make_float2( dot(p, make_float2(127.1f, 311.7f)),
			                dot(p, make_float2(299.5f, 783.3f)) );

	return -1.0f + 2.0f*fracf(make_float2(sin(a.x), sin(a.y))*43758.545f);
    

    //uint2 bits = reinterpret_cast<uint2&>(p);
    //uint2 hash = make_uint2(cuHash(bits.x), cuHash(bits.y));
    //p = reinterpret_cast<float2&>(hash);
    //return p * 2.3283064365386963e-10f * 2.0f - 1.0f;
}
*/

/*
inline __host__ __device__ 
float3 cuHash(const float3& p) {
    (1,1,0),(-1,1,0),(1,-1,0),(-1,-1,0),
    (1,0,1),(-1,0,1),(1,0,-1),(-1,0,-1),
    (0,1,1),(0,-1,1),(0,1,-1),(0,-1,-1) select one of these
    return make_float3(p.x, p.y, p.z);
}
*/


inline __host__ __device__ 
float cuNoise(const float2& p) {
    const float2 p00 = floorf(p);
    const float2 p10 = make_float2(p00.x + 1.0f, p00.y);
    const float2 p01 = make_float2(p00.x, p00.y + 1.0f);
    const float2 p11 = make_float2(p00.x + 1.0f, p00.y + 1.0f);

    const float2 s = p - p00;

    const float a = dot(cuHash(p00), s);
	const float b = dot(cuHash(p10), p - p10);
	const float c = dot(cuHash(p01), p - p01);
	const float d = dot(cuHash(p11), p - p11);

	const float qx = s.x*s.x*s.x*(s.x*(s.x*6.0f - 15.0f) + 10.0f);
	const float qy = s.y*s.y*s.y*(s.y*(s.y*6.0f - 15.0f) + 10.0f);

	//const float dqx = 30.0f * s.x*s.x*(s.x*(s.x - 2.0f) + 1.0f);
	//const float dqy = 30.0f * s.y*s.y*(s.y*(s.y - 2.0f) + 1.0f);

    const float c0 = a;
    const float c1 = b - a;
    const float c2 = c - a;
    const float c3 = d - c - b + a;

    return c0 + qx*c1 + qy*c2 + qx*qy*c3;
/* 
    return make_float3(c0 + qx*c1 + qy*c2 + qx*qy*c3,
                       (c1 + qy*c3) * dqx, (c2 + qx*c3) * dqy);*/
}

/*
inline __host__ __device__ 
void cuDnoise(const float3& p, float& h, float3& d) {
    float3 p0 = floorf(p);
    float3 p1 = make_float3(p0.x + 1.0f, p0.y, p0.z);
    float3 p2 = make_float3(p0.x, p0.y + 1.0f, p0.z);
    float3 p3 = make_float3(p0.x + 1.0f, p0.y + 1.0f, p0.z);
    float3 p4 = make_float3(p0.x, p0.y, p0.z + 1.0f);
    float3 p5 = make_float3(p0.x + 1.0f, p0.y, p0.z + 1.0f);
    float3 p6 = make_float3(p0.x, p0.y + 1.0f. p0.z + 1.0f);
    float3 p7 = make_float3(p0.x + 1.0f, p0.y + 1.0f, p0.z + 1.0f); 

    float3 s = p - p0;

    float a = dot(cuHash(p0), s);
	float b = dot(cuHash(p1), p - p1);
	float c = dot(cuHash(p2), p - p2);
	float d = dot(cuHash(p3), p - p3);
    float e = dot(cuHash(p4), p - p4); 
    float f = dot(cuHash(p5), p - p5); 
    float g = dot(cuHash(p6), p - p6); 
    float h = dot(cuHash(p7), p - p7); 

	const float qx = s.x*s.x*s.x*(s.x*(s.x*6.0f - 15.0f) + 10.0f);
	const float qy = s.y*s.y*s.y*(s.y*(s.y*6.0f - 15.0f) + 10.0f);
	const float qz = s.z*s.z*s.z*(s.z*(s.z*6.0f - 15.0f) + 10.0f);

	const float dqx = 30.0f * s.x*s.x*(s.x*(s.x - 2.0f) + 1.0f);
	const float dqy = 30.0f * s.y*s.y*(s.y*(s.y - 2.0f) + 1.0f);
	const float dqz = 30.0f * s.z*s.z*(s.z*(s.z - 2.0f) + 1.0f);

    const float c0 = a;
    const float c1 = b - a;
    const float c2 = c - a;
    const float c3 = e - a;
    const float c4 = a - b - c + d;
    const float c5 = a - c - e + g;
    const float c6 = a - b - e + f; 
    const float c7 = a + b + c - d + e - f - g + h;

    h = c0 + qx*c1 + qy*c2 + qz*c3 + qx*qy*c4 + qy*qz*c5 + qx*qz*c6 + qx*qy*qz*c7;

	d.x = dqx * (c1 + qy*c3);
    d.y = (c2 + qx*c3) * dqy;
    d.z = dqx * (c3 + qx*c6 + qy*c5 + qx*qy*c7);
}
*/

inline __host__ __device__ 
float cuFbm(float2 p, 
            int octaves, float amplitude, float lacunarity, float invH) {

    //invH = 1.0/1.8;

    //octaves = 12;
/*
    float sf = 1.0f;
    float h = 1.0f;

    for (int i = 0; i < octaves; ++i) {
        h *=  (cuNoise(p)) * sf + 0.976;
        p *= lacunarity;
        p += make_float2(23.43f, 34.2f);
        sf *= invH;
    }
*/

/*
    float h = 0.0f;
    for (int i = 0; i < octaves; ++i) {
        h += cuNoise(p) * pow(lacunarity, -1.0/invH*float(i));
        p *= lacunarity;
    }
*/

    float h = 0.2 + cuNoise(p);
    p *= lacunarity;
    for (int i = 1; i < octaves; ++i) {
        float inc = (cuNoise(p) + 0.2) * pow(lacunarity, (-1.0/invH)*float(i)) * h;
        h += inc;
        p *= lacunarity;
        p += make_float2(43.2f, 342.2f);
    }
    //return cuNoise(p*0.0002f) * amplitude*0.2f;
    return h * amplitude;
}

#endif
