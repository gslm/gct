#ifndef CUDA_TERRAIN_GENERATION_H
#define CUDA_TERRAIN_GENERATION_H

#ifdef __CUDACC__
#include <CudaGLSurfaceObject.hpp>
#else
#include <cuda/CudaGLSurfaceObject.hpp>
#endif

namespace gct {

void cudaDeviceGenerateRectangleHeight(
        const CudaGLSurfaceObject& surf, 
        unsigned surf_layer,
        int rect_coords_x, int rect_coords_y, 
        unsigned rect_dim_x, unsigned rect_dim_y,
        unsigned surf_dim_x, unsigned surf_dim_y, unsigned surf_dim_z,
        int octaves, float frequency, float amplitude, 
        float lacunarity, float invH);

void cudaDeviceGenerateRectangleNormal(
        const CudaGLSurfaceObject& surf, 
        unsigned surf_layer,
        int rect_coords_x, int rect_coords_y, 
        unsigned rect_dim_x, unsigned rect_dim_y,
        unsigned surf_dim_x, unsigned surf_dim_y, unsigned surf_dim_z);


void cuSynchronize();

void cudaHostGeneratePos(float x, float y,
        int octaves, float frequency, float amplitude, 
        float lacunarity, float invH,
        float& nx, float& ny, float& nz, float& h);

}
#endif
