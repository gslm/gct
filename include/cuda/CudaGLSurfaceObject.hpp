#ifndef CUDA_GL_SURFACE_OBJECT_HPP
#define CUDA_GL_SURFACE_OBJECT_HPP

class CudaGLSurfaceObject {

public:
    using cudaSurfaceObject_t = unsigned long long ;
    using GLuint = unsigned;
    using GLenum = GLuint;

private:
    cudaSurfaceObject_t object_;

public:
    CudaGLSurfaceObject();
    ~CudaGLSurfaceObject();

    bool Register(GLuint id, GLenum target, GLuint layer = 0);
    const cudaSurfaceObject_t& object() const;
    bool good() const;
};

#endif
