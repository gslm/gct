#ifndef GCT_FIXUP_MESH_DATA_HPP
#define GCT_FIXUP_MESH_DATA_HPP

#include <OptimizedIndexedGridMeshData.hpp>

namespace gct {

template<class IndexT, class Vec2T, 
                       class Vec2TAccess = GenericVec2Access<Vec2T>, 
                       class Vec2TOps = GenericVec2Ops<Vec2T,Vec2TAccess>>
class FixupMeshData 
    : public IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps> {

    using VA = Vec2TAccess;
    using VO = Vec2TOps;

    using B = IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps>;
    using Grid = OptimizedIndexedGridMeshData<IndexT, Vec2T, VA, VO>;

public:
    
    static const TrianglePrimitive Primitive = Grid::Primitive;

    FixupMeshData() {};
    FixupMeshData(size_t height) {
        Grid side_data(VO::make(0, 0), 3, height, VO::make(1,1), 0); 
        B::vertices() = std::vector<Vec2T>(side_data.cvertices().size() * 4);

        auto v_out_begin = B::vertices().begin();
        auto v_in_begin = side_data.cvertices().begin();
        auto v_in_end = side_data.cvertices().end();

        const size_t v_size = side_data.cvertices().size(); 

        std::transform(v_in_begin, v_in_end, v_out_begin + v_size*0, 
            [&](const Vec2T& p) { return VO::add(p, VO::make(-1, height)); });

        std::transform(v_in_begin, v_in_end, v_out_begin + v_size*1, 
            [&](const Vec2T& p) { return VO::add(p, VO::make(-1, -(2*height - 1))); });

        std::transform(v_in_begin, v_in_end, v_out_begin + v_size*2, 
            [&](const Vec2T& p) { return VO::add(VO::make(VA::y(p), VA::x(p)), 
                                         VO::make(height, -1)); });

        std::transform(v_in_begin, v_in_end, v_out_begin + v_size*3, 
            [&](const Vec2T& p) { return VO::add(VO::make(VA::y(p), VA::x(p)),
                                         VO::make(-(2*height -1), -1)); });

/*
        std::copy(v_in_begin, v_in_end, v_out_begin + v_size*0);
        std::copy(v_in_begin, v_in_end, v_out_begin + v_size*1);
        std::copy(v_in_begin, v_in_end, v_out_begin + v_size*2);
        std::copy(v_in_begin, v_in_end, v_out_begin + v_size*3);
*/
        B::indices() = std::vector<IndexT>(side_data.cindices().size() * 4);

        auto i_out_begin = B::indices().begin();
        auto i_in_begin = side_data.cindices().begin();
        auto i_in_end = side_data.cindices().end();
        
        const size_t i_size = side_data.cindices().size();

        for (size_t o = 0; o < 4; ++o) { 
            std::transform(i_in_begin, i_in_end, i_out_begin + i_size*o,
                [&](const IndexT& i) -> IndexT { return i + v_size*o; });
        }

    }
};

}
#endif
