#ifndef CUDA_GL_SURFACE_HPP
#define CUDA_GL_SURFACE_HPP

#include <stgl/OpenGLTexture.hpp>
#include <stgl/OpenGLEnumsTraits.hpp>

#include <cuda/CudaGLSurfaceObject.hpp>

#include <vector>

namespace gct {

namespace CudaGLSurface_detail {
/*
    template<class...>
    using void_t = void;
*/ 
    template<GLenum Target, class = void>
    struct CudaSurfaceObjectHolder {
        CudaGLSurfaceObject surface;
    };
    
    template<GLenum Target> 
    struct CudaSurfaceObjectHolder<Target, 
    std::enable_if_t<stgl::is_texture_array<Target>::value>> {    
        std::vector<CudaGLSurfaceObject> surface;
    };

}

template<GLenum InternalFormat, GLenum Target>
class CudaGLSurface : public stgl::OpenGLTexture<InternalFormat, Target> {
    static_assert(stgl::is_one_GLenum_of<Target,
                                   GL_TEXTURE_2D, 
                                   GL_TEXTURE_RECTANGLE, 
                                   GL_TEXTURE_CUBE_MAP, 
                                   GL_TEXTURE_3D, 
                                   GL_TEXTURE_2D_ARRAY,
                                   GL_RENDERBUFFER>::value, "");

    template<GLenum T>
    using CudaSurfaceObjectHolder = 
            CudaGLSurface_detail::CudaSurfaceObjectHolder<T>;

    CudaSurfaceObjectHolder<Target> surface_holder_;

public:
    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_1D_texture_target<T>::value>>
    CudaGLSurface(GLuint width, GLuint levels = 1) 
        : stgl::OpenGLTexture<InternalFormat, Target>(width, levels) {
        RegisterSurface(stgl::OpenGLObject::id());
    }
    
    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_2D_texture_target<T>::value>>
    CudaGLSurface(GLuint width, GLuint height, GLuint levels = 1) 
        : stgl::OpenGLTexture<InternalFormat, Target>(width, height, levels) {
        RegisterSurface(stgl::OpenGLObject::id());
    }

    template<GLenum T = Target, 
    class = std::enable_if_t<stgl::is_3D_texture_target<T>::value>>
    CudaGLSurface(GLuint width, GLuint height, GLuint depth, GLuint levels = 1)
        : stgl::OpenGLTexture<InternalFormat, Target>(width, height, depth, levels) {
        RegisterSurface(stgl::OpenGLObject::id(), depth);
    }

    //CudaGLSurface(CudaGLSurface&&) = default;
    //~CudaGLSurface() {}

    template<GLenum T = Target>
    std::enable_if_t<not stgl::is_texture_array<T>::value,
    CudaGLSurfaceObject> surface() {
        return surface_holder_.surface;
    }

    template<GLenum T = Target>
    std::enable_if_t<stgl::is_texture_array<T>::value,
    CudaGLSurfaceObject> surface(int layer) {
        return surface_holder_.surface[layer];
    }
    
private:
    template<GLenum T = Target>
    std::enable_if_t<not stgl::is_texture_array<T>::value,
    bool> RegisterSurface(GLuint id) {
        return surface_holder_.surface.Register(id, T, 0);
    }

    template<GLenum T = Target>
    std::enable_if_t<stgl::is_texture_array<T>::value,
    bool> RegisterSurface(GLuint id, GLuint layers) {
        surface_holder_.surface = std::vector<CudaGLSurfaceObject>(layers);

        for (GLuint layer = 0; layer < layers; ++layer) {
            bool success = surface_holder_.surface[layer].Register(id, T, layer);
            if (not success) return false; 
        }

        return true;
    }
};

}
#endif

