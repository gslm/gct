#ifndef GENERIC_VEC2_ACCESS_HPP
#define GENERIC_VEC2_ACCESS_HPP

#include <type_traits>

template<class Vec2T>
struct GenericVec2Access {
    static inline decltype(std::declval<Vec2T>().x)& x(Vec2T& v) {
        return v.x;
    }
    
    static inline decltype(std::declval<Vec2T>().y)& y(Vec2T& v) {
        return v.y;
    }

    static inline const decltype(std::declval<Vec2T>().x)& x(const Vec2T& v) {
        return v.x;
    }

    static inline const decltype(std::declval<Vec2T>().y)& y(const Vec2T& v) {
        return v.y;
    }
};

#endif
