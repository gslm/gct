#ifndef CAST_CHAIN
#define CAST_CHAIN

namespace gct {

namespace CastChain_detail {

//template<class... List>
//struct head;
//
//template<class Head>
//struct head<Head> { using type = Head; };
//
//template<class Head, class... List>
//struct head<Head, List...> { using type = Head; };

//template<class... List>
//using head_t = typename head<List...>::type;

template<class... List>
struct tail;

template<class Tail>
struct tail<Tail> { using type = Tail; };

template<class T, class... List>
struct tail<T, List...> { using type = typename tail<List...>::type; };

template<class... List>
using tail_t = typename tail<List...>::type;

template<class... Chain>
struct cast_chain;

template<class T>
struct cast_chain<T> { 
    template<class U>
    static inline T cast(const U& v) { return static_cast<T>(v); }
};

template<class T, class... Chain>
struct cast_chain<T, Chain...> {
    template<class U>
    static inline tail_t<Chain...> cast(const U& v) {
        return cast_chain<Chain...>::cast(static_cast<T>(v));
    }
};

}

template<class... Chain>
class CastChain {
public:    
    CastChain() {}

    template<class T>
    CastChain(T v) {
        value_ = CastChain_detail::cast_chain<Chain...>::cast(v);
    }

    operator CastChain_detail::tail_t<Chain...>() { return value_; }

private:
    CastChain_detail::tail_t<Chain...> value_;
};

}

#endif
