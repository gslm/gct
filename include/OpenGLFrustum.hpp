#ifndef OPENGL_FUSTRUM_HPP
#define OPENGL_FUSTRUM_HPP

#include <glm/glm.hpp>

#include <array>
#include <tuple>

namespace gct {

class OpenGLFrustum {
    enum : size_t { LEFT = 0, RIGHT = 1, TOP = 2, BOTTOM = 3,
                    NEAR = 4, FAR = 5 };
    std::array<glm::vec4, 6> planes_;

    enum : size_t { TLN = 0, TRN = 1, BLN = 2, BRN = 3,
                    TLF = 4, TRF = 5, BLF = 6, BRF = 7 };
    std::array<glm::vec3, 8> points_;

public:
    OpenGLFrustum() {
        Update(glm::mat4(1.0));
    }

    OpenGLFrustum(const glm::mat4& mvp_matrix) {
        Update(mvp_matrix);
    }

    void Update(const glm::mat4& mvp_matrix) {
        ComputePlanes(mvp_matrix);
        ComputeBoxPoints(planes_);
    }

    bool AABBoxInFrustum(const glm::vec3& min, const glm::vec3& max) const {
        //return true;

        auto point_plane_test = 
            [](const glm::vec3& point, const glm::vec4& plane) -> int {
            return plane.x*point.x + plane.y*point.y + plane.z*point.z + plane.w > 0.0f;
            //return glm::dot(plane, glm::vec4(point, 1)) > 0.0f;
        };

        for (const auto& p : planes_) {
            int test = 0;

            test += point_plane_test(glm::vec3(min.x, min.y, min.z), p);
            test += point_plane_test(glm::vec3(max.x, min.y, min.z), p);
            test += point_plane_test(glm::vec3(min.x, max.y, min.z), p);
            test += point_plane_test(glm::vec3(min.x, min.y, max.z), p);
            test += point_plane_test(glm::vec3(max.x, max.y, min.z), p);
            test += point_plane_test(glm::vec3(min.x, max.y, max.z), p);
            test += point_plane_test(glm::vec3(max.x, min.y, max.z), p);
            test += point_plane_test(glm::vec3(max.x, max.y, max.z), p);

            if (test == 0) return false;
        }

        auto point_coord_test = [this] (const auto& pbox, auto component, 
                                        auto compare) {
            int test = 0;
            for (const auto& p : points_) test += compare(component(p), 
                                                          component(pbox));
            return test == 0;
        };

        if (point_coord_test(max, 
                             [](const glm::vec3& p) { return p.x; }, 
                             [](float a, float b) { return a < b; })) return false;
        if (point_coord_test(min, 
                             [](const glm::vec3& p) { return p.x; }, 
                             [](float a, float b) { return a > b; })) return false;
        if (point_coord_test(max,
                             [](const glm::vec3& p) { return p.y; }, 
                             [](float a, float b) { return a < b; })) return false;
        if (point_coord_test(min,
                             [](const glm::vec3& p) { return p.y; }, 
                             [](float a, float b) { return a > b; })) return false;
        if (point_coord_test(max,
                             [](const glm::vec3& p) { return p.z; }, 
                             [](float a, float b) { return a < b; })) return false;
        if (point_coord_test(min,
                             [](const glm::vec3& p) { return p.z; }, 
                             [](float a, float b) { return a > b; })) return false;
        return true;
    }

private:
    void ComputePlanes(const glm::mat4& mvp_matrix) {
        planes_[LEFT].x = mvp_matrix[0][3] + mvp_matrix[0][0];
        planes_[LEFT].y = mvp_matrix[1][3] + mvp_matrix[1][0];
        planes_[LEFT].z = mvp_matrix[2][3] + mvp_matrix[2][0];
        planes_[LEFT].w = mvp_matrix[3][3] + mvp_matrix[3][0];

        planes_[RIGHT].x = mvp_matrix[0][3] - mvp_matrix[0][0];
        planes_[RIGHT].y = mvp_matrix[1][3] - mvp_matrix[1][0];
        planes_[RIGHT].z = mvp_matrix[2][3] - mvp_matrix[2][0];
        planes_[RIGHT].w = mvp_matrix[3][3] - mvp_matrix[3][0];
         
        planes_[TOP].x = mvp_matrix[0][3] - mvp_matrix[0][1];
        planes_[TOP].y = mvp_matrix[1][3] - mvp_matrix[1][1];
        planes_[TOP].z = mvp_matrix[2][3] - mvp_matrix[2][1];
        planes_[TOP].w = mvp_matrix[3][3] - mvp_matrix[3][1];
        
        planes_[BOTTOM].x = mvp_matrix[0][3] + mvp_matrix[0][1];
        planes_[BOTTOM].y = mvp_matrix[1][3] + mvp_matrix[1][1];
        planes_[BOTTOM].z = mvp_matrix[2][3] + mvp_matrix[2][1];
        planes_[BOTTOM].w = mvp_matrix[3][3] + mvp_matrix[3][1]; 
        
        planes_[NEAR].x = mvp_matrix[0][3] + mvp_matrix[0][2];
        planes_[NEAR].y = mvp_matrix[1][3] + mvp_matrix[1][2];
        planes_[NEAR].z = mvp_matrix[2][3] + mvp_matrix[2][2];
        planes_[NEAR].w = mvp_matrix[3][3] + mvp_matrix[3][2];

        planes_[FAR].x = mvp_matrix[0][3] - mvp_matrix[0][2];
        planes_[FAR].y = mvp_matrix[1][3] - mvp_matrix[1][2];
        planes_[FAR].z = mvp_matrix[2][3] - mvp_matrix[2][2];
        planes_[FAR].w = mvp_matrix[3][3] - mvp_matrix[3][2];

        // normalize.
        for (auto& p : planes_) {
            p *= glm::inversesqrt(p.x*p.x + p.y*p.y + p.z*p.z);
        }
    }

    void ComputeBoxPoints(const std::array<glm::vec4, 6>& planes) {

        auto three_planes_intersection = [](const glm::vec4& p1, 
                                            const glm::vec4& p2, 
                                            const glm::vec4& p3) -> glm::vec3 {
            const glm::vec3 n1 = glm::vec3(p1.x, p1.y, p1.z);
            const glm::vec3 n2 = glm::vec3(p2.x, p2.y, p2.z);
            const glm::vec3 n3 = glm::vec3(p3.x, p3.y, p3.z);

            float det = glm::determinant(glm::mat3(n1, n2, n3));
            if (det == 0.0) return glm::vec3(0.0);
    
            return ((glm::cross(n2, n3) * -p1.w) + 
                    (glm::cross(n3, n1) * -p2.w) + 
                    (glm::cross(n1, n2) * -p3.w)) / det;
        };

        points_[TLN] = three_planes_intersection(planes[TOP], planes[LEFT], planes[NEAR]);
        points_[TRN] = three_planes_intersection(planes[TOP], planes[RIGHT], planes[NEAR]);
        points_[BLN] = three_planes_intersection(planes[BOTTOM], planes[LEFT], planes[NEAR]);
        points_[BRN] = three_planes_intersection(planes[BOTTOM], planes[RIGHT], planes[NEAR]);
        points_[TLF] = three_planes_intersection(planes[TOP], planes[LEFT], planes[FAR]);
        points_[TRF] = three_planes_intersection(planes[TOP], planes[RIGHT], planes[FAR]);
        points_[BLF] = three_planes_intersection(planes[BOTTOM], planes[LEFT], planes[FAR]);
        points_[BRF] = three_planes_intersection(planes[BOTTOM], planes[RIGHT], planes[FAR]);

    }
};

}
#endif
