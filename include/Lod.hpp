#ifndef LOD_HPP
#define LOD_HPP

#include <glm/glm.hpp>

namespace gct {

inline
unsigned compute_lod(float frame_displacement, float scale) {
    return unsigned(glm::log2(glm::floor(frame_displacement*scale) + 1.0f));
}

inline
unsigned compute_lod(const glm::mat4& inv_projection, 
                     const glm::vec2& viewport_size, float distance) {
/*
    glm::vec4 p1 = glm::vec3(0.5f, 0.5f, -distance);
    glm::vec4 p2 = glm::vec3((viewport_size.x + 1.0) / viewport_size.x, 
                             0.5f, -distance);

    p1 = inv_projection * p1;
    p2 = inv_projection * p2;

*/
    return unsigned(distance/250.0f);

//    return static_cast<unsigned>(std::abs(p1.x - p2.x)); 
}

inline
unsigned compute_lod(float fovy, float viewport_height, float distance,
                                                        float scale) {
    float ps = glm::tan(fovy * 0.5f) * distance * 2.0f / viewport_height * scale;
    return unsigned(glm::log2(glm::floor(ps) + 1.0f));
}


}

#endif
