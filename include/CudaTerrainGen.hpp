#ifndef CUDA_TERRAIN_GEN_HPP
#define CUDA_TERRAIN_GEN_HPP

#include <CudaGLSurface.hpp>
#include <cuda/cudaTerrainGeneration.h>

namespace gct {

class CudaTerrainGen {

    float amplitude_; 
    float frequency_; 
    float lacunarity_; 
    float H_;
    float offset_;
    float gain_;
    int octaves_;
    int seed_;

public:
    void SetGenParams(float amplitude, float frequency, float lacunarity, 
                          float H, float offset, float gain, int ocatves, 
                          int seed) {
        amplitude_ = amplitude; 
        frequency_ = frequency; 
        lacunarity_ = lacunarity; 
        H_ = H;
        offset_ = offset;
        gain_ = gain;
        octaves_ = ocatves;
        seed_ = seed;
    }

    void HostCompute(float x, float y, 
        float& nx, float& ny, float& nz, float& h) const {
        cudaHostGeneratePos(x, y, 
            octaves_, frequency_, amplitude_, lacunarity_, 1.0f/H_,
            nx, ny, nz, h);
    }

    void DeviceGenerate
            (int x, int y, int delta_x, int delta_y, int layer, 
             CudaGLSurface<GL_RGBA32F, GL_TEXTURE_2D_ARRAY>& surf) {

        if (delta_x == 0 and delta_y == 0) return;

        // The x and y passed to the function represent the center of the cached area.
        // In order to compute the area to update it is convinient 
        // to have the position of the bottom left corner.
        x -= surf.width()/2;
        y -= surf.height()/2;

        auto generate_height = [&](int x, int y, 
                                   int width, int height) -> void {
            cudaDeviceGenerateRectangleHeight(
                surf.surface(layer & (surf.depth() - 1)), layer, 
                x, y, width, height,
                surf.width(), surf.height(), surf.depth(),
                octaves_, frequency_, amplitude_, lacunarity_, 1.0/H_);           
        };

        auto generate_normals = [&](int x, int y, 
                                    int width, int height) -> void {
            cudaDeviceGenerateRectangleNormal(
                surf.surface(layer & (surf.depth() - 1)), layer, 
                x, y, width, height,
                surf.width(), surf.height(), surf.depth());
        };

        if (std::abs(delta_x) >= surf.width() or  
            std::abs(delta_y) >= surf.height()) {

            generate_height(x + delta_x, y + delta_y, 
                               surf.width(), surf.height());
            cuSynchronize();
            generate_normals(x + delta_x + 1, y + delta_y + 1, 
                             surf.width() - 1, surf.height() - 1);

            cuSynchronize();
            return;
        }

        auto positive = [](int a) -> int { return a > 0; };

        // Vertical patch.
        
        int vx = x + (1 - positive(delta_x)) * (delta_x) + 
                          positive(delta_x) * (surf.width() - 1);
        int vy = y + delta_y;
        int vwidth = std::abs(delta_x);
        int vheight = surf.height();
       
        generate_height(vx, vy, vwidth, vheight); 
        
        // Horizontal patch.
        int hx = x + positive(delta_x) * delta_x;
        int hy = y + (1 - positive(delta_y)) * delta_y + 
                      positive(delta_y) * surf.height();
        int hwidth =  surf.width() - std::abs(delta_x);
        int hheight = std::abs(delta_y);

        generate_height(hx, hy, hwidth, hheight);  

        cuSynchronize();

        //generate_normals(vx - glm::sign(delta_x), vy - glm::sign(delta_y), vwidth, vheight);
        //generate_normals(hx - glm::sign(delta_x), hy - glm::sign(delta_y), hwidth, hheight);
 
        generate_normals(vx - glm::sign(delta_x), vy - glm::sign(delta_y), vwidth, vheight);
        generate_normals(hx - glm::sign(delta_x), hy - glm::sign(delta_y), hwidth, hheight);
    
        cuSynchronize();
    }

    float max_height() const {
        return amplitude_*4.0f;
    }

    float min_height() const {
        return -amplitude_*4.0f;
    }
};

}

#endif
