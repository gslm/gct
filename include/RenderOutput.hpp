#ifndef RENDER_OUTPUT_HPP
#define RENDER_OUTPUT_HPP

#include <cstdint>

namespace gct {

enum class RenderOutput : std::uint8_t {
    none = 0,
    texture_coords = 1 << 0,
    texture_coords_dF = 1 << 1,
    terrain_data = 1 << 2,
    depth = 1 << 3,
    stencil = 1 << 4,
    depth_stencil = 1 << 5,
    terrain_render_mode_end = 1 << 6 // Disallow flags which are not combination 
                                     // of the defined ones.  
};

template<RenderOutput...> struct RenderOutputList {};

using RenderOutputElems = 
        RenderOutputList<RenderOutput::texture_coords,
                         RenderOutput::texture_coords_dF,
                         RenderOutput::terrain_data,
                         RenderOutput::depth,
                         RenderOutput::stencil,
                         RenderOutput::depth_stencil>;

/*
inline constexpr 
bool is_sub_output(RenderOutput set, RenderOutput t) {
    return (set & t) == t;
}
*/
inline constexpr RenderOutput 
operator&(RenderOutput a, RenderOutput b)
{ return RenderOutput(static_cast<std::uint8_t>(a) & 
                      static_cast<std::uint8_t>(b)); }

inline constexpr RenderOutput
operator|(RenderOutput a, RenderOutput b)
{ return RenderOutput(static_cast<std::uint8_t>(a) | 
                      static_cast<std::uint8_t>(b)); }

inline constexpr RenderOutput
operator^(RenderOutput a, RenderOutput b)
{ return RenderOutput(static_cast<std::uint8_t>(a) ^ 
                      static_cast<std::uint8_t>(b)); }

inline constexpr RenderOutput
operator~(RenderOutput a)
{ return RenderOutput(~static_cast<std::uint8_t>(a)); }

inline const RenderOutput&
operator&=(RenderOutput& a, RenderOutput b)
{ return a = a & b; }

inline const RenderOutput&
operator|=(RenderOutput& a, RenderOutput b)
{ return a = a | b; }

inline const RenderOutput&
operator^=(RenderOutput& a, RenderOutput b)
{ return a = a ^ b; }

namespace RenderOutput_detail {

template<RenderOutput O, RenderOutput... OList>
struct output_count_impl2 {
    static constexpr int value = 0;
};

template<RenderOutput E, RenderOutput Head, RenderOutput... Tail>
struct output_count_impl2<E, Head, Tail...> {
    static constexpr int value = 
        static_cast<int>((E & Head) != RenderOutput::none) + 
            output_count_impl2<E, Tail...>::value;
};

template<RenderOutput E, class T>
struct output_count_impl;

template<RenderOutput E, RenderOutput... L>
struct output_count_impl<E, RenderOutputList<L...>>
    : output_count_impl2<E, L...> {};

}

template<RenderOutput O>
using output_count = 
    typename RenderOutput_detail::output_count_impl<O, RenderOutputElems>;

template<RenderOutput O>
constexpr int output_count_v = output_count<O>::value;

template<RenderOutput O1, RenderOutput O2>
using is_output_subset = typename std::integral_constant<bool, (O1 & O2) == O2>;

template<RenderOutput O1, RenderOutput O2>
constexpr bool is_output_subset_v = is_output_subset<O1, O2>::value;

}


#endif
