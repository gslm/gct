#ifndef INTEGRAL_PROPERTIES_HPP
#define INTEGRAL_PROPERTIES_HPP

#include <limits>
#include <type_traits>

#include <Pow2.hpp>

namespace gct {

namespace detail {

template<class T>
class IntegralProperty {
    static_assert(std::is_integral<T>::value, "");

public:   
    operator T() const { return value_; };
    
protected:
    T value_;
    IntegralProperty() {}
};

}

template<class T, T v>
struct LessThanEq : public detail::IntegralProperty<T> {
    LessThanEq(const T& u) { this->value_ = std::min(v, u); }
};

template<class T, T v>
struct LessThan : public detail::IntegralProperty<T> {
    static_assert(v > std::numeric_limits<T>::min(), "");
    LessThan(const T& u) { this->value_ = std::min(v-1, u); }
};

template<class T, T v>
struct GreaterThanEq : public detail::IntegralProperty<T> {
    GreaterThanEq(const T& u) { this->value_ = std::max(v, u); }
};

template<class T, T v>
struct GreaterThan : public detail::IntegralProperty<T> {
    static_assert(v < std::numeric_limits<T>::max(), "");
    GreaterThan(const T& u) { this->value_ = std::max(v+1, u); }
};

template<class T, T v>
struct Equal : public detail::IntegralProperty<T> {
    Equal(const T& u) { this->value_ = v; }
};

template<class T>
struct Odd : public detail::IntegralProperty<T> {
    Odd(const T& u) { this->value_ = u - (1 - u%2); } 
};

template<class T>
struct Even : public detail::IntegralProperty<T> {
    Even(const T& u) { this->value_ = u - (u%2); } 
};
/*
template<class T>
struct Pow2Minus5 : public detail::IntegralProperty<T> {
    Pow2Minus5(const T& u) { this->value_ = Pow2<T>(u); }
};
*/

template<class T, T M, T K>
struct MtimesNplusK : public detail::IntegralProperty<T> {
    MtimesNplusK(const T& u) { this->value_ = M*((u - K) / M) + K; }
};

}

#endif
