#ifndef GCT_TRANSITIONAL_ZERO_AREA_MESH_DATA_HPP
#define GCT_TRANSITIONAL_ZERO_AREA_MESH_DATA_HPP

#include <algorithm>

#include <IndexedMeshDataBase.hpp> 

namespace gct {

template<class IndexT, class Vec2T, 
                       class Vec2TAccess = GenericVec2Access<Vec2T>, 
                       class Vec2TOps = GenericVec2Ops<Vec2T,Vec2TAccess>>
class TransitionalZeroAreaMeshData 
    : public IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps> {

    using VA = Vec2TAccess;
    using VO = Vec2TOps;

    using B = IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps>;

public:
    static const TrianglePrimitive Primitive = TrianglePrimitive::Triangle;

    TransitionalZeroAreaMeshData(int ring_size) {
        B::vertices().reserve((ring_size - 1) * 4);
        B::indices().reserve(3*ring_size/2 * 4);

        GenerateVertices(ring_size);        
        GenerateIndices(ring_size);
    }

private: 
    void GenerateVertices(int ring_size) {

        for (int i = 0; i < (ring_size - 1); ++i) {
            B::vertices().push_back(VO::make(0, i));
        }

        for (int i = 0; i < (ring_size - 1); ++i) {
            B::vertices().push_back(VO::add(VO::make(i, 0), 
                                           VO::make(0, ring_size)));
        }

        for (int i = 0; i < (ring_size - 1); ++i) {
            B::vertices().push_back(VO::add(VO::make(0, -i), 
                                            VO::make(ring_size, ring_size)));
        }

        for (int i = 0; i < (ring_size - 1); ++i) {
            B::vertices().push_back(VO::add(VO::make(-i, 0), 
                                            VO::make(-ring_size, 0)));
        }

/*
        using xT = typename std::decay_t<decltype(VA::x(std::declval<Vec2T>()))>;
        using yT = typename std::decay_t<decltype(VA::y(std::declval<Vec2T>()))>;
        auto make_Vec2T = [](auto x, auto y) {
            Vec2T p;
            VA::x(p) = static_cast<xT>(x);
            VA::y(p) = static_cast<yT>(y);
            return p;
        };
*/
/*
        const auto b = std::cbegin(verices_);
        std::transform(b + 0*(ring_size - 1), b + 1*(ring_size - 1), 
                       b + 0*(ring_size - 1),
            [](const Vec2T& p) { 
                return p; 
            }
        );
        
        std::transform(b + 1*(ring_size - 1), b + 2*(ring_size - 1),
                       b + 1*(ring_size - 1),
            [ring_size](const Vec2T& p) { 
                return VO::add(make_Vec2T(VA::y(p), VA::x(p)), 
                               make_Vec2T(0, ring_size));
            }
        );

        std::transform(b + 2*(ring_size - 1), b + 3*(ring_size - 1),
                       b + 2*(ring_size - 1),
            [ring_size](const Vec2T& p) { 
                return VO::add(make_Vec2T(VA::x(p), -VA::y(p)), 
                               make_Vec2T(ring_size, ring_size));
            }
        );

        std::transform(b + 3*(ring_size - 1), b + 4*(ring_size - 1), 
                       b + 3*(ring_size - 1),
            [ring_size](const Vec2T& p) { 
                return VO::add(make_Vec2T(-VA::y(p), VA::x(p)), 
                               make_Vec2T(-ring_size, 0));
            }
        );
*/
    }

    void GenerateIndices(IndexT ring_size) {
        for (IndexT i = 0; i < 3*ring_size / 2; ++i) {
            B::indices().push_back(2*i + 0); 
            B::indices().push_back(2*i + 0); 
            B::indices().push_back(2*i + 1);
         
            B::indices().push_back(2*i + 2); 
            B::indices().push_back(2*i + 0); 
            B::indices().push_back(2*i + 1);
 
            B::indices().push_back(2*i + 1); 
            B::indices().push_back(2*i + 2); 
            B::indices().push_back(2*i + 2); 
        } 
    }

};

}
#endif
