#ifndef GCT_TRIMS_MESH_DATA_HPP
#define GCT_TRIMS_MESH_DATA_HPP

#include <IndexedMeshDataBase.hpp>

namespace gct {

template<class IndexT, class Vec2T, 
                       class Vec2TAccess = GenericVec2Access<Vec2T>, 
                       class Vec2TOps = GenericVec2Ops<Vec2T,Vec2TAccess>>
class TrimsMeshData 
    : public IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps, 4, 1> {

    using VA = Vec2TAccess;
    using VO = Vec2TOps;

    using B = IndexedMeshDataBase<IndexT, Vec2T, Vec2TAccess, Vec2TOps, 4, 1>;
    using Grid = OptimizedIndexedGridMeshData<IndexT, Vec2T, VA, VO>;

public:
    enum Index : size_t { BottomLeft = 0, BottomRight = 1, 
                          TopLeft = 2, TopRight = 3 };
    
    static const TrianglePrimitive Primitive = Grid::Primitive;

    TrimsMeshData() {}
    TrimsMeshData(size_t m) {

        Grid v_grid(VO::make(0, 0), 2, 2*m + 1, VO::make(1,1), 0);
        Grid h_grid(VO::make(0, 0), 2, 2*m, VO::make(1,1), 0);         

        for (auto&& v : B::vertices()) {
            v.resize(v_grid.cvertices().size() + h_grid.cvertices().size());
        }

        const size_t v_offset = v_grid.cvertices().size();
        std::transform(v_grid.cvertices().begin(), v_grid.cvertices().end(),
                       B::vertices()[Index::BottomLeft].begin(),
                [&](const Vec2T& p) {
                    return VO::add(p, VO::make(-m, -m));
                });
        std::transform(h_grid.cvertices().begin(), h_grid.cvertices().end(),
                       B::vertices()[BottomLeft].begin() + v_offset,
                [&](const Vec2T& p) {
                    return VO::add(VO::make(VA::y(p), VA::x(p)),
                                   VO::make(-(m-1), -m));
                });

        std::transform(v_grid.cvertices().begin(), v_grid.cvertices().end(),
                       B::vertices()[BottomRight].begin(),
                [&](const Vec2T& p) {
                    return VO::add(p, VO::make(m-1, -m));
                });
        std::transform(h_grid.cvertices().begin(), h_grid.cvertices().end(),
                       B::vertices()[BottomRight].begin() + v_offset,
                [&](const Vec2T& p) {
                    return VO::add(VO::make(VA::y(p), VA::x(p)),
                                   VO::make(-m, -m));
                });

        std::transform(v_grid.cvertices().begin(), v_grid.cvertices().end(),
                       B::vertices()[TopLeft].begin(),
                [&](const Vec2T& p) {
                    return VO::add(p, VO::make(-m, -m));
                });
        std::transform(h_grid.cvertices().begin(), h_grid.cvertices().end(),
                       B::vertices()[TopLeft].begin() + v_offset,
                [&](const Vec2T& p) {
                    return VO::add(VO::make(VA::y(p), VA::x(p)),
                                   VO::make(-(m-1), m-1));
                });

        std::transform(v_grid.cvertices().begin(), v_grid.cvertices().end(),
                       B::vertices()[TopRight].begin(),
                [&](const Vec2T& p) {
                    return VO::add(p, VO::make(m-1, -m));
                });
        std::transform(h_grid.cvertices().begin(), h_grid.cvertices().end(),
                       B::vertices()[TopRight].begin() + v_offset,
                [&](const Vec2T& p) {
                    return VO::add(VO::make(VA::y(p), VA::x(p)),
                                   VO::make(-m, m-1));
                });

        B::indices().resize(v_grid.cindices().size() + h_grid.cindices().size());
 
        std::copy(v_grid.cindices().begin(), v_grid.cindices().end(), 
                  B::indices().begin());

        std::transform(h_grid.cindices().begin(), h_grid.cindices().end(),
                       B::indices().begin() + v_grid.cindices().size(),
                [&](const IndexT& i) -> IndexT { 
                    return i + v_grid.cvertices().size(); 
                });
    }
};

}

#endif
