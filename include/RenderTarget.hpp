#ifndef OPENGL_RENDER_TARGET_HPP
#define OPENGL_RENDER_TARGET_HPP

#include <memory>

template<GLenum Format>
class OpenGLRenderTarget : OpenGLObject {
    std::unique_ptr<OpenGLTexture<Format, GL_TEXTURE_2D>> texture2D;
    
    std::unique_ptr<OpenGLTexture<Format, GL_TEXTURE_2D_ARRAY>> texture2DArray;
    uint32_t texture2DArray_layer_;
    
public:
    OpenGLRenderTarget(std::unique_ptr<OpenGLTexture<Format, GL_TEXTURE_2D>> texture2D) {
        texture2D_ = std::move(texture2D);
    }

    OpenGLRenderTarget(std::unique_ptr<OpenGLTexture<Format, GL_TEXTURE_2D_ARRAY>> texture2DArray, uint32_t layer) {
        texture2DArray_ = std::move(texture2D);
        texture2DArray_layer_ = layer;
    }

    template<GLenum Attachement>
    void AttachToFramebuffer(OpenGLFramebuffer& framebuffer) {
        if (texture2D) framebuffer.AttachTexture<Attachement>(*texture2D);
        else if(texture2DArray) framebuffer.AttachTexture<Attachement>(*texture2DArray, layer);
    }
};



#endif
