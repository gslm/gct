#ifndef GCT_INDEXED_MESH_DATA_BASE_HPP
#define GCT_INDEXED_MESH_DATA_BASE_HPP

#include <cstdint>
#include <vector>
//#include <array>

#include <type_traits>

#include <TrianglePrimitive.hpp>

#include <GenericVec2Access.hpp>
#include <GenericVec2Ops.hpp>

namespace gct {

namespace IndexedMeshDataBase_detail {

    template<class V, size_t C>
    struct storage {
        static_assert(C > 1, "");
        using type = typename std::array<std::vector<V>, C>;
    };

    template<class V>
    struct storage<V, 1> {
        using type = typename std::vector<V>;
    };

    template<class V, size_t C>
    using storage_t = typename storage<V, C>::type;
} 

template<class IndexT, class Vec2T, 
                       class Vec2TAccess = GenericVec2Access<Vec2T>, 
                       class Vec2TOps = GenericVec2Ops<Vec2T,Vec2TAccess>,
                       size_t VerticesCount = 1, size_t IndicesCount = 1>
class IndexedMeshDataBase {
    using VA = Vec2TAccess;

    using Vec2xT = typename std::decay_t<decltype(VA::x(std::declval<Vec2T>()))>;
    using Vec2yT = typename std::decay_t<decltype(VA::y(std::declval<Vec2T>()))>;
 
    static_assert(std::is_integral<IndexT>::value, "");
    static_assert(std::is_same<Vec2xT, Vec2yT>::value, "");
    static_assert(std::is_signed<Vec2xT>::value, "");

    using VerticesStorage = 
        IndexedMeshDataBase_detail::storage_t<Vec2T, VerticesCount>; 
    using IndicesStorage = 
        IndexedMeshDataBase_detail::storage_t<IndexT, IndicesCount>;

    VerticesStorage vertices_;
    IndicesStorage indices_;

protected:
    static constexpr TrianglePrimitive DefaultPrimitive = TrianglePrimitive::Triangle; 
    
    VerticesStorage& vertices() { return vertices_;}
    IndicesStorage& indices() { return indices_; }

public:
    static constexpr TrianglePrimitive primitive() {
        return DefaultPrimitive; 
    }

    const VerticesStorage& vertices() const { return vertices_; }
    const IndicesStorage& indices() const { return indices_; }  
 
    const VerticesStorage& cvertices() const { return vertices_; }
    const IndicesStorage& cindices() const { return indices_; }  
     
};

}
#endif

