#ifndef OPENGL_SKY_SOURCE_GLSL_HPP
#define OPENGL_SKY_SOURCE_GLSL_HPP

class OpenGLSkySource {
    using Program = stgl::OpenGLProgram<OpenGLSkySource>;
public:  
    STGL_OPENGL_SHADER(GL_VERTEX_SHADER) {
    return std::string( //BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450 core

layout(early_fragment_tests) in;
layout(location = 0) in vec2 position;

layout(location = 0) out vec2 uv;

void main() {
    uv = (position + 1.0) * 0.5;
    gl_Position = vec4(vec2(position), 0.0, 1.0);
}
)GLSLSOURCE" ); 
//END_GLSLSOURCE
    }

    STGL_OPENGL_SHADER(GL_FRAGMENT_SHADER) {
        return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450

layout(early_fragment_tests) in;

layout(location = 0) uniform vec2 frustum_nf;
layout(location = 1) uniform vec4 frustum_lrbt;
layout(location = 2) uniform mat4 camera_transform;

layout(location = 0) in vec2 uv;

layout(location = 0) out vec3 color;

uint hash(in uint x) {
    x += x << 10;
    x ^= x >> 6;
    x += x << 3;
    x ^= x >> 11;
    x += x << 15;
	return x;
}

float hash(float x) {
    uint h = (hash(floatBitsToUint(x)) >> 9u) | 0x40000000u;
    return uintBitsToFloat(h) - 3.0f; 
}

float hash21(vec2 p) {
	return hash(hash(p.x) + hash(p.y*2.0));
}

// value noise.
float noise(vec2 p) {    
    vec2 i = floor(p);
    vec2 f = fract(p);
    
    float a = hash21(i);
	float b = hash21(i + vec2(1.0, 0.0));
	float c = hash21(i + vec2(0.0, 1.0));
	float d = hash21(i + vec2(1.0, 1.0));

	vec2 q = f*f*(3.0-2.0*f);
    
    float c1 = b - a;
    float c2 = c - a;
    float c3 = d - c - b + a;

   	return a + q.x*c1 + q.y*c2 + q.x*q.y*c3;
}

float fbm( vec2 p )
{
    const mat2 r = mat2(0.8,-0.6,0.6,0.8);
    float f = 0.0;
    f += 0.500*noise(p); p = r*p*2.01;
    f += 0.250*noise(p); p = r*p*2.03;
    f += 0.125*noise(p); p = r*p*2.02;
    return f*1.143;
}

const mat2 m2 = mat2(0.8,-0.6,0.6,0.8);
vec3 Sky(vec3 ro, vec3 rd, vec3 sun) {
    float s = max(0.0, dot(rd, sun));
    // sky		
	vec3 col = vec3(0.3,.55,0.8)*(1.0-0.3*rd.y)*0.9;
    // sun
	col += 0.21*vec3(1.0,0.7,0.1)*pow(s, 2.0);
	col += 0.12*vec3(1.0,0.7,0.5)*pow(s, 4.0);
	col += 0.61*vec3(1.0,0.7,0.5)*pow(s, 512.0);
    // clouds
	vec2 sc = ro.xz + rd.xz*(10000.0-ro.y)/rd.y;
	col = mix( col, vec3(1.0,0.95,1.0), 0.5*smoothstep(-0.2,0.9,fbm(0.00005*sc)) );
    // horizon
    col = mix( col, vec3(0.7,0.75,0.8), pow( 1.0-max(rd.y,0.0), 8.0 ) );
    
    return col;
}

void CastRay(out vec3 ro, out vec3 rd) {
    vec2 frustum_dim = vec2(frustum_lrbt.y - frustum_lrbt.x, 
                            frustum_lrbt.w - frustum_lrbt.z);
    rd = vec3(uv*frustum_dim + frustum_lrbt.xz, -frustum_nf.x);
    rd = normalize(mat3(camera_transform[0].xyz, 
                        camera_transform[1].xyz, 
                        camera_transform[2].xyz) * rd);
 
    ro = camera_transform[3].xyz;
}

void main() {
    const vec3 sun = normalize(vec3(1.0, 1.0, -1.0));
    vec3 ro = vec3(0.0);
    vec3 rd = vec3(0.0);
    
    CastRay(ro, rd);

    color = Sky(ro, rd, sun);
}
)GLSLSOURCE"); 
//END_GLSLSOURCE
    
    }

    void set_frustum(float left, float right, float bottom, float top, float near, float far) {
        glProgramUniform2fv(Program::get_id(this), 0, 1, 
                            glm::value_ptr(glm::vec2(near, far)));
        glProgramUniform4fv(Program::get_id(this), 1, 1, 
                            glm::value_ptr(glm::vec4(left, right, bottom, top)));
    }

    void set_camera_transform(const glm::mat4& camera_transform) {
        glProgramUniformMatrix4fv(Program::get_id(this), 2, 1, GL_FALSE, 
                                  glm::value_ptr(camera_transform));
    }

    template<GLenum Primitive>
    void Draw(const stgl::OpenGLBuffer<uint8_t>& indices, 
              const stgl::OpenGLBuffer<glm::vec2>& vertices) {
        static_assert(stgl::is_primitive<Primitive>::value, "");
        glUseProgram(Program::get_id(this));
       
        glBindBuffer(GL_ARRAY_BUFFER, vertices.id());
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 
                              reinterpret_cast<GLvoid*>(0));
 
        //Draw Call
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.id());
        glDrawElements(Primitive, indices.size(), GL_UNSIGNED_BYTE, 
                       reinterpret_cast<void*>(0));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
        glDisableVertexAttribArray(0);

        glUseProgram(0);
    }
};


#endif
