#ifndef TRIANGLE_PRIMITIVE_HPP
#define TRIANGLE_PRIMITIVE_HPP

namespace gct {

enum class TrianglePrimitive {
    Triangle,
    Strip,
    Fan
};

}

#endif
