#ifndef TERRAIN_DATA_CACHE_HPP
#define TERRAIN_DATA_CACHE_HPP

#include <TerrainTransform.hpp>

#include <type_traits>

namespace gct {

template<size_t LayerCount, template<size_t> class Impl> 
class TerrainDataCache;

template<template<size_t> class Filter, class CacheT>
class TerrainDataCacheView {
    
    friend CacheT; 

    using transform_view_t = std::decay_t<decltype(
                                std::declval<CacheT>().transform().template make_view<Filter>())>;

    const CacheT& cache_;
    transform_view_t transform_view_;

private:
    TerrainDataCacheView(const CacheT& cache) 
        : cache_(cache) 
        , transform_view_(
                    cache_.transform().template make_view<Filter>()) {} 

public:
    decltype(auto) data() const {
        return cache_.data();
    }

    decltype(auto) max_height() const {
        return cache_.max_height();
    }

    decltype(auto) min_height() const {
        return cache_.min_height();
    }
/*
    const auto& transform() const {
        return transform_view_;
    }*/

    decltype(auto) log2_scale(size_t layer) const {
        return transform_view_.log2_scale(layer);
    }

    decltype(auto) lattice_offset(size_t layer) const {
        return transform_view_.lattice_offset(layer);
    }

    static constexpr decltype(auto) layers() {
        return transform_view_t::layers();
    }

};

template<size_t E>
using DefaultTerrainDataCacheViewFilter =
    gct::OrderedIndexingSetFilter<E, true>;

template<size_t Layers, template<size_t> class Impl>
class TerrainDataCache : public Impl<Layers> {
    using this_t = TerrainDataCache<Layers, Impl>;
    using container_t = typename Impl<Layers>::container_t;

    container_t data_;
    TerrainTransform<Layers> transform_;

public:
    using vec2_t = typename TerrainTransform<Layers>::vec2_t;
    
    TerrainDataCache(uint32_t width, uint32_t height) 
        : data_(Impl<Layers>::make_container(width, height, Layers)) {}

    template<template<size_t> class Filter = DefaultTerrainDataCacheViewFilter>
    auto make_view() const {
        return TerrainDataCacheView<Filter, this_t>(*this);
    }

    auto GetDataAt(const vec2_t& p) const {
        return Impl<Layers>::GetDataAt(p, transform_, data_);
    }

    void Lod(unsigned lod) {
        Impl<Layers>::Lod(lod, transform_, data_);
    }

    void Translate(float delta_x, float delta_y) {
        Impl<Layers>::Translate(delta_x, delta_y, transform_, data_); 
    }

    void Init(float x, float y, uint32_t lod = 0) {
        Impl<Layers>::Init(x, y, lod, transform_, data_);
    }

    const auto& data() const {
        return data_;
    }

    float max_height() const {
        return Impl<Layers>::max_height();
    }

    float min_height() const {
        return Impl<Layers>::min_height();
    }

    const auto& transform() const {
        return transform_;
    }
};

}

#endif
