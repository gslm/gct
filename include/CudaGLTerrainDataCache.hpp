#ifndef CUDA_GL_TERRAIN_DATA_CACHE
#define CUDA_GL_TERRAIN_DATA_CACHE

#include <TerrainDataCache.hpp>
#include <CudaGLDeviceHostGen.hpp>
#include <CudaGLSurface.hpp>

namespace gct {

template<size_t N>
using CudaGLTerrainDataCache = 
        TerrainDataCache<N, CudaGLDeviceHostGen>;

}

#endif
