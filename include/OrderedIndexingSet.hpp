#ifndef ORDERED_INDEXING_SET_HPP
#define ORDERED_INDEXING_SET_HPP

#include <utility>
#include <array>

namespace gct {

namespace detail {

template<size_t... Ints>
using ISeq = std::index_sequence<Ints...>;

template<class IT1, class IT2>
struct ISeqConcat;

template<size_t... A, size_t... B>
struct ISeqConcat<ISeq<A...>, ISeq<B...>> {
    using type = ISeq<A..., B...>;
};

template<template<size_t> class Filter, class ISeq>
struct FilteredISeq;

template<template<size_t> class Filter, size_t E, size_t... Rest>
struct FilteredISeq<Filter, ISeq<E, Rest...>> {
    using type = 
        typename ISeqConcat<
                    typename Filter<E>::type, 
                    typename FilteredISeq<Filter,  ISeq<Rest...>>::type>::type;
};

template<template<size_t> class Filter, size_t E>
struct FilteredISeq<Filter, ISeq<E>> {
    using type = typename Filter<E>::type;
};

template<template<size_t> class Filter>
struct FilteredISeq<Filter, ISeq<>> {
    using type = ISeq<>;  
};

template<class IT>
struct ISeqToArray;

template<size_t... Elems>
struct ISeqToArray< ISeq<Elems...> > {
    constexpr static const std::array<size_t, sizeof...(Elems)> array() {
        return {{Elems...}};
    }
};

}

template<size_t, bool>
struct OrderedIndexingSetFilter {
    using type = std::index_sequence<>;
};

template<size_t E>
struct OrderedIndexingSetFilter<E, true> {
    using type = std::index_sequence<E>;
};

template<size_t N>
struct OrderedIndexingSet {
    using type = typename std::make_index_sequence<N>;

    constexpr static decltype(auto) array() {
        return detail::ISeqToArray<type>::array();
    }

    constexpr static decltype(auto) size() {
        return type::size();
    }
};

template<class Set, template<size_t> class Filter>
struct OrderedIndexingSubset {
    using type = 
        typename detail::FilteredISeq<Filter, typename Set::type>::type;

    constexpr static decltype(auto) array() {
        return detail::ISeqToArray<type>::array();
    }

    constexpr static decltype(auto) size() {
        return type::size();
    }
};

}


#endif
