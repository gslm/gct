#ifndef TERRAIN_MODEL_HPP
#define TERRAIN_MODEL_HPP

#include <utility>
#include <memory>
#include <tuple>
#include <array>

#include <CastChain.hpp>
#include <IntegralProperties.hpp>

#include <TrianglePrimitive.hpp>
#include <OptimizedIndexedGridMeshData.hpp>
#include <TransitionalZeroAreaMeshData.hpp>
#include <FixupMeshData.hpp>
#include <TrimsMeshData.hpp>

#include <glm/glm.hpp>

namespace gct {

template<template<class, class> class DeviceMemoryAllocation, class IndexT, class Vec2T, 
         class Vec2TA = GenericVec2Access<Vec2T>, 
         class Vec2TO = GenericVec2Ops<Vec2T, Vec2TA>
        >
class TerrainModel : public DeviceMemoryAllocation<IndexT, Vec2T> {
    using BlockMesh = OptimizedIndexedGridMeshData<IndexT, Vec2T, Vec2TA, Vec2TO>;    
    using FixupMesh = FixupMeshData<IndexT, Vec2T, Vec2TA, Vec2TO>;
    using TransMesh = TransitionalZeroAreaMeshData<IndexT, Vec2T, Vec2TA, Vec2TO>;   
    using TrimsMesh = TrimsMeshData<IndexT, Vec2T, Vec2TA, Vec2TO>; 

 public:
    static const TrianglePrimitive BlockPrimitive = BlockMesh::Primitive;
    static const TrianglePrimitive FixupPrimitive = FixupMesh::Primitive;
    static const TrianglePrimitive TransPrimitive = TransMesh::Primitive;
    static const TrianglePrimitive TrimsPrimitive = TrimsMesh::Primitive;

    TerrainModel
            (CastChain<LessThanEq<uint32_t, 1024>, Pow2<uint32_t>, 
             uint32_t> ring_size, uint32_t post_transform_cache_size = 0) 
            : DeviceMemoryAllocation<IndexT, Vec2T>(
                    make_dev_allocator(ring_size - 1, post_transform_cache_size))
            , ring_size_(ring_size - 1)
            , m_(compute_m(ring_size- 1))
            , core_blocks_position_(make_core_blocks_position(ring_size - 1))
            , ring_blocks_position_(make_ring_blocks_position(ring_size - 1))
    {}

    int ring_size() const {
        return ring_size_; 
    }

    static constexpr TrianglePrimitive block_primitive() {
        return BlockMesh::Primitive;
    }

    static constexpr TrianglePrimitive fixup_primitive() {
        return FixupMesh::Primitive;
    }

    static constexpr TrianglePrimitive trans_primitive() {
        return TransMesh::Primitive;
    }

    static constexpr TrianglePrimitive trims_primitive() {
        return TrimsMesh::Primitive;
    }

    static constexpr size_t ring_blocks_count() {
        return 12;
    }
    
    static constexpr size_t core_blocks_count() {
        return 4;
    }

    static constexpr size_t ring_trims_count() {
        return 1;
    }

    static constexpr size_t core_trims_count() {
        return 1;
    }

    static constexpr size_t ring_fixups_count() {
        return 1;
    }

    static constexpr size_t core_fixups_count() {
        return 0;
    }

    static constexpr size_t ring_trans_count() {
        return 1;
    }

    static constexpr size_t core_trans_count() {
        return 0;
    }

    template<class Cache>
    static constexpr size_t max_blocks_instances() {
        return ring_blocks_count() * Cache::layers().size() + 
               core_blocks_count();
    }

    template<class Cache>
    static constexpr size_t max_trims_instances() {
        return ring_trims_count() * Cache::layers().size() +
               core_trims_count();
    }

    template<class Cache>
    static constexpr size_t max_fixups_instances() {
        return ring_fixups_count() * Cache::layers().size() +
               core_fixups_count();
    }

    template<class Cache>
    static constexpr size_t max_trans_instances(const Cache&) {
        return ring_trans_count() * Cache::layers().size() +
               core_trans_count();
    }

    template<class Cache, class OutputIt, class VisibilityFn>
    OutputIt block_instances(const Cache& cache, OutputIt d_first,
                             VisibilityFn visibility_fn) const {

        auto f = [&](size_t l, const Vec2T& p) {
            Vec2T offset = cache.lattice_offset(l) >> 1;
            Vec2T bmin = (Vec2TO::make(0, 0) + p + offset) << cache.log2_scale(l);
            Vec2T bmax = (Vec2TO::make(m_, m_) + p + offset) << cache.log2_scale(l);

            if (visibility_fn(glm::ivec3(bmin.x, cache.min_height(), bmin.y), 
                              glm::ivec3(bmax.x, cache.max_height(), bmin.y))) {
                *d_first = glm::ivec3(p.x, p.y, cache.log2_scale(l));
                ++d_first;
            }
        };

        for (auto&& p : core_blocks_position_) {
            f(Cache::layers().front(), p);
        }
        for (size_t l : Cache::layers()) {
            for (auto&& p : ring_blocks_position_) f(l, p);
        }
        
        return d_first;
    }

    template<class Cache, class OutputIt>
    OutputIt trims_instances(const Cache& cache, OutputIt d_first) const {
        *d_first = glm::ivec3(0, 0, cache.log2_scale(Cache::layers().front()));
        ++d_first;

        for (size_t l : Cache::layers()) {
            *d_first = glm::ivec3(0,0, cache.log2_scale(l));
            ++d_first;
        }
        
        return d_first;
    }

    template<class Cache, class OutputIt>
    OutputIt trims_vertices_index(const Cache& cache, OutputIt d_first) const {
        constexpr std::array<typename TrimsMesh::Index, 4> index = 
            {{ TrimsMesh::Index::TopRight,    TrimsMesh::Index::TopLeft,
               TrimsMesh::Index::BottomRight, TrimsMesh::Index::BottomLeft }};

        *d_first = index[3];
        ++d_first;

        for (size_t l : Cache::layers()) {
            Vec2T i = l == Cache::layers().front()
                ? Vec2TO::make(0,0)
                : (cache.lattice_offset(0) >> cache.log2_scale(l)) & 1;
            *d_first = index[static_cast<size_t>(i.x + (i.y << 1))];
            ++d_first;
        }

        return d_first;
    }

    template<class Cache, class OutputIt>
    OutputIt fixup_instances(const Cache& cache, OutputIt d_first) const {
        for (size_t l : Cache::layers()) {
            *d_first = glm::ivec3(0, 0,  cache.log2_scale(l));
            ++d_first;
        }
        
        return d_first;
    }

    template< class Cache, class OutputIt>
    OutputIt trans_instances(const Cache& cache, OutputIt d_first) const {
        for (size_t l : Cache::layers()) {
            *d_first = glm::ivec3(0, 0, cache.log2_scale(l));
            ++d_first;
        }

        return d_first;
    }

private:
    int ring_size_;
    int m_;
    std::array<Vec2T, core_blocks_count()> core_blocks_position_;
    std::array<Vec2T, ring_blocks_count()> ring_blocks_position_;

    static inline int compute_m(uint32_t ring_size) {
        return (ring_size + 1) / 4;
    }

    static inline 
    auto make_dev_allocator(uint32_t ring_size,
        size_t post_transform_cache_size) {
        uint32_t m = compute_m(ring_size);
         
        BlockMesh block_mesh_data(Vec2T(0,0), m, m, Vec2T(1,1), 
                                  0);
        FixupMesh fixup_mesh_data(m);
        TransMesh trans_mesh_data(m); 
        TrimsMesh trims_mesh_data(m); 

        return DeviceMemoryAllocation<IndexT, Vec2T>(
                    block_mesh_data.cvertices(), block_mesh_data.cindices(),
                    fixup_mesh_data.cvertices(), fixup_mesh_data.cindices(),
                    trans_mesh_data.cvertices(), trans_mesh_data.cindices(),
                    trims_mesh_data.cvertices(), trims_mesh_data.cindices());
    }

    static inline
    auto make_ring_blocks_position(uint32_t ring_size) 
    -> std::array<Vec2T, ring_blocks_count()> {
        int m = compute_m(ring_size);

        return {{
            Vec2TO::make(-(2*m - 1),          m),
            Vec2TO::make(-m,                  m),
            Vec2TO::make(1,                   m),
            Vec2TO::make(m,                   m),
            Vec2TO::make(-(2*m - 1),          1),
            Vec2TO::make(m,                   1),
            Vec2TO::make(-(2*m - 1),         -m),
            Vec2TO::make(m,                  -m),
            Vec2TO::make(-(2*m - 1), -(2*m - 1)),
            Vec2TO::make(-m,         -(2*m - 1)),
            Vec2TO::make(1,          -(2*m - 1)),
            Vec2TO::make(m,          -(2*m - 1)) 
        }};
    }

    static inline
    auto make_core_blocks_position(uint32_t ring_size)
    -> std::array<Vec2T, core_blocks_count()> { 
        uint32_t m = compute_m(ring_size);
        
        return {{ 
            Vec2TO::make(-(m-1), 0), 
            Vec2TO::make(0, 0), 
            Vec2TO::make(-(m-1), -(m-1)), 
            Vec2TO::make(0, -(m-1)) 
        }};
    }
 
};

}
#endif
