#ifndef GCT_HISTOGRAM_HPP
#define GCT_HISTOGRAM_HPP

#include <type_traits>
#include <iostream>
#include <vector>
#include <cmath>


template<class T, class S = uint32_t>
class Histogram {
    static_assert(std::is_integral<S>::value, "");
    
    T rmin_;
    T rmax_;

    T min_;
    T max_;
    T total_;

    T count_;

    std::vector<S> bins_;
public:

    using size_type = typename std::vector<S>::size_type;

    Histogram() {}

    Histogram(T min, T max, size_type bins_count) :
        rmin_(min), 
        rmax_(max),
        min_(std::numeric_limits<T>::max()),
        max_(std::numeric_limits<T>::min()),
        total_(T(0)),
        count_(T(0)),
        bins_(std::vector<S>(bins_count + 2, 0)) 
    {}

    void insert(T e) {
        ++count_;
        total_ += e; 
        min_ = std::min(min_, e);
        max_ = std::max(max_, e);

        const T r = T(bins_.size() - 2) / (rmax_ - rmin_);
        const size_type b = std::floor((e - rmin_)*r); 
        ++bins_[std::max<size_type>(std::min(bins_.size() - 1, b), 0)]; 
    }
    
    void print() {
        S m = 0;
        for (size_type i = 0; i < bins_.size(); ++i) m = std::max(m, bins_[i]);

        T step = (rmax_ - rmin_) / T(bins_.size() - 2);

        T c = rmin_ - step;
        for (auto&& b : bins_) {
            std::cout << std::fixed <<  c << " :: ";
            c += step;
            std::cout << "[";
            S bb = (b*50 / m) + S(b > 0);
            for (S i = 0; i < bb; ++i) std::cout << "#";
            std::cout << "]" << std::endl;
        }

        std::cout << "total = " << total_ << "\n";
        std::cout << "min = " << min_ << "\n"; 
        std::cout << "avg = " << total_/count_ << "\n"; 
        std::cout << "max = " << max_ << std::endl; 

    }
};


#endif
