#ifndef OPENGL_TERRAIN_MODEL
#define OPENGL_TERRAIN_MODEL

#include <TerrainModel.hpp>
#include <OpenGLDeviceMemoryAllocator.hpp>

namespace gct {

using OpenGLTerrainModel = 
        TerrainModel<OpenGLDeviceMemoryAllocator, uint16_t, glm::ivec2>; 
}

#endif
