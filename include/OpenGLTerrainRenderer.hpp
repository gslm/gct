#ifndef OPENGL_RENDERER_HPP
#define OPENGL_RENDERER_HPP

#include <type_traits>
//#include <iostream>
//#include <memory>
//#include <algorithm>

#include <stgl/OpenGLTexture.hpp>
#include <stgl/OpenGLFramebuffer.hpp>
#include <stgl/OpenGLProgram.hpp>
#include <stgl/OpenGLDebug.hpp>

#include <TerrainDataCache.hpp>
#include <OpenGLFrustum.hpp>
#include <OpenGLTerrainSource.glsl.hpp>
#include <OpenGLTerrainModel.hpp>
#include <RenderOutput.hpp>

#include <glm/gtc/matrix_transform.hpp>

namespace gct {

namespace OpenGLTerrainRenderer_detail {

    template<RenderOutput Output>
    struct RenderOutputAttachment;

    template<>
    struct RenderOutputAttachment<RenderOutput::none> {
        static const GLenum value = GL_NONE;
    };

    template<>
    struct RenderOutputAttachment<RenderOutput::terrain_data> {
        static const GLenum value = GL_COLOR_ATTACHMENT0;
    };

    template<>
    struct RenderOutputAttachment<RenderOutput::texture_coords> {
        static const GLenum value = GL_COLOR_ATTACHMENT1;
    };
 
    template<>
    struct RenderOutputAttachment<RenderOutput::texture_coords_dF> {
        static const GLenum value = GL_COLOR_ATTACHMENT2;
    };

    template<>
    struct RenderOutputAttachment<RenderOutput::depth> {
        static const GLenum value = GL_DEPTH_ATTACHMENT;
    };

    template<>
    struct RenderOutputAttachment<RenderOutput::stencil> {
        static const GLenum value = GL_STENCIL_ATTACHMENT;
    };

    template<>
    struct RenderOutputAttachment<RenderOutput::depth_stencil> {
        static const GLenum value = GL_DEPTH_STENCIL_ATTACHMENT;
    };


    template<class T>
    struct texture_format;

    template<GLenum TFormat, GLenum TTarget>
    struct texture_format<stgl::OpenGLTexture<TFormat, TTarget>> {
        static const GLenum value = TFormat;
    };
    
    template<TrianglePrimitive P>
    struct PrimitiveGLenum;

    template<>
    struct PrimitiveGLenum<TrianglePrimitive::Triangle> {
        static const GLenum value = GL_TRIANGLES;
    };
    
    template<>
    struct PrimitiveGLenum<TrianglePrimitive::Strip> {
        static const GLenum value = GL_TRIANGLE_STRIP;
    };
    
    template<>
    struct PrimitiveGLenum<TrianglePrimitive::Fan> {
        static const GLenum value = GL_TRIANGLE_FAN;
    };
/*
    template<class T, GLenum A>
    struct is_texture_attachable : std::true_type {};

    template<class T>
    struct is_texture_attachable<T, GL_DEPTH_ATTACHMENT> 
        : std::integral_constant<bool, stgl::is_depth_texture_format<
                                                texture_format<T>::value
                                >> {};
    template<class T>
    struct is_texture_attachable<T, GL_STENCIL_ATTACHEMNT> 
        : std::integral_constant<bool, stgl::is_stencil_texture_format<
                                                texture_format<T>::value>
                                >> {};
*/

    template<RenderOutput, RenderOutput...>
    struct framebuffer_setup_impl2 {
        template<class... TList>
        static inline void setup(stgl::OpenGLFramebuffer& framebuffer, 
                                 const TList&... tex_list) {}       
    };

    template<RenderOutput T, RenderOutput O, RenderOutput... OutputList>
    struct framebuffer_setup_impl3 {
        template<class THead, class... TTail>
        static inline void setup(stgl::OpenGLFramebuffer& framebuffer, 
                                 const THead& tex, const TTail&... tex_tail) {
            //static_assert(is_texture_attachable<THead, RenderOutputAttachment<T>::value>::value, "");

            framebuffer.AttachTexture<RenderOutputAttachment<T>::value>(tex);
            using fs = framebuffer_setup_impl2<O, OutputList...>;
            fs::setup(framebuffer, tex_tail...); 
        } 
    };

    template<RenderOutput O, RenderOutput... OutputList>
    struct framebuffer_setup_impl3<RenderOutput::none, O, OutputList...> {
        template<class... TList>
        static inline void setup(stgl::OpenGLFramebuffer& framebuffer, 
                                 const TList&... tex_list) {
            using fs = framebuffer_setup_impl2<O, OutputList...>;
            fs::setup(framebuffer, tex_list...);
        }
    };

    template<RenderOutput O, RenderOutput Head, RenderOutput... Tail>
    struct framebuffer_setup_impl2<O, Head, Tail...> {
        template<class... TList>
        static inline void setup(stgl::OpenGLFramebuffer& framebuffer, 
                                const TList&... tex_list) {
            using fs = framebuffer_setup_impl3<O & Head, O, Tail...>;
            fs::setup(framebuffer, tex_list...);
        }
    };

    template<RenderOutput O, class OutputElems>
    struct framebuffer_setup_impl;

    template<RenderOutput O, RenderOutput... L>
    struct framebuffer_setup_impl<O, RenderOutputList<L...>> {
        template<class... TList>
        static inline void setup(stgl::OpenGLFramebuffer& framebuffer, 
                                 const TList&... tex_list) {
            using fs = framebuffer_setup_impl2<O, L...>;
            fs::setup(framebuffer, tex_list...);
        }
    };

    template<RenderOutput DSOutput>
    struct framebuffer_setup 
        : framebuffer_setup_impl<DSOutput, RenderOutputElems> {}; 

    template<RenderOutput>
    struct OpenGLStateMod;

    template<RenderOutput O, RenderOutput... OutputElems>
    struct OpenGLState_impl3 {
        static_assert(sizeof...(OutputElems) == 0, "");
    
        static inline void apply() {}
        static inline void restore() {}
    };

    template<RenderOutput O, RenderOutput Head, RenderOutput... Tail>
    struct OpenGLState_impl3<O, Head, Tail...> {
        static inline void apply() { 
            OpenGLStateMod<O & Head>::apply(); 
            OpenGLState_impl3<O, Tail...>::apply(); 
        }

        static inline void restore() {
            OpenGLStateMod<O & Head>::restore();
            OpenGLState_impl3<O, Tail...>::restore();
        }
    };

    template<RenderOutput O, class OutputElems>
    struct OpenGLState_impl2;

    template<RenderOutput O, RenderOutput... L>
    struct OpenGLState_impl2<O, RenderOutputList<L...>> 
        : OpenGLState_impl3<O, L...> {}; 

    template<RenderOutput O>
    struct OpenGLState_impl {
        static inline void apply() {
            OpenGLState_impl2<O, RenderOutputElems>::apply();
        }

        static inline void restore() {
            OpenGLState_impl2<O, RenderOutputElems>::restore();
        }
    };

    struct OpenGLStateMod_shared {
        static inline void apply() {}
        static inline void restore() {}
    };

    template<RenderOutput>
    struct OpenGLStateMod {
        static inline void apply() {}
        static inline void restore() {}
    };

    template<>
    struct OpenGLStateMod<RenderOutput::stencil> {
        static inline void apply() {
            glEnable(GL_STENCIL_TEST);
            glStencilFunc(GL_ALWAYS, 1, 1);
            glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        }

        static inline void restore() {
            glDisable(GL_STENCIL_TEST);
            // There is no need to change neither StencilFunc nor StencilOp
            // since the default values are used.
        }
    };

    template<>
    struct OpenGLStateMod<RenderOutput::depth>  {
        static inline void apply() {
            glEnable(GL_DEPTH_TEST);
        }

        static inline void restore() {
            glDisable(GL_DEPTH_TEST);
        }
    };
    
    template<RenderOutput O>
    struct OpenGLState {
        static inline void apply() {
            OpenGLStateMod_shared::apply();
            OpenGLState_impl<O>::apply();
        }

        static inline void restore() {
            OpenGLStateMod_shared::restore();
            OpenGLState_impl<O>::restore();
        }
    }; 

    template<RenderOutput O>
    using is_valid_depth_stencil = 
        typename std::integral_constant<bool, 
            not(is_output_subset_v<O, RenderOutput::depth_stencil> and 
                (is_output_subset_v<O, RenderOutput::depth> or
                 is_output_subset_v<O, RenderOutput::stencil>))>;

    template<RenderOutput O>
    constexpr bool is_valid_depth_stencil_v = is_valid_depth_stencil<O>::value;

    template<RenderOutput O, class = void>
    struct separate_depth_stencil {
        static constexpr RenderOutput value = O;
    };

    template<RenderOutput O>
    struct separate_depth_stencil<O, 
        std::enable_if_t<is_output_subset_v<O, RenderOutput::depth_stencil>>> {
        static constexpr RenderOutput value = 
            (O & ~RenderOutput::depth_stencil) | 
                (RenderOutput::depth | RenderOutput::stencil);
    };

    template<RenderOutput O>
    constexpr RenderOutput separate_depth_stencil_v = 
        separate_depth_stencil<O>::value;

    template<RenderOutput O>
    struct has_color_buffer 
        : std::integral_constant<bool,
            is_output_subset_v<O, RenderOutput::terrain_data>   or
            is_output_subset_v<O, RenderOutput::texture_coords> or 
            is_output_subset_v<O, RenderOutput::texture_coords_dF> > {};

    template<RenderOutput O>
    constexpr bool has_color_buffer_v = has_color_buffer<O>::value;

    template<RenderOutput O>
    struct has_depth 
        : std::integral_constant<bool,
            is_output_subset_v<O, RenderOutput::depth> or 
            is_output_subset_v<O, RenderOutput::depth_stencil>> {};

    template<RenderOutput O>
    constexpr bool has_depth_v = has_depth<O>::value;



}

template<RenderOutput Output>
class OpenGLTerrainRenderer {
 
    static constexpr RenderOutput DSOutput = 
        OpenGLTerrainRenderer_detail::separate_depth_stencil_v<Output>;
    
    template<RenderOutput O>
    static constexpr bool has_color_v = 
        OpenGLTerrainRenderer_detail::has_color_buffer_v<O>;

    template<RenderOutput O>
    static constexpr bool has_depth_v = 
        OpenGLTerrainRenderer_detail::has_depth_v<O>;  
   
    template<RenderOutput O>
    static constexpr bool is_valid_depth_stencil_v =
        OpenGLTerrainRenderer_detail::is_valid_depth_stencil_v<Output>;
 
    static_assert(is_valid_depth_stencil_v<Output>, 
        "Depth or Stencil and Depth_Stencil are not allowed."); 
    static_assert(not has_color_v<Output> or has_depth_v<Output>, 
        "Depth Buffer must be provided with Color Outputs.");
  
public:
    class Framebuffer;
   
    OpenGLTerrainRenderer() {}
/*
    void set_camera(const glm::mat4& projection, const glm::quat& orientation) {
        glm::mat4 mvp = projection *   
        //glm::mat4 debug_p = glm::perspective(45.0f, 0.5f, 1.0f, 10000.0f);
        frustum_.Update(mvp_matrix);
        program_.set_mvp_matrix(mvp_matrix);
    }*/
 
    void set_mvp_matrix(const glm::mat4& mvp_matrix) {
        frustum_.Update(mvp_matrix);
        program_.set_mvp_matrix(mvp_matrix);
    }

    template<RenderOutput O = DSOutput>
    std::enable_if_t<(O & RenderOutput::texture_coords) != RenderOutput::none,
    void> set_wang_tiles(int32_t log2_Kh, int32_t log2_Kv, 
                         int32_t log2_tile_size) {

        program_.set_wang_tiles(glm::ivec2(log2_Kh, log2_Kv), log2_tile_size);
    }   

    template<class Cache>
    void DrawBlocks(const Cache& cache, const OpenGLTerrainModel& model) {
        using Model = OpenGLTerrainModel; 
        constexpr size_t n = Model::max_blocks_instances<Cache>();  
        std::array<glm::ivec3, n> instances;

        auto blocks_begin = instances.begin();
        auto blocks_end = model.block_instances(cache, blocks_begin, 
            [&](const glm::ivec3& bmin, const glm::ivec3& bmax) -> bool {
                return true;
                //return frustum_.AABBoxInFrustum(bmin, bmax);                  
            });

        using namespace OpenGLTerrainRenderer_detail;
        constexpr TrianglePrimitive p = OpenGLTerrainModel::BlockPrimitive;
        constexpr GLenum primitive = PrimitiveGLenum<p>::value;        

        program_.template DrawInstances<primitive>(blocks_begin, blocks_end, 
                    model.block_vertices_dev_ptr(),
                    model.block_indices_dev_ptr());
    }

    template<class Cache>
    void DrawTrims(const Cache& cache, const OpenGLTerrainModel& model) {
        using Model = OpenGLTerrainModel; 
        
        using namespace OpenGLTerrainRenderer_detail;
        constexpr auto primitive = 
            PrimitiveGLenum<Model::TrimsPrimitive>::value;
       
        constexpr size_t n = Model::max_trims_instances<Cache>(); 
        std::array<glm::ivec3, n> instances;
        model.trims_instances(cache, instances.begin());
        
        std::array<size_t, n> vertices_indices;
        model.trims_vertices_index(cache, vertices_indices.begin());

        auto instances_it = instances.begin(); 
        for (size_t i : vertices_indices) {
            program_.template DrawInstances<primitive>(instances_it, instances_it+1,
                                     model.trims_vertices_dev_ptr(i),
                                     model.trims_indices_dev_ptr());
            ++instances_it; 
        }
    }

    template<class Cache>
    void DrawFixups(const Cache& cache, const OpenGLTerrainModel& model) {
        using namespace OpenGLTerrainRenderer_detail;
        using Model = OpenGLTerrainModel; 
        constexpr auto primitive = 
            PrimitiveGLenum<Model::FixupPrimitive>::value; 

        constexpr size_t n = Model::max_fixups_instances<Cache>();
        std::array<glm::ivec3, n> instances;

        auto instances_end = model.fixup_instances(cache, instances.begin());
        program_.template DrawInstances<primitive>(instances.begin(), instances_end,
                                 model.fixup_vertices_dev_ptr(),
                                 model.fixup_indices_dev_ptr());
    }

    template<class Cache>
    void DrawTrans(const Cache& cache, const OpenGLTerrainModel& model) {
        using namespace OpenGLTerrainRenderer_detail;
        using Model = OpenGLTerrainModel; 
        constexpr auto primitive = 
            PrimitiveGLenum<Model::TransPrimitive>::value;

        constexpr size_t n = Model::max_trans_instances<Cache>();
        std::array<glm::ivec3, n> instances;

        auto instances_end = model.trans_instances(cache, instances.begin());
        program_.template DrawInstances<primitive>(instances.begin(), instances_end,
                                 model.trans_vertices_dev_ptr(),
                                 model.trans_indices_dev_ptr());
    } 

/*
    using DefaultOpenGLState = 
        OpenGLTerrainRenderer_detail::OpenGLState<DSOutpt>;
    using DeaultOpenGLClear = 
        OpenGLTerrainRenderer_detail::OpenGLClear<DSOutput>;
*/

    template<
        template<size_t> class F, class TerrainCache 
        //,class OpenGLClear = DefaultOpenGLClear 
        //,class OpenGLState = DefaultOpenGLState
        >
    void Render(//const glm::mat4& camera_projection,
                //const glm::quat& camera_orientation,
                const OpenGLTerrainModel& model, 
                const TerrainDataCacheView<F, TerrainCache>& cache,
                const Framebuffer& framebuffer) {
/* 
        TerrainCache::vec3_type vo = cache.view_offset();
        const glm::vec3 view_offset = glm::vec3(vo.x, vo.y, vo.z);

        const glm::mat4 o = glm::mat4_cast(glm::conjugate(camera_orientation));
        const glm::mat4 mvp_matrix = camera_projection * 
                                     glm::translate(o, -view_offset);
        frustum_.Update(mvp_matrix);
*/
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.framebuffer_.id());

        program_.Enable();

        //program_.set_mvp_matrix(mvp_matrix);
        program_.set_cache_data(cache.data());
        program_.set_cache_offset(cache.lattice_offset(0));
        program_.set_transition(/*view_offset.xy()*/ glm::vec2(0.0), 
                                model.ring_size(), 
                                model.ring_size()/10);

       
        glBindBuffer(GL_ARRAY_BUFFER, model.vertices_buffer().id());
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.indices_buffer().id());

        DrawBlocks(cache, model);
        DrawFixups(cache, model);
        DrawTrims(cache, model);
       // DrawTrans(cache, model);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        program_.Disable();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    bool good() { return program_.good(); }  

private:
    
    using Program = stgl::OpenGLProgram<OpenGLTerrainSource<DSOutput>>;
    Program program_;
    OpenGLFrustum frustum_;
};

template<RenderOutput Output>
class OpenGLTerrainRenderer<Output>::Framebuffer {
    stgl::OpenGLFramebuffer framebuffer_;
    bool good_;

public:
    friend class OpenGLTerrainRenderer<Output>;

    template<GLenum... TextureTypes>
    Framebuffer(const stgl::OpenGLTexture<TextureTypes, 
                                           GL_TEXTURE_2D>&... textures) {
        using namespace OpenGLTerrainRenderer_detail;

        framebuffer_setup<Output>::setup(framebuffer_, textures...);

        constexpr GLuint out_texture_coords = 
            Program::template get_out_index<RenderOutput::texture_coords>();
        constexpr GLuint out_terrain_data = 
            Program::template get_out_index<RenderOutput::terrain_data>();
        constexpr GLuint out_texture_coords_dF = 
            Program::template get_out_index<RenderOutput::texture_coords_dF>();

        constexpr size_t n = static_cast<size_t>(std::max(std::max(out_texture_coords, 
                                                                   out_texture_coords_dF), 
                                                          out_terrain_data) + 1);
        std::array<GLenum, n> dbuffs;
        dbuffs.fill(GL_NONE);

        dbuffs[out_terrain_data] = 
            RenderOutputAttachment<Output & RenderOutput::terrain_data>::value;
        dbuffs[out_texture_coords] = 
            RenderOutputAttachment<Output & RenderOutput::texture_coords>::value;
        dbuffs[out_texture_coords_dF] = 
            RenderOutputAttachment<Output & RenderOutput::texture_coords_dF>::value;

        framebuffer_.DrawBuffers(dbuffs);

        good_ = framebuffer_.CheckStatus<GL_FRAMEBUFFER>() == GL_FRAMEBUFFER_COMPLETE;
    }

    bool good() const { return good_; }

    template<RenderOutput O, class T>
    std::enable_if_t<(   O != RenderOutput::depth 
                      or O != RenderOutput::stencil 
                      or O != RenderOutput::depth_stencil) 
                      , 
    void> Clear(std::array<T, 4>&& value) {
        static_assert(output_count_v<O> == 1, ""); 
        static_assert(is_output_subset_v<Output, O>, "");
        framebuffer_.Clear<GL_COLOR, T>(Program::template get_out_index<O>(),  
                                        std::forward<std::array<T, 4>>(value));
    }

    template<RenderOutput O>
    std::enable_if_t<O == RenderOutput::depth,
    void> Clear(float value) {
        static_assert(is_output_subset_v<DSOutput, RenderOutput::depth>, "");
        framebuffer_.Clear<GL_DEPTH>(value);
    }

    template<RenderOutput O>
    std::enable_if_t<O == RenderOutput::stencil,
    void> Clear(int value) {
        static_assert(is_output_subset_v<DSOutput, RenderOutput::stencil>, "");
        framebuffer_.Clear<GL_STENCIL>(value); 
    }

    template<RenderOutput O>
    std::enable_if_t<O == RenderOutput::depth_stencil,
    void> Clear(float depth_value, int stencil_value) {
        static_assert(is_output_subset_v<DSOutput, RenderOutput::stencil>, "");
        static_assert(is_output_subset_v<DSOutput, RenderOutput::depth>, "");
        framebuffer_.Clear<GL_DEPTH_STENCIL>(depth_value, stencil_value); 
    } 
};

}
#endif
