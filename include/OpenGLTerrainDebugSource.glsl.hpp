#ifndef OPENGL_TERRAIN_DEBUG_SOURCE_GLSL_HPP
#define OPENGL_TERRAIN_DEBUG_SOURCE_GLSL_HPP

class OpenGLTerrainDebugSource {
    using Program = stgl::OpenGLProgram<OpenGLTerrainDebugSource>;
public:    
    STGL_OPENGL_SHADER(GL_VERTEX_SHADER) {
    return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450 core

layout(early_fragment_tests) in;
layout(location = 0) in vec2 position;

void main() {
    gl_Position = vec4(vec2(position), 0.0, 1.0);
}
)GLSLSOURCE" ); 
//END_GLSLSOURCE
    }

    STGL_OPENGL_SHADER(GL_FRAGMENT_SHADER) {
        return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450

layout(early_fragment_tests) in;
layout(location = 0, binding = 0) uniform sampler2D texture_coords_buffer;
layout(location = 1, binding = 1) uniform sampler2D texture_coords_dF_buffer;
layout(location = 2, binding = 2) uniform sampler2D terrain_data_buffer;

layout(location = 0) out vec3 color;

void main() {
    ivec2 texel_coords = ivec2(gl_FragCoord.xy);
    vec4 terrain_data = texelFetch(terrain_data_buffer, texel_coords, 0);
    vec3 tc = texelFetch(texture_coords_buffer, texel_coords, 0).rgb;
    vec4 tc_dF = texelFetch(texture_coords_dF_buffer, texel_coords, 0).rgba;
    color = mix(abs(terrain_data.xyz), tc, 0.5);
}
)GLSLSOURCE"); 
//END_GLSLSOURCE
    
    }

    template<GLenum TextureFormat>
    void set_texture_coords_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& texture_coords_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[0] = texture_coords_buffer.id();        
    }

    template<GLenum TextureFormat>
    void set_texture_coords_dF_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& texture_coords_dF_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[1] = texture_coords_dF_buffer.id();        
    } 

    template<GLenum TextureFormat>
    void set_terrain_data_buffer(const stgl::OpenGLTexture<TextureFormat, 
                                              GL_TEXTURE_2D>& terrain_data_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[2] = terrain_data_buffer.id(); 
    }

    template<GLenum Primitive>
    void Draw(const stgl::OpenGLBuffer<uint8_t>& indices, 
              const stgl::OpenGLBuffer<glm::vec2>& vertices) {
        static_assert(stgl::is_primitive<Primitive>::value, "");
        glUseProgram(Program::get_id(this));

        for (int i = 0; i < textures_.size(); ++i) {
            glBindTextureUnit(i, textures_[i]);
        }
        
        glBindBuffer(GL_ARRAY_BUFFER, vertices.id());
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 
                              reinterpret_cast<GLvoid*>(0));
 
        //Draw Call
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.id());
        glDrawElements(Primitive, indices.size(), GL_UNSIGNED_BYTE, 
                       reinterpret_cast<void*>(0));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
        glDisableVertexAttribArray(0);

        glUseProgram(0);
    }

private:
    std::array<GLuint, 3> textures_ = {{0,0,0}};
};

#endif 
