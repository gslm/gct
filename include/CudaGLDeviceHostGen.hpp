#ifndef CUDA_GL_DEVICE_GENERATE_HPP
#define CUDA_GL_DEVICE_GENERATE_HPP

#include <glm/glm.hpp>

#include <TerrainTransform.hpp>
#include <CudaTerrainGen.hpp>
#include <CudaGLSurface.hpp>

#include <Pow2.hpp>

namespace gct {

template<size_t N>
class CudaGLDeviceHostGen {

    using vec2_t = typename TerrainTransform<N>::vec2_t;
    using ivec2_t = typename TerrainTransform<N>::ivec2_t;
    using vec4_t = glm::vec4;

private:
   
    template<class Fn> 
    static inline void GenerateRange(int from, int to, Fn g) {
        int s = glm::sign(to - from); 
        for (; from != to; from += s) g(from);
    }

protected:
    using container_t = CudaGLSurface<GL_RGBA32F, GL_TEXTURE_2D_ARRAY>; 
    CudaTerrainGen gen;

    container_t make_container(Pow2<uint32_t> width, 
                               Pow2<uint32_t> height, 
                               Pow2<uint32_t> layers) {
        container_t surface = CudaGLSurface<GL_RGBA32F, GL_TEXTURE_2D_ARRAY>
                                (width, height, layers);
        surface.SetParameter<GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE>();
        surface.SetParameter<GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE>();
        surface.SetParameter<GL_TEXTURE_MAG_FILTER, GL_NEAREST>();            
        surface.SetParameter<GL_TEXTURE_MIN_FILTER, GL_NEAREST>(); 

        return surface;
    }

    void Init(int x, int y, uint32_t lod, 
              TerrainTransform<N>& transform, 
              container_t& container) {

        transform.set_position(ivec2_t(x, y));
        transform.set_base_layer(lod);

        GenerateRange(lod, lod + transform.layer_count(), 
            [&](int layer) {
                const ivec2_t p = transform.lattice_offset(layer) - 
                                  ivec2_t(container.width(), 
                                          container.height()); 
                gen.DeviceGenerate(p.x, p.y, 
                                   container.width(), container.height(), 
                                   layer, container);
            }
        );
    }

    void Lod(unsigned lod, 
             TerrainTransform<N>& transform, container_t& container) {
        unsigned prev_lod = transform.base_layer();
        transform.set_base_layer(lod);

        int delta_lod = lod - prev_lod;        

        auto positive = [](int x) { return x > 0; };       
        
        int from = prev_lod + positive(delta_lod) * transform.layer_count(); 
        int to = from + delta_lod - 1;
 
        GenerateRange(from, to, 
            [&](int layer) {
                const ivec2_t p = transform.lattice_offset(layer) - 
                                  ivec2_t(container.width(), 
                                          container.height()); 
                gen.DeviceGenerate(p.x, p.y, 
                                   container.width(), container.height(), 
                                   layer, container);
            }
        );
    }

    void Translate(float delta_x, float delta_y, 
                   TerrainTransform<N>& transform, container_t& container) {

        TerrainTransform<N> prev_transform = transform;
        transform.Translate(vec2_t(delta_x, delta_y));
       
        GenerateRange(transform.base_layer(),
                      transform.base_layer() + transform.layer_count(),
            [&](int layer) {
                auto delta_lattice_offset = transform.lattice_offset(layer) - 
                                            prev_transform.lattice_offset(layer); 
                auto prev_lattice_offset = prev_transform.lattice_offset(layer);
                gen.DeviceGenerate(prev_lattice_offset.x, prev_lattice_offset.y, 
                                   delta_lattice_offset.x, delta_lattice_offset.y,
                                   layer, 
                                   container);
            }
        ); 

    }

    vec4_t GetDataAt(const vec2_t& p, const TerrainTransform<N>&, 
                     const container_t&) const {
        vec4_t data;
        gen.HostCompute(p.x, p.y, data.r, data.g, data.b, data.a);
        return data;
    }

    float max_height() const {
        return gen.max_height();
    }

    float min_height() const {
        return gen.min_height();
    } 

public:
    void SetGenParams(float amplitude, float frequency, float lacunarity, 
                      float H, float offset, float gain, int ocatves, int seed) {
        gen.SetGenParams(amplitude, frequency, 
                         lacunarity, H, offset, gain, 
                         ocatves, seed);
    }

};

}

#endif

