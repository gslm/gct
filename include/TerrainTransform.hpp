#ifndef TERRAIN_TRANSFORM_HPP
#define TERRAIN_TRANSFORM_HPP

#include <glm/glm.hpp>
#include <OrderedIndexingSet.hpp>

namespace gct {

template<size_t>
class TerrainTransform;

template<class Set, template<size_t> class Filter>
class TerrainTransformView {
    friend class TerrainTransform<Set::size()>;
    
    using LayerSet = OrderedIndexingSubset<Set, Filter>;

    const TerrainTransform<Set::size()>& transform_;

private:
    TerrainTransformView(const TerrainTransform<Set::size()>& transform) 
        : transform_(transform) {}

public:
    decltype(auto) base_layer() const {
        return transform_.base_layer();
    }

    decltype(auto) get_layer(size_t layer) const {
        return transform_.get_layer(layer);
    }

    decltype(auto) lattice_offset(size_t layer) const {
        return transform_.lattice_offset(layer);
    }

    decltype(auto) position() const {
        return transform_.position();
    }

    decltype(auto) scale(size_t layer) const {
        return transform_.scale(layer);
    }

    decltype(auto) log2_scale(size_t layer) const {
        return transform_.log2_scale(layer);
    }

    static constexpr decltype(auto) layers() {
        return LayerSet::array(); 
    }

};

template<size_t LayerCount>
class TerrainTransform {
public:

    using vec2_t = glm::vec2;
    using ivec2_t = glm::ivec2;

private:
    using LayerSet = OrderedIndexingSet<LayerCount>;

    ivec2_t iposition_;
    vec2_t fposition_;

    vec2_t position_;

    size_t base_layer_;

public:
    TerrainTransform(size_t base_layer = 0)
        : base_layer_(base_layer) {}

    template<template<size_t> class SubsetFilter>
    auto make_view() const {
        return TerrainTransformView<LayerSet, SubsetFilter>(*this);
    }

    void Translate(const vec2_t& delta_t) {
        position_ += delta_t;
        iposition_ += glm::ivec2(glm::floor(delta_t));
        fposition_ += glm::fract(delta_t);
        iposition_ += glm::ivec2(glm::floor(fposition_));
        fposition_ = glm::fract(fposition_);
    }
    
    void set_base_layer(size_t base_layer) {
        base_layer_ = base_layer;
    }

    int base_layer() const {
        return static_cast<int>(base_layer_);
    }

    constexpr int layer_count() const {
        return LayerCount;
    }   
 
    const ivec2_t& iposition() const {
        return iposition_;
    }

    const vec2_t& fposition() const {
        return fposition_;
    }
/*
    const vec2_t& p(size_t l) const {
        return iposition + cache.log2_scale(l);
    }
*/
    void set_position(const ivec2_t& ip, const vec2_t fp = vec2_t(0.0, 0.0) ) {
        position_ = ip;
        iposition_ = ip;
        fposition_ = fp;
    }

    int get_layer(size_t layer) const {
        return static_cast<int>(layer + base_layer_);
    }

    ivec2_t lattice_offset(size_t layer) const {
        return ivec2_t(position_) >> log2_scale(layer);
    }

    template<class T>
    T scale(size_t layer) const {
        return static_cast<T>(1 << (layer + base_layer_));  
    } 

    int log2_scale(size_t layer) const {
        return static_cast<int>(layer + base_layer_);
    }

    static constexpr decltype(auto) layers() {
        return LayerSet::array();
    }

};

}
#endif
