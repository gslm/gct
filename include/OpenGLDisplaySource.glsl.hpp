#ifndef OPENGL_DISPLAY_SOURCE_GLSL_HPP
#define OPENGL_DISPLAY_SOURCE_GLSL_HPP

class OpenGLDisplaySource {
    using Program = stgl::OpenGLProgram<OpenGLDisplaySource>;

public:    
    STGL_OPENGL_SHADER(GL_VERTEX_SHADER) {
    return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450 core

//layout(early_fragment_tests) in;
layout(location = 0) in vec2 position;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
}
)GLSLSOURCE" ); 
//END_GLSLSOURCE
    }

    STGL_OPENGL_SHADER(GL_FRAGMENT_SHADER) {
        return std::string( 
//BEGIN_GLSLSOURCE 
R"GLSLSOURCE(
#version 450

//layout(early_fragment_tests) in;

layout(location = 0, binding = 0) uniform sampler2D output_buffer;

layout(location = 0) out vec3 color;

void main() {
    ivec2 texel_coords = ivec2(gl_FragCoord.xy);

    color = texelFetch(output_buffer, texel_coords, 0).rgb;
}

)GLSLSOURCE"); 
//END_GLSLSOURCE
    
    }
    template<GLenum TextureFormat>
    void set_output_buffer(stgl::OpenGLTexture<TextureFormat, 
                                               GL_TEXTURE_2D>& output_buffer) {
        static_assert(stgl::is_sampler_assignable<TextureFormat>::value, "");
        textures_[0] = output_buffer.id();        
    }

    void set_cam_tranform(const glm::mat4& m) {
        glProgramUniformMatrix4fv(Program::get_id(this), 1, 1, GL_FALSE, 
                                  glm::value_ptr(m));
    }

    template<GLenum Primitive>
    void Draw(const stgl::OpenGLBuffer<uint8_t>& indices, 
              const stgl::OpenGLBuffer<glm::vec2>& vertices) {
        static_assert(stgl::is_primitive<Primitive>::value, "");
        glUseProgram(Program::get_id(this));

        for (int i = 0; i < textures_.size(); ++i) {
            glBindTextureUnit(i, textures_[i]);
        }
        
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertices.id());
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 
                              reinterpret_cast<GLvoid*>(0));
 
        //Draw Call
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices.id());
        glDrawElements(Primitive, indices.size(), GL_UNSIGNED_BYTE, 
                       reinterpret_cast<void*>(0));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
        glDisableVertexAttribArray(0);

        glUseProgram(0);
    }
private:
    std::array<GLuint, 1> textures_ = {{0}};
};


#endif
