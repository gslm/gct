#ifndef GCT_GLSL_TERRAIN_SOURCE_GLSL_HPP
#define GCT_GLSL_TERRAIN_SOURCE_GLSL_HPP

namespace gct {

namespace GLSLTerrainSource_detail {    
    template<RenderOutput Output>
    struct RenderFlags {
        static constexpr const char* const flag = "";
    };

    template<>
    struct RenderFlags<RenderOutput::texture_coords> {
        static constexpr const char* const flag = 
            "#define TEXTURE_COORDS"; 
    };
 
    template<>
    struct RenderFlags<RenderOutput::texture_coords_dF> {
        static constexpr const char* const flag = 
            "#define TEXTURE_COORDS_DF"; 
    };
   
    template<>
    struct RenderFlags<RenderOutput::terrain_data> {
        static constexpr const char* const flag = 
            "#define TERRAIN_DATA"; 
    };
}

class GLSLTerrainSource {
    static constexpr const char* vertex_str = 
//BEGIN_GLSLSOURCE
R"GLSLSOURCE(
// Per vertex data.
layout(location = 0) in ivec2 position;

// Per instance uniform data.
layout(location = 0) uniform ivec3 footprint;

// Uniform data among instances.
layout(location = 1) uniform vec4 transition; 
// xy = view_offets; z = alpha_offset; w = one_over_width
layout(location = 2) uniform sampler2DArray cache_data;
layout(location = 3) uniform ivec2 cache_offset;
layout(location = 4) uniform mat4 mvp_matrix;

// Data sent to fragment shader.
#if defined(TERRAIN_DATA)
layout (location = 0) out vec4 frag_terrain_data;
layout (location = 1) out float alpha;
#endif

#if defined(TEXTURE_COORDS)
//layout (location = 2) out float height;
layout (location = 3) out flat ivec2 tile_id;
layout (location = 4) out vec2 tile_offset;
#endif

void main() {

    ivec2 ring_position = position + footprint.xy + 1;
    vec2 f_ring_position = vec2(ring_position);

    ivec2 c0 = ring_position + ((cache_offset >> (footprint.z+1)) << 1);
    ivec2 c1 = c0 >> 1;
    ivec2 d = ring_position & 1;
    ivec3 ts = textureSize(cache_data, 0) - 1;

    vec4 s0 = texelFetch(cache_data, ivec3( c0      & ts.xy, (footprint.z) & ts.z), 0);
    vec4 s1 = texelFetch(cache_data, ivec3((c1 + d) & ts.xy, (footprint.z + 1) & ts.z), 0);
    vec4 s2 = texelFetch(cache_data, ivec3(c1 & ts.xy, (footprint.z + 1) & ts.z), 0);

    vec4 m = (s1 + s2);
    vec2 a = ((abs(f_ring_position) + transition.xy) - transition.z) * transition.w;
    alpha = clamp(max(a.x, a.y), 0.0, 1.0);
    frag_terrain_data.w = mix(s0.w, m.w*0.5, alpha);
    frag_terrain_data.xyz = mix(s0.xyz, normalize(m.xyz), alpha); 

    tile_id = c0 - 1;
    tile_offset = f_ring_position;
    //frag_position = p;
    vec2 p = vec2(c0 << footprint.z);//((ring_position + 1) << footprint.z) + ((cache_offset >> (footprint.z+1)) << (footprint.z+1));// + (1 << footprint.z);
    gl_Position = mvp_matrix * vec4(p, frag_terrain_data.a, 1).xzyw;
}
)GLSLSOURCE"; //END_GLSLSOURCE


    static constexpr const char* fragment_str =
//BEGIN_GLSLSOURCE
R"GLSLSOURCE(

#if defined(TEXTURE_COORDS)
layout (location = 0) uniform ivec3 footprint;
layout (location = 9) uniform ivec2 K;
layout (location = 10) uniform int log2_tile_size;
layout (location = 11) uniform vec2 one_over_atlas_size;
#endif

#if defined(TERRAIN_DATA)
layout (location = 0) in vec4 frag_terrain_data;
layout (location = 1) in float frag_alpha;
#endif

#if defined(TEXTURE_COORDS)
layout (location = 3) in flat ivec2 tile_id;
layout (location = 4) in vec2 tile_offset;
#endif

#if defined(TERRAIN_DATA)
layout (location = 0) out vec4 terrain_data;
#endif

#if defined(TEXTURE_COORDS)
layout (location = 1) out vec3 texture_coords;
#endif

#if defined(TEXTURE_COORDS_DF)
layout (location = 2) out vec4 texture_coords_dF;
#endif


#if defined(TEXTURE_COORDS)

    uint hash(in uint x) {
        x += x << 10;
        x ^= x >> 6;
        x += x << 3;
        x ^= x >> 11;
        x += x << 15;
        return x;
    }

    vec4 tileEdges(in ivec2 t) {
        uvec2 h = uvec2(hash(t.x), hash(t.y << 1));
 
        uint s = hash(h.x + t.y) & (K.y - 1);
        uint n = hash(h.x + (t.y + 1)) & (K.y - 1);
        
        uint w = hash(t.x + h.y) & (K.x - 1);
        uint e = hash((t.x + 1) + h.y) & (K.x - 1);

        return vec4(s,n,w,e);
    } 

    float tileIndex1D(in vec2 e) {
        if (e.x < e.y) return 2.0 * e.x + e.y * e.y; 
        if (e.x > 0.0 && e.x == e.y) return (e.x + 1.0) * (e.y + 1.0) - 2.0;
        if (e.x <= 0.0 && e.x == e.y) return 0.0;
        if (e.y > 0.0) return e.x * e.x + 2.0 * e.y - 1.0;
        return (e.x + 1.0) * (e.y + 1.0) - 1.0;
    }

    float tileIndex1D_nobr(in vec2 e) {
        float c1 = (e.x * e.x + 2.0 * e.y - 1.0) * float(e.x > e.y) * float(e.y > 0.0);
        float c2 = (2.0 * e.x + e.y * e.y) * float(e.y > e.x) * float(e.x >= 0.0);
        float c3 = ((e.x + 1.0) * (e.x + 1.0) - 2.0) * float(e.x == e.y) * float(e.y > 0.0);
        float c4 = ((e.x + 1.0) * (e.x + 1.0) - 1.0) * float(e.x > e.y) * float(e.y == 0.0);

        return c1 + c2 + c3 + c4;
    }
 
    vec2 TileIndex2D(in vec4 tile_edges) {
        return vec2(tileIndex1D_nobr(tile_edges.zw), 
                    tileIndex1D_nobr(tile_edges.xy));
    }

#endif

vec3 pal(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d) {
    return a + b*cos( 6.28318*(c*t+d) );
}

void main() {

#if defined(TERRAIN_DATA) 
    terrain_data = vec4(normalize(frag_terrain_data.xyz), frag_terrain_data.a);
#endif

#if defined(TEXTURE_COORDS) || defined(TEXTUERE_COORDS_DF)
#endif

#if defined(TEXTURE_COORDS) 
    vec2 i = floor(tile_offset);
    vec2 f = fract(tile_offset);

    vec2 l = f * float(1 << footprint.z);
     
    ivec2 k = ivec2(floor(l)) + (tile_id << footprint.z);

    vec2 ti = TileIndex2D(tileEdges(k >> log2_tile_size));
    vec2 tc = ti*float(1 << log2_tile_size) + 
              vec2(k & ((1 << log2_tile_size) - 1)) + 
              fract(l);

    texture_coords = vec3((tc * one_over_atlas_size), 0.0); 
    #endif

#if defined(TEXTURE_COORDS_DF)
    vec2 fake_texture_coords = tile_offset * float(1 << max(0, footprint.z - log2_tile_size));
    texture_coords_dF = vec4(dFdx(texture_coords).xy, dFdy(texture_coords).xy); 
#endif
}
    )GLSLSOURCE"; //END_GLSLSOURCE

public:
    enum class VertexAttribLocation : GLuint {
        position = 0
    };

    enum class TextureUnit : GLuint {
        cache_data = 0,
        Size = 1
    };

    enum class UniformLocation : GLint {
        footprint = 0,
        transition = 1,
        cache_data = 2,
        cache_offset= 3,
        mvp_matrix = 4,

        K = 9,
        log2_tile_size = 10,
        one_over_atlas_size = 11
    };

    enum class OutputLocation : GLuint {
        terrain_data = 0,
        texture_coords = 1,
        texture_coords_dF = 2
    };

    template<RenderOutput O>
    static inline std::string vertex_source() {
        using namespace GLSLTerrainSource_detail;

        std::string source = "#version 450 core\n";
        source += RenderFlags<O & RenderOutput::terrain_data>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords_dF>::flag;
        source += "\n";
        return source += vertex_str; 
    }

    template<RenderOutput O>
    static inline std::string fragment_source() { 
        using namespace GLSLTerrainSource_detail;
        
        std::string source = "#version 450 core\n";
        source += RenderFlags<O & RenderOutput::terrain_data>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords>::flag;
        source += "\n";
        source += RenderFlags<O & RenderOutput::texture_coords_dF>::flag;
        source += "\n";
        return source += fragment_str; 
    }
};

}
#endif
