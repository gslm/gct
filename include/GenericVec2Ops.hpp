#ifndef GENERIC_VEC2_OPS_HPP
#define GENERIC_VEC2_OPS_HPP

#include <type_traits>

namespace gct {

namespace GenericVec2Ops_detail {
    template<class... >
    using void_t = void;

    template<class Vec2T, class Vec2TA>
    using Vec2xT = decltype(Vec2TA::x(std::declval<Vec2T>()));

    template<class Vec2T, class Vec2TA>
    using Vec2yT = decltype(Vec2TA::y(std::declval<Vec2T>()));

    template<class Vec2T, class Vec2TA, class = void_t<>>
    struct Creation {
        template<class xT, class yT>
        static inline Vec2T make(xT&& x, yT&& y) {
            Vec2T r;
            Vec2TA::x(r) = static_cast<Vec2xT<Vec2T, Vec2TA>>(x);
            Vec2TA::y(r) = static_cast<Vec2yT<Vec2T, Vec2TA>>(y);

            return r;
        }
    };

    template<class Vec2T, class Vec2TA>
    struct Creation<Vec2T, Vec2TA, 
    void_t<decltype(Vec2T(std::declval<Vec2xT<Vec2T, Vec2TA>>(), 
                          std::declval<Vec2yT<Vec2T, Vec2TA>>()))>> {

        template<class xT, class yT>
        static inline Vec2T make(xT&& x, yT&& y) {
            return Vec2T(x,y);
        }

    };

    template<class Vec2T, class Vec2TA, class = void_t<> >
    struct Addition {
        static inline Vec2T add(const Vec2T& v, const Vec2T& u) {
            Vec2T r = v;
            Vec2TA::x(r) = Vec2TA::x(r) + Vec2TA::x(u);
            Vec2TA::y(r) = Vec2TA::y(r) + Vec2TA::y(u);

            return r; 
        } 
    };

    template<class Vec2T, class Vec2TA>
    struct Addition <Vec2T, Vec2TA, 
    void_t<decltype(std::declval<Vec2T>() + std::declval<Vec2T>())> > {

        static inline Vec2T add(const Vec2T& v, const Vec2T& u) {
            return v + u;
        } 
    };

    template<class Vec2T, class Vec2TA, class = void>
    struct Multiplication {
        static inline Vec2T mul(const Vec2T& v, const Vec2T& u) {
            Vec2T r = v;
            Vec2TA::x(r) = Vec2TA::x(r) * Vec2TA::x(u);
            Vec2TA::y(r) = Vec2TA::y(r) * Vec2TA::y(u);
            
            return r;
        }
    };

    template<class Vec2T, class Vec2TA>
    struct Multiplication<Vec2T, Vec2TA, 
    void_t<decltype(std::declval<Vec2T>() + std::declval<Vec2T>())> > {

        static inline Vec2T mul(const Vec2T& v, const Vec2T& u) {
            return v * u;
        }
    };

}

template<class Vec2T, class Vec2TAccess>
struct GenericVec2Ops {
    template<class xT, class yT>
    static inline Vec2T make(const xT& x, const yT& y) {
        using namespace GenericVec2Ops_detail;
        return Creation<Vec2T, Vec2TAccess>::make(x,y);
    }

    static inline Vec2T add(const Vec2T& v, const Vec2T& u) {
        using namespace GenericVec2Ops_detail;
        return Addition<Vec2T, Vec2TAccess>::add(v,u); 
    }
    
    static inline Vec2T mul(const Vec2T& v, const Vec2T& u) {
        using namespace GenericVec2Ops_detail;
        return Multiplication<Vec2T, Vec2TAccess>::mul(v,u); 
    }
};

}
#endif
