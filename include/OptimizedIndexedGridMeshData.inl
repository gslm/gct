
template<class IT, class VT, class VA, class VO>
OptimizedIndexedGridMeshData<IT,VT,VA,VO>::OptimizedIndexedGridMeshData() {}

template<class IT, class VT, class VA, class VO>
OptimizedIndexedGridMeshData<IT, VT, VA, VO>::OptimizedIndexedGridMeshData
        (const VT& p0, IT width, IT height, const VT& scale, IT cache_size) {
    GenerateGridVertices(p0, width, height, scale, cache_size);
    GenerateGridIndices(width, height, cache_size);
}

template<class IT, class VT, class VA, class VO>
OptimizedIndexedGridMeshData<IT,VT,VA,VO>::~OptimizedIndexedGridMeshData() {}

template<class IT, class VT, class VA, class VO>
std::size_t OptimizedIndexedGridMeshData<IT,VT,VA,VO>::vertices_bytes() {
    return vertices.size() * sizeof(VT);
}

template<class IT, class VT, class VA, class VO>
std::size_t OptimizedIndexedGridMeshData<IT,VT,VA,VO>::indices_bytes() {
    return indices.size() * sizeof(IT);
}

template<class IT, class VT, class VA, class VO>
std::size_t OptimizedIndexedGridMeshData<IT,VT,VA,VO>::vertices_size() {
    return vertices.size();
}

template<class IT, class VT, class VA, class VO>
std::size_t OptimizedIndexedGridMeshData<IT,VT,VA,VO>::indices_size() {
    return indices.size();
}

template<class IT, class VT, class VA, class VO>
VT* OptimizedIndexedGridMeshData<IT,VT,VA,VO>::vertices_data() {
    return vertices.data();
}

template<class IT, class VT, class VA, class VO>
IT* OptimizedIndexedGridMeshData<IT,VT,VA,VO>::indices_data() {
    return indices.data();
}

template<class IT, class VT, class VA, class VO>
void OptimizedIndexedGridMeshData<IT,VT,VA,VO>::GenerateSubGridVertices
        (const VT& p0, IT width, IT height, const VT& scale) {
 
    for (IT y = 0; y < height; ++y) {
        for (IT x = 0; x < width; ++x) {
            VT p;
            VA::x(p) = x; 
            VA::y(p) = y;
            vertices.push_back(VO::add(p0, VO::mul(p, scale)));
        }
    }
}

template<class IT, class VT, class VA, class VO>
void OptimizedIndexedGridMeshData<IT,VT,VA,VO>::GenerateGridVertices
        (const VT& p0, IT width, IT height, const VT& scale, IT cache_size) {

    const IT part = width/cache_size + std::min(1, width%cache_size) - 1;

    const IT w = width + part; 
 
    vertices.reserve(w * height);

    //using Vec2T_xT = typename std::decay_t<decltype(VA::x(p0))>;

    VT pc = p0; 
    for (int i = 0; i < w/cache_size; ++i) {
        GenerateSubGridVertices(pc, cache_size, height, scale);
        VA::x(pc) = VA::x(pc) + cache_size - 1;
    }
    
    GenerateSubGridVertices(pc, (w%cache_size), height, scale);

}

template<class IT, class VT, class VA, class VO>
void OptimizedIndexedGridMeshData<IT,VT,VA,VO>::GenerateSubGridIndices
        (IT offset, IT width, IT height, IT start_quad) { 

    auto emit_top_left_quad = [&](IT i, IT j) {
        indices.push_back(offset + width*(i+1) + j+0);
        indices.push_back(offset + width*(i+0) + j+0);
        indices.push_back(offset + width*(i+0) + j+1);
            
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+1) + j+0);
        indices.push_back(offset + width*(i+0) + j+1);
    };

    auto emit_top_right_quad = [&](IT i, IT j) {
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+1) + j+0);
        indices.push_back(offset + width*(i+0) + j+0);
         
        indices.push_back(offset + width*(i+0) + j+1);
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+0) + j+0);
    };

    auto emit_bottom_right_quad = [&](IT i, IT j) {
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+1) + j+0);
        indices.push_back(offset + width*(i+0) + j+0);
         
        indices.push_back(offset + width*(i+0) + j+1);
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+0) + j+0);
    };

    auto emit_bottom_right_quad = [&](IT i, IT j) {
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+1) + j+0);
        indices.push_back(offset + width*(i+0) + j+0);
         
        indices.push_back(offset + width*(i+0) + j+1);
        indices.push_back(offset + width*(i+1) + j+1);
        indices.push_back(offset + width*(i+0) + j+0);
    };

    // Precached indices.
    for (IT j = 0; j < width-1; ++j) {
        indices.push_back(offset + j+0);
        indices.push_back(offset + j+1);
        indices.push_back(offset + j+1);
    }

    for (IT i = 0; i < height-1; ++i) {
        for (IT j = 0; j < width-1; ++j) {
            bool top    = i % 2 == 0;
            bool right  = j % 2 == 0;

            if (top and right) emit_top_right_quad(i, j);
            else if (top and not right) emit_top_left_quad(i, j);
            else if (not top and right) emit_bottom_right_quad(i, j);
            else if (not top and not right) emit_bottom_left_quad(i, j)
            
            if ((i + j + start_quad) % 2 == 1) emit_right_quad(i, j);
            else emit_left_quad(i, j);
        }
    }

}

template<class IT, class VT, class VA, class VO>
void OptimizedIndexedGridMeshData<IT,VT,VA,VO>::GenerateGridIndices
        (IT width, IT height, IT cache_size) {
   
    indices.reserve((width-1)*(height-1)*6 + (width-1)*3); 

    const IT part = width/cache_size + std::min(1, width%cache_size) - 1;
    const IT w = width + part;
 
    IT offset = 0; 
    for (IT i = 0; i < w/cache_size; ++i) {
        GenerateSubGridIndices(offset, cache_size, height, (i + cache_size)%2);
        offset += height*cache_size;
    }
 
    GenerateSubGridIndices(offset, (w%cache_size), height,  (w/cache_size - cache_size)%2);
}   
