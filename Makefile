CPP_OBJECTS = app.o
CPP_SOURCES = src/app.cpp
CPP_INCLUDES = -I./include 
CPP_CC = g++ -std=c++14
CPP_OPT = -O2

CUDA_OBJECTS = cudaTerrainGeneration.o CudaGLSurfaceObject.o
CUDA_SOURCES = src/cuda/cudaTerrainGeneration.cu src/cuda/CudaGLSurfaceObject.cpp
CUDA_INCLUDES = -I./include/cuda
CUDA_CC = nvcc -std=c++11 -arch sm_30
CUDA_LIB = -L/opt/cuda/bin/..//lib64/
CUDA_OPT = -O2

GLSL_DEBUG = -DGLSL_DEBUG


LIBS = -L./lib  -lGL -lgl3w -lglfw3 -lX11 -ldl -lXxf86vm -lXrandr -lpthread  -lXi -lXinerama -lXcursor -lOpenImageIO $(CUDA_LIB) -lcudart 

all: link

cuda_build:
	$(CUDA_CC) $(CUDA_OPT) -dc -c $(CUDA_SOURCES) $(CUDA_INCLUDES)

cpp_build:
	$(CPP_CC) $(CPP_OPT) -c $(CPP_SOURCES) $(CPP_INCLUDES) $(GLSL_DEBUG) -Wall -g3

cuda_link: cuda_build
	$(CUDA_CC) $(CUDA_OPT) -dlink $(CUDA_OBJECTS) -o cuda_objects.o

link: cpp_build cuda_link
	$(CPP_CC) $(CPP_OPT) $(CPP_OBJECTS) $(CUDA_OBJECTS) cuda_objects.o $(LIBS) -o app -g

clean:
	rm -f *.o app
